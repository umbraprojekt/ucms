(function(){
	var dialog = function(editor) {
		var
			lang = editor.lang.pagelink,
			elements = [[lang.selectEmpty,'0']]
		;

		jQuery.ajax({
			url: '/ajax/page/ckeditor-list',
			dataType: 'json',
			async: false,
			success: function(data, textStatus, jqXHR) {
				for(var i in data.pages) elements.push(data.pages[i]);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (jqXHR.status == 404) {
					jQuery.ajax({
						url: '/ajax/page/ckeditor-list',
						dataType: 'json',
						async: false,
						success: function(data, textStatus, jqXHR) {
							for(var i in data.pages) elements.push(data.pages[i]);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log(errorThrown);
						}
					});
				} else {
					console.log(errorThrown);
				}
			}
		});

		return {
			title: lang.dialogTitle,
			minWidth: 400,
			minHeight: 60,
			buttons: [CKEDITOR.dialog.okButton,CKEDITOR.dialog.cancelButton],
			onOk: function() {
				var val = this.getContentElement('page','alias').getValue();
				val = '/' + val + '.html';
				var selection = editor.getSelection().getSelectedText();
				if (selection == '') selection = val;
				if (val != 0) CKEDITOR.instances.body.insertHtml('<a href="'+val+'">'+selection+'</a>');
			},
			onLoad: function() {},
			onShow: function() {
				var select = $(this.parts.contents.$).css({overflow: "visible"}).find("select");
				select.width(select.width() + 30).chosen();
			},
			onHide: function() {},
			onCancel: function() {},
			resizable: 'both',
			contents: [{
				id: 'page',
				label: lang.labelPage,
				accessKey: 'P',
				elements: [{
					id: 'alias',
					type: 'select',
					label: lang.labelDropdown,
					labelLayout: 'horizontal',
					items: elements
				}]
			}]
		}
	};

	CKEDITOR.dialog.add('pagelink',function(editor) {
		return dialog(editor);
	});
})();
