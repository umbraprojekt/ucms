CKEDITOR.plugins.add('macrorelatedgallery',{
	lang: ['pl','en','es'],
	init: function(editor) {
		var pluginName = 'macrorelatedgallery';
		var lang = editor.lang.macrorelatedgallery;
		CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/macrorelatedgallery.js');
		editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
		editor.ui.addButton('RelatedGalleryMacro',{
			label: lang.buttonLabel,
			command: pluginName,
			icon: CKEDITOR.plugins.getPath(pluginName) + 'images/macrorelatedgallery.png'
		});
	}
});
