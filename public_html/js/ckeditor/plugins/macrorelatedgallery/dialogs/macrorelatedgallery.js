(function(){
	var dialog = function(editor) {
		var
			lang = editor.lang.macrorelatedgallery,
			elements = [[lang.selectEmpty,'0']]
		;

		jQuery.ajax({
			url: '/admin/gallery/list-related-ajax',
			dataType: 'json',
			data: {
				cid: UCMS.get("cid")
			},
			async: false,
			success: function(data, textStatus, jqXHR) {
				for(var i in data.galleries) elements.push(data.galleries[i]);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});

		return {
			title: lang.dialogTitle,
			minWidth: 400,
			minHeight: 60,
			buttons: [CKEDITOR.dialog.okButton,CKEDITOR.dialog.cancelButton],
			onOk: function() {
				var val = this.getContentElement('gallery','gid').getValue();
				if (val != 0) CKEDITOR.instances.body.insertHtml('{{{relatedGallery|'+val+'}}}');
			},
			onLoad: function() {},
			onShow: function() {},
			onHide: function() {},
			onCancel: function() {},
			resizable: 'both',
			contents: [{
				id: 'gallery',
				label: lang.labelGallery,
				accessKey: 'G',
				elements: [{
					id: 'gid',
					type: 'select',
					label: lang.labelDropdown,
					labelLayout: 'horizontal',
					items: elements
				}]
			}]
		}
	};

	CKEDITOR.dialog.add('macrorelatedgallery',function(editor) {
		return dialog(editor);
	});
})();
