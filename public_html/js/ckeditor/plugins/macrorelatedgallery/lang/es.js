CKEDITOR.plugins.setLang('macrorelatedgallery', 'es', {
	macrorelatedgallery : {
		buttonLabel: 'Macro de la galería relacionada',
		selectEmpty: '-- seleccionar --',
		dialogTitle: 'Insertar macro de la galería relacionada',
		labelGallery: 'Galería relacionada',
		labelDropdown: 'Título de la galería'
	}
});
