CKEDITOR.plugins.setLang('macrorelatedgallery', 'en', {
	macrorelatedgallery : {
		buttonLabel: 'Related gallery macro',
		selectEmpty: '-- select --',
		dialogTitle: 'Insert related gallery macro',
		labelGallery: 'Related gallery',
		labelDropdown: 'Gallery title'
	}
});
