CKEDITOR.plugins.setLang('macrorelatedgallery', 'pl', {
	macrorelatedgallery : {
		buttonLabel: 'Makro powiązanej galerii',
		selectEmpty: '-- wybierz --',
		dialogTitle: 'Wstaw makro powiązanej galerii',
		labelGallery: 'Powiązana galeria',
		labelDropdown: 'Tytuł galerii'
	}
});
