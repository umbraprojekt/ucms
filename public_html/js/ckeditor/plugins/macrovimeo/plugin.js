CKEDITOR.plugins.add('macrovimeo',{
	lang: ['pl','en','es'],
	init: function(editor) {
		var pluginName = 'macrovimeo';
		var lang = editor.lang.macrovimeo;
		CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/macrovimeo.js');
		editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
		editor.ui.addButton('VimeoMacro',{
			label: lang.buttonLabel,
			command: pluginName,
			icon: CKEDITOR.plugins.getPath(pluginName) + 'images/macrovimeo.png'
		});
	}
});
