CKEDITOR.plugins.setLang('macrovimeo', 'es', {
	macrovimeo : {
		buttonLabel: 'Macro de Vimeo',
		dialogTitle: 'Insertar macro de Vimeo',
		labelYT: 'Vimeo',
		labelYTCode: 'Código Vimeo',
		labelYTWidth: 'Ancho',
		labelYTHeight: 'Alto',
		validateYTCode: 'ID del vídeo incorrecto.',
		validateYTWidth: 'El ancho tiene que ser un número integral.',
		validateYTHeight: 'El alto tiene que ser un número integral.'
	}
});
