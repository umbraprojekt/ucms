CKEDITOR.plugins.setLang('macrovimeo', 'pl', {
	macrovimeo : {
		buttonLabel: 'Makro Vimeo',
		dialogTitle: 'Wstaw makro Vimeo',
		labelYT: 'Vimeo',
		labelYTCode: 'Kod Vimeo',
		labelYTWidth: 'Szerokość',
		labelYTHeight: 'Wysokość',
		validateYTCode: 'ID filmu jest nieprawidłowe.',
		validateYTWidth: 'Szerokość musi być liczbą całkowitą.',
		validateYTHeight: 'Wysokość musi być liczbą całkowitą.'
	}
});
