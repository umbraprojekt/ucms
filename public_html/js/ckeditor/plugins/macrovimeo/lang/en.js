CKEDITOR.plugins.setLang('macrovimeo', 'en', {
	macrovimeo : {
		buttonLabel: 'Vimeo macro',
		dialogTitle: 'Insert Vimeo macro',
		labelYT: 'Vimeo',
		labelYTCode: 'Vimeo code',
		labelYTWidth: 'Width',
		labelYTHeight: 'Height',
		validateYTCode: 'The video ID is incorrect.',
		validateYTWidth: 'The width needs to be an integer.',
		validateYTHeight: 'The height needs to be an integer.'
	}
});
