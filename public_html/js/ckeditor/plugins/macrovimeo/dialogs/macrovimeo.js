(function(){
	var dialog = function(editor) {
		var lang = editor.lang.macrovimeo;

		return {
			title: lang.dialogTitle,
			minWidth: 300,
			minHeight: 100,
			buttons: [CKEDITOR.dialog.okButton,CKEDITOR.dialog.cancelButton],
			onOk: function() {
				var
					code = this.getContentElement('vimeo','ytcode').getValue(),
					width = this.getContentElement('vimeo','ytwidth').getValue(),
					height = this.getContentElement('vimeo','ytheight').getValue(),
					macro = 'vimeo|'
				;

				if (code.length == 0) return;
				else macro += code;

				if (width.length > 0 && height.length > 0) {
					macro += '|' + width + '|' + height;
				}

				CKEDITOR.instances.body.insertHtml('{{{' + macro + '}}}');
			},
			onLoad: function() {},
			onShow: function() {},
			onHide: function() {},
			onCancel: function() {},
			resizable: 'both',
			contents: [{
				id: 'vimeo',
				label: lang.labelYT,
				accessKey: 'Y',
				elements: [{
					id: 'ytcode',
					type: 'text',
					label: lang.labelYTCode,
					labelLayout: 'horizontal',
					validate: CKEDITOR.dialog.validate.regex(/^[0-9]+$/,lang.validateYTCode)
				},{
					id: 'ytwidth',
					type: 'text',
					label: lang.labelYTWidth,
					labelLayout: 'horizontal',
					validate: CKEDITOR.dialog.validate.integer(lang.validateYTWidth)
				},{
					id: 'ytheight',
					type: 'text',
					label: lang.labelYTHeight,
					labelLayout: 'horizontal',
					validate: CKEDITOR.dialog.validate.integer(lang.validateYTHeight)
				}]
			}]
		}
	};

	CKEDITOR.dialog.add('macrovimeo',function(editor) {
		return dialog(editor);
	});
})();
