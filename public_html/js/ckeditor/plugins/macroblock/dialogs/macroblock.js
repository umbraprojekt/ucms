(function(){
	var dialog = function(editor) {
		var
			lang = editor.lang.macroblock,
			elements = [[lang.selectEmpty,'0']]
		;

		$.ajax({
			url: '/admin/block/list-ajax',
			dataType: 'json',
			async: false,
			success: function(data, textStatus, jqXHR) {
				for(var i in data.blocks) elements.push(data.blocks[i]);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});

		return {
			title: lang.dialogTitle,
			minWidth: 400,
			minHeight: 60,
			buttons: [CKEDITOR.dialog.okButton,CKEDITOR.dialog.cancelButton],
			onOk: function() {
				var val = this.getContentElement('block','gid').getValue();
				if (val != 0) CKEDITOR.instances.body.insertHtml('{{{block|'+val+'}}}');
			},
			onLoad: function() {},
			onShow: function() {},
			onHide: function() {},
			onCancel: function() {},
			resizable: 'both',
			contents: [{
				id: 'block',
				label: lang.labelBlock,
				accessKey: 'G',
				elements: [{
					id: 'gid',
					type: 'select',
					label: lang.labelDropdown,
					labelLayout: 'horizontal',
					items: elements
				}]
			}]
		}
	}

	CKEDITOR.dialog.add('macroblock',function(editor) {
		return dialog(editor);
	});
})();
