CKEDITOR.plugins.add('macroblock',{
	lang: ['pl','en'],
	init: function(editor) {
		var pluginName = 'macroblock';
		var lang = editor.lang.macroblock;
		CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/macroblock.js');
		editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
		editor.ui.addButton('BlockMacro',{
			label: lang.buttonLabel,
			command: pluginName,
			icon: CKEDITOR.plugins.getPath(pluginName) + 'images/macroblock.png'
		});
	}
});
