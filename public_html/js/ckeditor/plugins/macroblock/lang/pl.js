CKEDITOR.plugins.setLang('macroblock', 'pl', {
	macroblock: {
		buttonLabel: 'Makro bloku',
		selectEmpty: '-- wybierz --',
		dialogTitle: 'Wstaw makro bloku',
		labelBlock: 'Block',
		labelDropdown: 'Tytuł galerii'
	}
});
