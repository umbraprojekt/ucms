CKEDITOR.plugins.setLang('macroblock', 'en', {
	macroblock: {
		buttonLabel: 'Block macro',
		selectEmpty: '-- select --',
		dialogTitle: 'Insert block macro',
		labelBlock: 'Block',
		labelDropdown: 'Block title'
	}
});
