CKEDITOR.plugins.add('macroyoutube',{
	lang: ['pl','en','es'],
	init: function(editor) {
		var pluginName = 'macroyoutube';
		var lang = editor.lang.macroyoutube;
		CKEDITOR.dialog.add(pluginName, this.path + 'dialogs/macroyoutube.js');
		editor.addCommand(pluginName, new CKEDITOR.dialogCommand(pluginName));
		editor.ui.addButton('YouTubeMacro',{
			label: lang.buttonLabel,
			command: pluginName,
			icon: CKEDITOR.plugins.getPath(pluginName) + 'images/macroyoutube.png'
		});
	}
});
