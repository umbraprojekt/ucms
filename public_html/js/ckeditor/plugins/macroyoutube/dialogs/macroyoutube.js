(function(){
	var dialog = function(editor) {
		var lang = editor.lang.macroyoutube;

		return {
			title: lang.dialogTitle,
			minWidth: 300,
			minHeight: 100,
			buttons: [CKEDITOR.dialog.okButton,CKEDITOR.dialog.cancelButton],
			onOk: function() {
				var
					code = this.getContentElement('youtube','ytcode').getValue(),
					width = this.getContentElement('youtube','ytwidth').getValue(),
					height = this.getContentElement('youtube','ytheight').getValue(),
					macro = 'youtube|'
				;

				if (code.length == 0) return;
				else macro += code;

				if (width.length > 0 && height.length > 0) {
					macro += '|' + width + '|' + height;
				}

				CKEDITOR.instances.body.insertHtml('{{{' + macro + '}}}');
			},
			onLoad: function() {},
			onShow: function() {},
			onHide: function() {},
			onCancel: function() {},
			resizable: 'both',
			contents: [{
				id: 'youtube',
				label: lang.labelYT,
				accessKey: 'Y',
				elements: [{
					id: 'ytcode',
					type: 'text',
					label: lang.labelYTCode,
					labelLayout: 'horizontal',
					validate: CKEDITOR.dialog.validate.regex(/^[a-zA-Z0-9\-\_]{11}$/,lang.validateYTCode)
				},{
					id: 'ytwidth',
					type: 'text',
					label: lang.labelYTWidth,
					labelLayout: 'horizontal',
					validate: CKEDITOR.dialog.validate.integer(lang.validateYTWidth)
				},{
					id: 'ytheight',
					type: 'text',
					label: lang.labelYTHeight,
					labelLayout: 'horizontal',
					validate: CKEDITOR.dialog.validate.integer(lang.validateYTHeight)
				}]
			}]
		}
	}

	CKEDITOR.dialog.add('macroyoutube',function(editor) {
		return dialog(editor);
	});
})();
