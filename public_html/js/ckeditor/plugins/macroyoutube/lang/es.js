CKEDITOR.plugins.setLang('macroyoutube', 'es', {
	macroyoutube : {
		buttonLabel: 'Macro de YouTube',
		dialogTitle: 'Insertar macro de YouTube',
		labelYT: 'YouTube',
		labelYTCode: 'Código YouTube',
		labelYTWidth: 'Ancho',
		labelYTHeight: 'Alto',
		validateYTCode: 'ID del vídeo incorrecto.',
		validateYTWidth: 'El ancho tiene que ser un número integral.',
		validateYTHeight: 'El alto tiene que ser un número integral.'
	}
});
