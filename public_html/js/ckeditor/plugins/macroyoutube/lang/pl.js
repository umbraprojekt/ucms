CKEDITOR.plugins.setLang('macroyoutube', 'pl', {
	macroyoutube : {
		buttonLabel: 'Makro YouTube',
		dialogTitle: 'Wstaw makro YouTube',
		labelYT: 'YouTube',
		labelYTCode: 'Kod YouTube',
		labelYTWidth: 'Szerokość',
		labelYTHeight: 'Wysokość',
		validateYTCode: 'ID filmu jest nieprawidłowe.',
		validateYTWidth: 'Szerokość musi być liczbą całkowitą.',
		validateYTHeight: 'Wysokość musi być liczbą całkowitą.'
	}
});
