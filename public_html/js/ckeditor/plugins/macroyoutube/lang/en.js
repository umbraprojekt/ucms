CKEDITOR.plugins.setLang('macroyoutube', 'en', {
	macroyoutube : {
		buttonLabel: 'YouTube macro',
		dialogTitle: 'Insert YouTube macro',
		labelYT: 'YouTube',
		labelYTCode: 'YouTube code',
		labelYTWidth: 'Width',
		labelYTHeight: 'Height',
		validateYTCode: 'The video ID is incorrect.',
		validateYTWidth: 'The width needs to be an integer.',
		validateYTHeight: 'The height needs to be an integer.'
	}
});
