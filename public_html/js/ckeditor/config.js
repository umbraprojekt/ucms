﻿CKEDITOR.editorConfig = function( config )
{
	/**
	 * Extra plugins.
	 * Comment out the unneeded ones.
	 */
	var extraPlugins = [];
	extraPlugins.push('macrogallery');
	extraPlugins.push('macrorelatedgallery');
	extraPlugins.push('macroyoutube');
	extraPlugins.push('macrovimeo');
	extraPlugins.push('macroblock');
	extraPlugins.push('pagelink');

	for (i in extraPlugins) {
		if (config.extraPlugins.length > 0) config.extraPlugins += ',';
		config.extraPlugins += extraPlugins[i];
	}

	config.filebrowserBrowseUrl = '/js/filemanager/index.html';

	config.toolbar_Full = [
		{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
		{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
		'/',
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
		'/',
		{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
	];

	config.toolbar_Umbra = [
		['Source'],
		['Cut','Copy','PasteText'],
		['Bold','Italic','Strike','Underline','-','Subscript','Superscript','-','Format','RemoveFormat'],
		'/',
		['NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		['Link','Unlink','-','Image','HorizontalRule','SpecialChar'],
		['GalleryMacro','RelatedGalleryMacro','YouTubeMacro','VimeoMacro','BlockMacro','PageLink']
	];

	config.toolbar = 'Umbra';
};
