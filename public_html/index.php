<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// We don't like magic quotes...
if (get_magic_quotes_gpc()) throw new Exception('The PHP flag "magic_quotes_gpc" is required to be turned off.',500);

// Define paths
defined('DOC_ROOT')
	|| define ('DOC_ROOT', realpath(dirname(__FILE__) . '/..'));

defined('PUBLIC_PATH')
	|| define ('PUBLIC_PATH', realpath(dirname(__FILE__)));

defined('APPLICATION_PATH')
	|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
	|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Define application domain
defined('APPLICATION_DOMAIN')
	|| define('APPLICATION_DOMAIN', (getenv('APPLICATION_DOMAIN') ? getenv('APPLICATION_DOMAIN') : $_SERVER['SERVER_NAME']));

// check if the request is for an image that should be in the upload folder
if (preg_match('/\.(jpeg|jpg|gif|png)/', $_SERVER["REQUEST_URI"])) {
	$file = PUBLIC_PATH . "/upload" . $_SERVER["REQUEST_URI"];
	list($filename) = explode("?", $file);
	if (is_file($filename)) {
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime = finfo_file($finfo, $filename);
		header('Content-Type:'.$mime);
		readfile($filename);
		die;
	} else {
		if (php_sapi_name() === "cli-server") {
			return false;
		}
	}
}

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
	realpath(APPLICATION_PATH . '/../library'),
	get_include_path(),
)));

// Composer autoloader
require_once DOC_ROOT . "/vendor/autoload.php";

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
	APPLICATION_ENV,
	APPLICATION_PATH . '/configs/application.ini'
);

$application
	->bootstrap()
	->run()
;
