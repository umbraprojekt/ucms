<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	/**
	 * Circumvent ZF 1.12.1 "improvements"
	 */
	public function _initResourceLoader() {
		if ($this->getResourceLoader() === null) {
			$this->setResourceLoader(new Zend_Application_Module_Autoloader(array(
				"namespace" => $this->getAppNamespace(),
				"basePath" => dirname(__FILE__)
			)));
		}
	}

	/**
	 * Set the application.ini options registry key and initialise other options
	 */
	protected function _initConfig () {
		Zend_Registry::set('Config',$this->getOptions());

		// front controller
		$front = Zend_Controller_Front::getInstance();

		// init layout
		Zend_Layout::startMvc();

		// init modules
		$front->addModuleDirectory(APPLICATION_PATH.'/modules');

		// set action helper directory
		Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH.'/modules/default/helpers');
	}

	/**
	 * Set cache
	 */
	protected function _initCache () {
		// get config
		$config = Zend_Registry::get('Config');

		// use Memcached if available, else stick with File
		if (APPLICATION_ENV === "production" && class_exists("Memcached") && extension_loaded("memcached")) {
			$backend = 'Libmemcached';
		} else if (APPLICATION_ENV === "production" && class_exists("Memcache") && extension_loaded("memcache")) {
			$backend = 'Memcached';
		} else {
			$backend = 'File';
		}
		$cache = Zend_Cache::factory(
			'Core',
			$backend,
			array(
				'lifetime' => 3600 * 24, // clear cache once per day
				'automatic_serialization' => true,
				'cache_id_prefix' => $config['resources']['db']['params']['dbname'] // use a key prefix
			),
			array('cache_dir' => DOC_ROOT.'/cache')
		);
		Zend_Translate::setCache($cache); // cache translations
		Zend_Db_Table_Abstract::setDefaultMetadataCache($cache); // cache database schema tables
		Zend_Registry::set('Cache', $cache); // set cache registry key
	}

	/**
	 * Create appropriate routers
	 */
	protected function _initRouter () {
		$front = Zend_Controller_Front::getInstance();
		$router = $front->getRouter();

		// create default routes, available in all configurations
		// alias route: /alias.html/remainder
		$alias = new Zend_Controller_Router_Route_Regex('(.+)\.html(.*)',
			array(
				'module' => 'default',
				'controller' => 'index',
				'action' => 'index',
				'remainder' => ''
			),
			array(
				1 => 'alias',
				2 => 'remainder'
			),
			'%s.html%s'
		);
		// installation route: /install
		$install = new Zend_Controller_Router_Route('install/:action',array(
			'module' => 'admin',
			'controller' => 'install',
			'action' => 'index'
		));
		// default RSS feed route: /rss.xml
		$rss = new Zend_Controller_Router_Route('rss.xml',array(
			'module' => 'default',
			'controller' => 'feed',
			'action' => 'index'
		));
		// default sitemap route: /sitemap.xml
		$sitemap = new Zend_Controller_Router_Route('sitemap.xml',array(
			'module' => 'default',
			'controller' => 'index',
			'action' => 'sitemap'
		));

		// create correct routes
		$router->addRoute('alias', $alias);
		$router->addRoute('install', $install);
		$router->addRoute('sitemap', $sitemap);
		$router->addRoute('rss', $rss);
	}

	// initialise the view resource
	protected function _initView () {
		$view = new Zend_View();

		$view->setHelperPath(APPLICATION_PATH.'/modules/default/views/helpers');
		$view->setEncoding('utf-8');
		$view->doctype('HTML5');
		$view->headMeta()->setCharset('utf-8')->setIndent("\t");
		$view->headTitle()->setSeparator(' | ')->setIndent("\t");
		$view->headLink()->setIndent("\t");
		$view->headScript()->setIndent("\t");
		$view->inlineScript()->setIndent("\t");
		$view->addScriptPath(DOC_ROOT.'/views');

		Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer')->setView($view);

		return $view;
	}

	/**
	 * Initialise autoloader namespaces
	 */
	protected function _initAL () {
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace("UCMS_");
	}

	protected function _initTranslator()
	{
		$config = Zend_Registry::get('Config');
		$language = $config["site"]["language"];

		// initialise translator
		$locale = new Zend_Locale($language);
		Zend_Registry::set("Zend_Locale", $locale);

		// choose translations catalogue file
		if (is_file(APPLICATION_PATH . "/languages/{$language}.mo")) {
			$catalogue = APPLICATION_PATH . "/languages/{$language}.mo";
		} else {
			throw new Zend_Exception("Language '{$language}' is missing a gettext catalogue file.");
		}

		$translate = new Zend_Translate("Gettext", $catalogue, $locale);
		Zend_Form::setDefaultTranslator($translate);
		Zend_Controller_Router_Route::setDefaultTranslator($translate);
		Zend_Registry::set("Zend_Translate", $translate);
	}
}
