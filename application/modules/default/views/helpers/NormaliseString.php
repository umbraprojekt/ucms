<?php
use Mingos\uCMS\Filter\NormaliseStringFilter;

class Zend_View_Helper_NormaliseString {
	/**
	 * Normalise a string
	 * @param string $string
	 * @return string
	 */
	public function normaliseString ($string) {
		$filter = new NormaliseStringFilter();
		return $filter->filter($string);
	}
}
