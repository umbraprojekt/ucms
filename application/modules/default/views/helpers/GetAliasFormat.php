<?php
use Mingos\uCMS\Filter\NormaliseStringFilter;
use Mingos\uCMS\Model\ConfigModel;

class Zend_View_Helper_GetAliasFormat {
	/**
	 * @var Zend_View_Interface
	 */
	public $view;

	/**
	 * Returns the alias corresponding to the given pattern
	 * @param string $aliasType
	 * @param array $node Node for which the alias is generated
	 * @param array $data additional values (if specified in the pattern, but unavailable from the node)
	 * @return string
	 */
	public function getAliasFormat ($aliasType, $node = null, $data = array()) {
		$model_config = new ConfigModel($aliasType);
		$filter = new NormaliseStringFilter();
		if (is_string($node["created"])) {
			$node["created"] = new DateTime($node["created"]);
		}

		$translations = array(
			'%year%' => !empty($node) && array_key_exists('created',$node) ? $node['created']->format('Y') : strftime('%Y'),
			'%month%' => !empty($node) && array_key_exists('created',$node) ? $node['created']->format('m') : strftime('%m'),
			'%day%' => !empty($node) && array_key_exists('created',$node) ? $node['created']->format('d') : strftime('%d'),
			'%userid%' => !empty($node) && array_key_exists('uid',$node) ? $node['uid'] : Zend_Auth::getInstance()->getIdentity()->id,
			'%username%' => !empty($node) && array_key_exists('author',$node) ? $node['author'] : Zend_Auth::getInstance()->getIdentity()->login
		);

		foreach ($data as $key => $val) {
			$translations["%{$key}%"] = $filter->filter(strval($val));
		}

		$format = $model_config->getParam("alias");
		if (!$format) $format = '%title%';

		return strtr($format, $translations);
	}

	/**
	 * Sets the view field
	 * @param $view Zend_View_Interface
	 */
	public function setView (Zend_View_Interface $view) {
		$this->view = $view;
	}
}
