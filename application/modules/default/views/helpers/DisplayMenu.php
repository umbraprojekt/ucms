<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'Zend/View/Interface.php';
class Zend_View_Helper_DisplayMenu {
	/**
	 * @var Zend_View_Interface
	 */
	public $view;

	/**
	 * Display a menu
	 */
	public function displayMenu ($menu, $alias, $onlyActive = false) {
		if (empty($menu)) return ''; // failsafe in case there's no menu
		$this->checkActive($menu['items'], $alias, $onlyActive);
		$return = "<ul class=\"menu\" id=\"menu-{$menu['description']['id']}\">";
		$return .= $this->renderItems($menu['items'],$onlyActive);
		$return .= "</ul>";
		return $return;
	}

	/**
	 * Check whether the menu elements are active.
	 */
	private function checkActive (&$menu, $alias) {
		$return = false;
		foreach ($menu as &$item) {
			if ($this->checkActive($item['children'],$alias)) {
				$item['active'] = 'active-trail';
				$return = true;
			} else {
				$item['active'] = 'inactive';
			}
			if ($item['href'] == "/{$alias}.html") {
				$item['active'] = 'active active-trail';
				$return = true;
			}
		}
		return $return;
	}

	/**
	 * Render the items of a menu
	 */
	private function renderItems ($menu, $onlyActive) {
		$return = "";
		foreach($menu as $item) {
			$linktype = $item['href'][0] == '/' ? 'internal-link' : 'external-link';
			$return .= "<li id=\"menu-item-{$item['id']}\" class=\"menu-item {$item['active']}\"><a href=\"{$item['href']}\" class=\"menu-link {$linktype}\">{$item['label']}</a>";
			if (count($item['children']) > 0 && (!$onlyActive || strstr($item['active'],'active-trail'))) {
				$return .= "<ul>".$this->renderItems($item['children'],$onlyActive)."</ul>";
			}
			$return .= "</li>";
		}
		return $return;
	}

	/**
	 * Sets the view field
	 * @param $view Zend_View_Interface
	 */
	public function setView (Zend_View_Interface $view) {
		$this->view = $view;
	}
}
