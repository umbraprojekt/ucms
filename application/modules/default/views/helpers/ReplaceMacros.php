<?php
use Mingos\uCMS\Model\GalleryModel;
use Mingos\uCMS\Model\BlockModel;
use Mingos\uCMS\Model\ContentGalleryModel;
use Mingos\uCMS\Model\ImageModel;

class Zend_View_Helper_ReplaceMacros extends Zend_View_Helper_Abstract {
	public $view;

	/**
	 * Replace macros in a string
	 * @param string $string
	 * @return string
	 */
	public function replaceMacros ($string) {
		$me = $this;
		return preg_replace_callback("/\{\{\{([a-zA-Z]+\|.+)\}\}\}/sU", function ($matches) use ($me) {
			return $me->render($matches[1]);
		}, $string);
	}

	public function setView (Zend_View_Interface $view) {
		$this->view = $view;
	}

	/**
	 * Return the rendered output of any macro
	 * @param string $string The macro string
	 * @return string HTML output
	 */
	private function render ($string) {
		list($macro, $params) = explode('|',$string,2);
		switch($macro) {
			case 'gallery': return $this->renderGallery($params); break;
			case 'relatedGallery': return $this->renderRelatedGallery($params); break;
			case 'youtube': return $this->renderYouTube($params); break;
			case 'vimeo': return $this->renderVimeo($params); break;
			case 'block': return $this->renderBlock($params); break;
		}
		throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);
	}

	/**
	 * Output a rendered gallery partial view
	 * @param int $id gallery ID
	 * @return string HTML output
	 */
	private function renderGallery($id) {
		if (empty($id)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		$model_node_gallery = new GalleryModel();
		$gallery = $model_node_gallery->getById($id);

		if (empty($gallery)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		$imageModel = new ImageModel();
		$images = $imageModel->get(array('table' => 'node_gallery', 'nid' => $id));

		return $this->view->partial('macro/gallery.phtml',array('gallery' => $gallery, 'images' => $images));
	}

	/**
	 * Output a rendered gallery partial view
	 * @param int $position related gallery position
	 * @return string HTML output
	 */
	private function renderRelatedGallery($position) {
		if (empty($position)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		$ccid = $this->view->alias["id"];

		$modelContentGallery = new ContentGalleryModel();
		$gallery = $modelContentGallery->getByPosition($position, $ccid);
		if (empty($gallery)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		$imageModel = new ImageModel();
		$images = $imageModel->get(array('table' => 'node_gallery', 'nid' => $gallery["id"]));

		return $this->view->partial('macro/gallery.phtml',array('gallery' => $gallery, 'images' => $images));
	}

	/**
	 * Output a rendered YouTube video iframe
	 * @param string $params Parametres from the macro (unprocessed string)
	 * @return string HTML output
	 */
	private function renderYouTube ($params) {
		if (empty($params)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		$params = explode('|',$params);
		$params = array(
			'id' => $params[0],
			'width' => array_key_exists(1, $params) ? $params[1] : 560,
			'height' => array_key_exists(2, $params) ? $params[2] : 349
		);

		return $this->view->partial('macro/youtube.phtml',$params);
	}

	/**
	 * Output a rendered Vimeo video iframe
	 * @param string $params Parametres from the macro (unprocessed string)
	 * @return string HTML output
	 */
	private function renderVimeo($params)
	{
		if (empty($params)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."), 500);

		$params = explode('|', $params);
		$params = array(
			'id' => $params[0],
			'width' => array_key_exists(1, $params) ? $params[1] : 560,
			'height' => array_key_exists(2, $params) ? $params[2] : 349
		);

		return $this->view->partial('macro/vimeo.phtml', $params);
	}

	/**
	 * Output a rendered block partial view
	 * @param int $id block ID
	 * @return string HTML output
	 */
	private function renderBlock($id) {
		if (empty($id)) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		$blockModel = new BlockModel();
		$block = $blockModel->getById($id);

		if (empty($block["id"])) throw new Zend_Exception($this->view->translate("Invalid macro syntax."),500);

		return $this->view->partial("macro/block.phtml",array("block" => $block));
	}
}
