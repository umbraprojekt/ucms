<?php
use Mingos\uCMS\Model\ConfigModel;

class Zend_View_Helper_Image extends Zend_View_Helper_Abstract {
	public $view;

	/**
	 * Fetch image URL, creating the file if it is missing
	 * @param array $image
	 * @param array $data Additional data:
	 *                    [w], [h] => (int) - output image size
	 *                    [type] => (string) - scaling method (basic|magic|adaptive)
	 *                    [fx] => (array) - effects to be applied. Will be applied in the order they are specified. Possible values:
	 *                                      array("greyscale")  - convert image to greyscale
	 *                                      array("sharpen", n) - sharpen image by the value of n
	 *                                      array("overlay", f) - overlay the image f over the original image
	 * @return string
	 */
	public function image ($image, $data = array()) {
		if (!is_dir(PUBLIC_PATH."/upload/{$image['path']}")) mkdir(PUBLIC_PATH."/upload/{$image['path']}",octdec('0777'),true);
		if (empty($data)) { // full size image
			$filename = "{$image['filename']}.{$image['extension']}";
			if (!file_exists(PUBLIC_PATH."/upload/{$image['path']}/{$filename}")) {
				$modelConfig = new ConfigModel("image");
				if ($modelConfig->getParam('unsafe_uploads') == '1') {
					copy(DOC_ROOT."/upload/{$image['orig_path']}/{$image['orig_filename']}.{$image['extension']}",PUBLIC_PATH."/upload/{$image['path']}/{$filename}");
				} else {
					include_once "PhpThumb/ThumbLib.inc.php";
					$img = $this->createBaseImage($image);
					$img->save(PUBLIC_PATH."/upload/{$image['path']}/{$filename}");
				}
			}
		} else { // thumbnail
			// check if all required params are in place
			if (!(array_key_exists('w',$data) && array_key_exists('h',$data) && array_key_exists('type',$data)) && !array_key_exists('fx', $data)) throw new Zend_Exception($this->view->translate('Invalid parametres.'),500);

			// generate filename
			$filename = "{$image['filename']}";
			if (array_key_exists('w',$data)) $filename .= "_{$data['w']}x{$data['h']}_{$data['type'][0]}";
			if (array_key_exists('fx',$data) && count($data['fx'] > 0)) {
				$filename .= '_';
				foreach ($data['fx'] as $fx) {
					$filename .= substr($fx[0],0,1);
				}
			}
			$filename .= ".{$image['extension']}";

			// if such a file does not exist, generate it
			if (!file_exists(PUBLIC_PATH."/upload/{$image['path']}/{$filename}")) {
				// get the base image
				include_once "PhpThumb/ThumbLib.inc.php";
				$img = $this->createBaseImage($image);

				// resize the image, according to the specified resize type
				if (array_key_exists('w',$data)) switch($data['type']) {
					case 'magic':
						$img->magicResize($data['w'], $data['h']);
						break;
					case 'adaptive': // not using the inbuilt resizing because it occasionally misses the correct output dimensions...
						$img->magicResize($data['w'], $data['h']);
						$dim = $img->getCurrentDimensions();
						$dim['midx'] = (int)(($dim['width']-$data['w'])/2);
						$dim['midy'] = (int)(($dim['height']-$data['h'])/2);
						$dim['maxx'] = (int)(($dim['width']-$data['w']));
						$dim['maxy'] = (int)(($dim['height']-$data['h']));
						switch ($image['cut']) {
							case 'N': $img->crop($dim['midx'],0,$data['w'], $data['h']); break;
							case 'E': $img->crop($dim['maxx'],$dim['midy'],$data['w'], $data['h']); break;
							case 'S': $img->crop($dim['midx'],$dim['maxy'],$data['w'], $data['h']); break;
							case 'W': $img->crop(0,$dim['midy'],$data['w'], $data['h']); break;
							case '0': $img->crop($dim['midx'], $dim['midy'], $data['w'], $data['h']); break;
							default: throw new Zend_Exception($this->view->translate('Invalid parametres.'),500); break;
						}
						break;
					case 'basic':
						$img->resize($data['w'], $data['h']); break;
						break;
					default:
						throw new Zend_Exception($this->view->translate('Invalid parametres.'),500);
						break;
				}

				// apply effects
				if (array_key_exists('fx',$data) && count($data['fx'] > 0)) {
					foreach ($data['fx'] as $fx) {
						switch ($fx[0]) {
							case "greyscale": $img->greyscale(); break;
							case "gaussianblur":
								if (!array_key_exists(1, $fx) || $fx < 1) $fx[1] = 1;
								$img->gaussianblur($fx[1]);
								break;
							case "overlay":
								if (!array_key_exists(1, $fx) || !$fx[1]) throw new Zend_Exception($this->view->translate('Invalid parametres.'),500);
								$img->overlay($fx[1]);
								break;
							case "sharpen":
								if (!array_key_exists(1, $fx)) throw new Zend_Exception($this->view->translate('Invalid parametres.'),500);
								$img->sharpen($fx[1]);
								break;
							default: throw new Zend_Exception($this->view->translate('Invalid parametres. '),500); break;
						}
					}
				}

				// save the image to disc
				$img->save(PUBLIC_PATH."/upload/{$image['path']}/{$filename}");
			}
		}

		// return the filename with the correct path
		return "/{$image['path']}/{$filename}";
	}

	public function setView (Zend_View_Interface $view) {
		$this->view = $view;
	}

	private function createBaseImage ($image) {
		if (!file_exists(DOC_ROOT."/upload/{$image['orig_path']}/{$image['orig_filename']}.{$image['extension']}")) throw new Zend_Exception($this->view->translate('File not found.'),404);

		$model = new ConfigModel("image");
		include_once "PhpThumb/ThumbLib.inc.php";
		$img = PhpThumbFactory::create(DOC_ROOT."/upload/{$image['orig_path']}/{$image['orig_filename']}.{$image['extension']}");
		$img->setOptions(array('jpegQuality' => $model->getParam('quality')));

		if (!empty($image['crop'])) {
			list($x, $y, $w, $h) = explode(',',$image['crop']);
			$img->crop($x, $y, $w, $h);
		}

		return $img;
	}
}
