<?php
/**
 * Comment form
 */
class Default_Form_Comment extends Zend_Form {
	public function init() {
		$view = $this->getView();

		// name
		$name = new Zend_Form_Element_Text('name');
		$name
			->setLabel('Name')
			->setRequired()
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $view->translate('This field cannot be empty.'))))
			->addValidator('StringLength', true, array('min' => 3, 'max' => 60, 'encoding' => 'utf-8', 'messages' => array('stringLengthTooShort' => $view->translate('Minimum of %1$s characters.',3), 'stringLengthTooLong' => $view->translate('Maximum of %1$s characters.',60))))
			->addFilters(array(
			new Zend_Filter_StripTags(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StringTrim()
		))
		;
		$this->addElement($name);

		// e-mail address
		$email = new Zend_Form_Element_Text('email');
		$email
			->setLabel('E-mail address')
			->setRequired()
			->addValidator("NotEmpty",true,array('messages' => array('isEmpty' => $this->getView()->translate('This field cannot be empty.'))))
			->addValidator('Regex',true,array('pattern' => "/^[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$/", 'messages' => array('regexNotMatch' => $this->getView()->translate('This e-mail address is invalid.'))))
			->addFilters(array(
			new Zend_Filter_StripTags(),
			new Zend_Filter_StripNewlines(),
			new Zend_Filter_StringTrim()
		))
		;
		$this->addElement($email);

		// comment body
		$content = new Zend_Form_Element_Textarea('content');
		$content
			->setLabel('Comment body')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $view->translate('This field cannot be empty.'))))
			->addValidator('StringLength', true, array('min' => 15, 'max' => 600, 'encoding' => 'utf-8', 'messages' => array('stringLengthTooShort' => $view->translate('Minimum of %1$s characters.',15), 'stringLengthTooLong' => $view->translate('Maximum of %1$s characters.',600))))
			->setRequired()
			->setAttribs(array('rows' => 4,' cols' => 50))
			->addFilters(array(
			new Zend_Filter_StripTags(),
			new Zend_Filter_StringTrim()
		))
		;
		$this->addElement($content);

		$this->addElement('Submit','submit');
		$this->submit->setLabel('Send');
	}
}
