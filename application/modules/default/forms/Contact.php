<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Contact form
 */
class Default_Form_Contact extends Zend_Form {
	public function init() {
		$view = $this->getView();

		// name
		$name = new Zend_Form_Element_Text('name');
		$name
			->setLabel('Name')
			->setRequired()
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $view->translate('This field cannot be empty.'))))
			->addValidator('StringLength', true, array('min' => 3, 'max' => 60, 'encoding' => 'utf-8', 'messages' => array('stringLengthTooShort' => $view->translate('Minimum of %1$s characters.',3), 'stringLengthTooLong' => $view->translate('Maximum of %1$s characters.',60))))
			->addFilter('StripTags');
		$this->addElement($name);

		// e-mail address
		$email = new Zend_Form_Element_Text('email');
		$email
			->setLabel('E-mail address')
			->setRequired()
			->addValidator("NotEmpty",true,array('messages' => array('isEmpty' => $this->getView()->translate('This field cannot be empty.'))))
			->addValidator('Regex',true,array('pattern' => "/^[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$/", 'messages' => array('regexNotMatch' => $this->getView()->translate('This e-mail address is invalid.'))))
		;
		$this->addElement($email);

		// message body
		$content = new Zend_Form_Element_Textarea('content');
		$content
			->setLabel('Message body')
			->addFilter('StripTags')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $view->translate('This field cannot be empty.'))))
			->addValidator('StringLength', true, array('min' => 15, 'max' => 600, 'encoding' => 'utf-8', 'messages' => array('stringLengthTooShort' => $view->translate('Minimum of %1$s characters.',15), 'stringLengthTooLong' => $view->translate('Maximum of %1$s characters.',600))))
			->setRequired()
			->setAttribs(array('rows'=>4,'cols'=>50));
		$this->addElement($content);

		// captcha
		$captcha = new Zend_Form_Element_Captcha ('code', array(
			'label' => $this->getView()->translate('Rewrite the code from the image'),
			'captcha' => array(
				'captcha' => 'Figlet',
				'wordLen' => 6,
				'timeout' => 600,
				'font' => APPLICATION_PATH . '/fonts/roman.flf',
				'messages' => array('badCaptcha' => $this->getView()->translate('Incorrect captcha code.'))
			)
		));
		$this->addElement($captcha);

		$this->addElement('Submit','submit');
		$this->submit->setLabel('Send');
	}
}
