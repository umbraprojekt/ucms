<?php
use Mingos\uCMS\Model\PageModel;
use Mingos\uCMS\Model\NewsModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\MenuItemModel;
use Mingos\uCMS\Model\AliasModel;
use Mingos\uCMS\Model\AclModel;
use Mingos\uCMS\Model\ImageModel;

class IndexController extends Zend_Controller_Action {
	public $alias = '';
	public $acl;

	public function init() {
		// variables passed to the view object
		$this->view->messages = array();

		// flash messages
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) $this->view->messages += $this->flash->getMessages();
		$this->flash->clearMessages();

		// fetch menu
		$modelMenu = new Default_Model_Menu();
		$this->view->nav = $modelMenu->getElements(1);

		// request object
		$this->view->request = $this->_request->getParams();
		if (!array_key_exists('alias',$this->view->request)) $this->view->request['alias'] = '';

		// identity identity
		$auth = Zend_Auth::getInstance()->hasIdentity();
		$role = $auth ? Zend_Auth::getInstance()->getIdentity()->role : 'anonymous';

		// ACL
		if (!($this->acl = $this->view->acl = Zend_Registry::get("Cache")->load('acl'))) {
			$this->acl = $this->view->acl = AclModel::getInstance();
			Zend_Registry::get("Cache")->save($this->acl,'acl');
		}

		// check whether the site's offline
		$offline = Zend_Registry::get("Cache")->load('offline');
		if ($offline === false) {
			$model = new ConfigModel("site");
			$offline = $model->getParam('offline');
			Zend_Registry::get("Cache")->save($offline,'offline');
		}

		// does the URL contain a path alias? if so, decipher it...
		$alias = $this->_request->getParam('alias',null);
		if ($alias) {
			$params = array();
			$remainder = trim($this->_request->getParam('remainder',''),'/');
			if ($remainder) {
				$params_tmp = explode('/',$remainder);
				for ($i = 1; $i < count($params_tmp); $i += 2) {
					$params[$params_tmp[$i-1]] = $params_tmp[$i];
				}
			}
			$model = new AliasModel();
			$this->alias = $this->view->alias = $model->getByAlias($alias);

			if ($this->alias) {
				if ($offline === '0' || $this->acl->isAllowed($role,'admin/index/index')) $this->forward($this->alias['action'],$this->alias['controller'],$this->alias['module'],$params);
				else $this->forward('offline','index','default');
			}
			else throw new Zend_Exception($this->view->translate('Page not found.'),404);
		} else {
			$this->alias = $this->view->alias = array('alias' => '');
			if ($offline && !$this->acl->isAllowed($role,'admin/index/index')) $this->forward('offline','index','default');
		}
	}

	/**
	 * Home page
	 */
	public function indexAction() {
		$this->view->info = $this->_helper->seo();
	}

	/**
	 * News list
	 */
	public function newsListAction() {
		$model = new NewsModel();
       	$data = $model->getAll(true);
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($data));
        $paginator->setItemCountPerPage(5);
        $paginator->setCurrentPageNumber($this->_request->getParam('page'));
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator.phtml');
        $this->view->paginator = $paginator;

		$this->view->vars += array(
			'info' => $this->_helper->seo()
		);
	}

	/**
	 * News item display
	 */
	public function newsAction() {
		$nodeModel = new NewsModel();
		$node = $nodeModel->getById($this->alias['id']);

		$info = $this->_helper->seo($node['seo_keywords'], $node['seo_description'], $node["seo_robots"]);
		$this->view->headTitle($node['seo_title'] ? $node['seo_title'] : $node['title']);

		// news image
		$imageModel = new ImageModel();
		$image = $imageModel->get(array("nid" => $node["id"], "table" => "node_news"));
		$node["image"] = empty($image) ? array() : array_shift($image);

		// apply input format
		$node = $this->view->inputFormat($node);

		$this->view->node = $node;
		$this->view->info = $info;
		$this->view->next = $nodeModel->getNext($node['id']);
		$this->view->prev = $nodeModel->getPrev($node['id']);
	}

	/**
	 * Simple page display
	 */
	public function pageAction () {
		$nodeModel = new PageModel();
		$node = $nodeModel->getById($this->alias['id']);

		$info = $this->_helper->seo($node['seo_keywords'], $node['seo_description'], $node["seo_robots"]);
		$this->view->headTitle($node['seo_title'] ? $node['seo_title'] : $node['title']);

		// apply input format
		$node = $this->view->inputFormat($node);

		$this->view->node = $node;
		$this->view->info = $info;

		// page image
		$imageModel = new ImageModel();
		$image = $imageModel->get(array("nid" => $node["id"], "table" => "node_page"));
		$this->view->node["image"] = empty($image) ? array() : array_shift($image);

		$this->_helper->viewRenderer->setRender($node['view_script']);
	}

	/**
	 * Display the "site offline" page
	 */
	public function offlineAction () {
		$this->view->info = $this->_helper->seo();
	}

	/**
	 * Generate and display an XML sitemap
	 */
	public function sitemapAction () {
		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);

		$nav = new Zend_Navigation();

		$model = new MenuItemModel();
		$nav->addPages($model->getSitemap(1));

		echo $this->view->navigation($nav)->sitemap();
		die;
	}
}
