<?php
use Mingos\uCMS\Model\ImageModel;

class ImageController extends Zend_Controller_Action {
	/**
	 * Display an image with a given ID
	 */
	public function displayAction () {
		$iid = (int)$this->_request->getParam('iid',0);
		if (!$iid) throw new Zend_Exception($this->view->translate('File not found.'),404);

		$imageModel = new ImageModel();
		$image = $imageModel->getById($iid);

		$this->_helper->viewRenderer->setNoRender(true);
		$this->view->layout()->disableLayout();

		$this
			->getResponse()
			->setHeader('Content-Type', $image['mime'])
			->setHeader('Expires', '', true)
			->setHeader('Cache-Control', 'public', true)
			->setHeader('Cache-Control', 'max-age=3800')
			->setHeader('Pragma', '', true)
		;
		echo file_get_contents(DOC_ROOT."/upload/{$image['orig_path']}/{$image['filename']}.{$image['extension']}");
	}
}
