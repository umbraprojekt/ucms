<?php
use Mingos\uCMS\Model\UserModel;

class UserController extends Zend_Controller_Action {
	public function init()
	{
		$this->_helper->_layout->setLayout('login');
		// variables passed to the view object
		$this->view->messages = array();

		// flash messages
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) $this->view->messages += $this->flash->getMessages();
		$this->flash->clearMessages();
	}

	/**
	 * Do nothing. Can be recycled if necessary.
	 */
	public function indexAction()
	{
		$this->redirect("/default/user/login");
	}

	/**
	 * Login form
	 */
	public function loginAction()
	{
		if (!Zend_Auth::getInstance()->hasIdentity()) {
			$form = new Default_Form_Login();
			if ($this->_request->isPost()) {
				$login = $this->_request->getPost('login','');
				$password = $this->_request->getPost('password','');
				if ($form->isValid($this->_request->getPost())) {
					$authAdapter = new Zend_Auth_Adapter_DbTable(
						Zend_Db_Table::getDefaultAdapter(),
						'user',
						'login',
						'password',
						'MD5(CONCAT(salt,?)) AND active = \'1\''
					);
					$authAdapter->setIdentity($login)->setCredential($password);
					$auth = Zend_Auth::getInstance();
					$result = $auth->authenticate($authAdapter);
					if ($result->isValid()) {
						$auth->getStorage()->write($authAdapter->getResultRowObject(null,'password'));
						$userModel = new UserModel();
						$userModel->update(array(
							'last_access' => new Zend_Db_Expr('CURRENT_TIMESTAMP')
						),"login = '{$login}'");
						$userModel->update(array(
							'salt' => new Zend_Db_Expr('MD5(last_access)'),
							'password' => new Zend_Db_Expr("MD5(CONCAT(MD5(last_access),'{$password}'))")
						),"login = '{$login}'");
						$this->flash->addMessage(array('success' => $this->view->translate('You have logged in.')));
						$this->redirect("/admin");
					} else {
						switch ($result->getCode()) {
							case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND:
								$form->getElement('login')->addError($this->view->translate('Login not found.'));
								break;
							case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID:
								$form->getElement('password')->addError($this->view->translate('Incorrect password.'));
								break;
							default:
								$this->view->messages[] = array('error' => $this->view->translate('Failed to log in.'));
								break;
						}
					}
				}
			}
			$this->view->form = $form;
		} else {
			$this->flash->addMessage(array('error' => $this->view->translate('You are already logged in.')));
			$this->redirect("/admin");
		}
	}

	/**
	 * Log out
	 */
	public function logoutAction()
	{
		if (Zend_Auth::getInstance()->hasIdentity()) {
			Zend_Auth::getInstance()->clearIdentity();
			$this->flash->addMessage(array('success' => $this->view->translate('You have logged out.')));
		} else {
			$this->flash->addMessage(array('error' => $this->view->translate('You are not logged in.')));
		}
		$this->redirect("/");
	}
}
