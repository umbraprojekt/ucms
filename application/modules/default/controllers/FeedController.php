<?php
use Mingos\uCMS\Model\NewsModel;

class FeedController extends Zend_Controller_Action {
	public function init() {
		// view variables
		$this->view->vars = array();
		$this->view->messages = array();

		// flash messenger
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) $this->view->messages += $this->flash->getMessages();
		$this->flash->clearMessages();
	}

	public function indexAction () {
		$this->getResponse()->setheader('Content-Type', 'application/rss+xml');

	    $this->view->layout()->disableLayout(true);
	    $this->_helper->viewRenderer->setNoRender(true);

		$model = new NewsModel();

		$nodes = $model->getAll(1);
		$entries = array();
		foreach ($nodes as $node) {
			$entries[] = $node['feed'];
		}

		$rss = array(
			'title' => 'uCMS',
			'link' => "http://".APPLICATION_DOMAIN,
			'charset' => 'utf-8',
			'entries' => $entries
		);

		echo Zend_Feed::importArray($rss, 'rss')->saveXML();
	}
}
