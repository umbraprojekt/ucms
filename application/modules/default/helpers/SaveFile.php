<?php
class Zend_Controller_Action_Helper_SaveFile extends Zend_Controller_Action_Helper_Abstract {
	/**
	 * @var Zend_Loader_PluginLoader
	 */
	public $pluginLoader;
	/**
	 * Constructor: initialize plugin loader
	 *
	 * @return void
	 */
	public function __construct () {
		$this->pluginLoader = new Zend_Loader_PluginLoader();
	}

	/**
	 * Save a file, possibly processing it, if applicable. Does not delete the source file.
	 * @param string|array $file source filename (e.g. temporary file created after a file upload) or the array containing the file data of the uploaded file
	 * @param string $destination destination path (no filename; original filename will be used)
	 * @param array $params Extra parametres:
	 *              [filename]         => (string) - new filename
	 * @return array information about the output file:
	 *               [mime] => mime type
	 *               [filename] => file name (normalised and unique)
	 *               [extension] => file extension
	 *               [type] => file type (audio|image)
	 */
	public function direct ($file,$destination,$params = array()) {
		if (!is_array($file)) {
			// check MIME type
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mime = finfo_file($finfo, $file);
			$filename = $file;
		} else {
			$mime = $file['type'];
			$filename = $file['tmp_name'];
			$pathinfo = pathinfo($file['name']);
			$params += array('filename' => $pathinfo['filename'], 'extension' => $pathinfo['extension']);
		}

		// act accordingly, based on the detected MIME type
		switch ($mime) {
			case 'image/jpeg': $ext = 'jpg'; $type = 'image'; $output = $this->image($filename,$destination,$ext,$params); break;
			case 'image/png': $ext = 'png'; $type = 'image'; $output = $this->image($filename,$destination,$ext,$params); break;
			case 'image/svg+xml': $ext = 'svg'; $type = 'document'; $output = $this->file($filename, $destination, $ext, $params); break;
			case 'audio/mpeg':
			case 'audio/x-mpeg':
			case 'audio/mp3':
			case 'audio/x-mp3':
			case 'audio/mpeg3':
			case 'audio/x-mpeg3':
			case 'audio/mpg':
			case 'audio/x-mpg':
			case 'audio/x-mpegaudio': $ext = 'mp3'; $type = 'audio'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/x-7z-compressed': $ext = '7z'; $type = 'archive'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/zip':
			case 'application/x-zip':
			case 'application/x-zip-compressed':
			case 'application/x-compress':
			case 'application/x-compressed':
			case 'multipart/x-zip': $ext = 'zip'; $type = 'archive'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/rar':
			case 'application/x-rar-compressed': $ext = 'rar'; $type = 'archive'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/x-gzip': $ext = 'gz'; $type = 'archive'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/x-bzip2': $ext = 'bz2'; $type = 'archive'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/msword': $ext = 'doc'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.ms-excel': $ext = 'xls'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.ms-powerpoint': $ext = 'ppt'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document': $ext = 'docx'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': $ext = 'xlsx'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.openxmlformats-officedocument.presentationml.presentation': $ext = 'pptx'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/pdf':
			case 'application/x-pdf':
			case 'application/acrobat':
			case 'applications/vnd.pdf':
			case 'text/pdf':
			case 'text/x-pdf': $ext = 'pdf'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.oasis.opendocument.text':
			case 'application/x-vnd.oasis.opendocument.text': $ext = 'odt'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.oasis.opendocument.spreadsheet':
			case 'application/x-vnd.oasis.opendocument.spreadsheet': $ext = 'ods'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'application/vnd.oasis.opendocument.presentation':
			case 'application/x-vnd.oasis.opendocument.presentation': $ext = 'odp'; $type = 'document'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/plain':
			case 'text/anytext': $ext = 'txt'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/html': $ext = 'html'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/css':
			case 'application/css-stylesheet': $ext = 'css'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/javascript':
			case 'application/x-javascript': $ext = 'js'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/x-c':
			case 'text/x-csrc':
			case 'application/x-c': $ext = 'c'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/x-c++': $ext = 'cpp'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			case 'text/x-chdr': $ext = 'h'; $type = 'text'; $output = $this->file($filename,$destination,$ext,$params); break;
			default: throw new Zend_Exception($this->_actionController->view->translate("Unrecognised MIME type: ".$mime,500)); break;
		}

		// return file info
		return array(
			'mime' => $mime,
			'filename' => $output,
			'extension' => $ext,
			'type' => $type
		);
	}

	/**
	 * Save image
	 */
	private function image ($filename,$destination,$ext,$params = array()) {
		$pathinfo = pathinfo($filename);
		$pathinfo['filename_new'] = array_key_exists('filename',$params) && $params['filename'] ?
			$this->getActionController()->view->normaliseString($params['filename']) :
			$this->getActionController()->view->normaliseString($pathinfo['filename'])
		;

		if (!is_dir($destination)) mkdir($destination,octdec('0777'),true);

		$output = $pathinfo['filename_new'];
		$i = 1;
		while (file_exists("{$destination}/{$output}.{$ext}")) {
			$output = $pathinfo['filename_new']."-{$i}";
			++$i;
		}
		if (file_exists("{$destination}/{$output}.{$ext}")) unlink("{$destination}/{$output}.{$ext}");
		copy($filename,"{$destination}/{$output}.{$ext}");

		return $output;
	}

	/**
	 * Save any file that does not require processing
	 */
	private function file ($filename,$destination,$ext,$params = array()) {
		$pathinfo = pathinfo($filename);
		$filename_new = array_key_exists('filename',$params) && $params['filename'] ?
			$this->getActionController()->view->normaliseString($params['filename']) :
			$this->getActionController()->view->normaliseString($pathinfo['filename'])
		;

		if (!is_dir($destination)) mkdir($destination,octdec('0777'),true);
		if (file_exists("{$destination}/{$filename_new}.{$ext}")) unlink("{$destination}/{$filename_new}.{$ext}");
		file_put_contents("{$destination}/{$filename_new}.{$ext}",file_get_contents($filename));

		$pathinfo = pathinfo($filename);
		return $filename_new;
	}
}
