<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'Zend/Loader/PluginLoader.php';
require_once 'Zend/Controller/Action/Helper/Abstract.php';
class Zend_Controller_Action_Helper_RenameDirectory extends Zend_Controller_Action_Helper_Abstract {
	/**
	 * @var Zend_Loader_PluginLoader
	 */
	public $pluginLoader;
	/**
	 * Constructor: initialize plugin loader
	 *
	 * @return void
	 */
	public function __construct () {
		$this->pluginLoader = new Zend_Loader_PluginLoader();
	}

	/**
	 * Renames a directory
	 * @param string $old old name
	 * @param string $new new name
	 * @return bool
	 */
	public function direct ($old,$new) {
		// nonexistent and nonempty directories are not permitted
		if (!is_dir($old) || !$this->isEmpty($new)) return false;

		// create destination directory
		if (!is_dir($new)) mkdir($new,octdec('0777'),true);
		if ($handle = opendir($old)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != '.' && $file != '..') {
					@rename("{$old}/{$file}","{$new}/{$file}");
				}
			}
			closedir($handle);
		} else return false;

		while ($this->isEmpty($old)) {
			@rmdir($old);
			$old = preg_replace('/\/[a-z\-]+$/U','',$old,1);
		}

		return true;
	}

	/**
	 * Check whether a given directory is empty
	 */
	private function isEmpty ($dir) {
		if (!is_dir($dir)) return true;
		$count = 0;
		if ($handle = opendir($dir)) {
			while (false !== ($file = readdir($handle))) {
				if ($file != '.' && $file != '..') {
					$count++;
				}
			}
			closedir($handle);
			return $count == 0;
		} else return false;
	}
}
