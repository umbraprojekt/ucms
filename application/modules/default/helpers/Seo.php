<?php
use Mingos\uCMS\Model\ConfigModel;

class Zend_Controller_Action_Helper_Seo extends Zend_Controller_Action_Helper_Abstract {
	/**
	 * Fetch SEO information
	 * @param string $keywords node-specific settings
	 * @param string $description node-specific settings
	 * @param string $robots node-specific settings
	 * @return array
	 */
	public function direct($keywords = '', $description = '', $robots = '')
	{
		// get view object
		$view = $this->getActionController()->view;
		// metatags & Google Analytics
		$configModel = new ConfigModel("seo");
		$seo = $configModel->getParams();

		if (!empty($keywords)) {
			if (!empty($seo['keywords'])) $seo['keywords'] .= ",{$keywords}";
			else $seo['keywords'] = $keywords;
		}
		if (!empty($description)) {
			$seo['description'] = $description;
		}

		// display
		if (!empty($seo['keywords']))
			$view->headMeta()->appendName('keywords',$seo['keywords']);
		if (!empty($seo['description']))
			$view->headMeta()->appendName('description',$seo['description']);
		if (!empty($robots))
			$view->headMeta()->appendName('robots',$robots);
		else if (!empty($seo['robots']))
			$view->headMeta()->appendName('robots',$seo['robots']);
		if (!empty($seo['verification']))
			$view->headMeta()->appendName('google-site-verification',$seo['verification']);
		if (!empty($seo['canonical']))
			$view->headLink(array('rel' => 'canonical','href' => "http://{$seo['canonical']}".(!empty($view->alias["alias"])?$view->url(array("alias" => $view->alias['alias']),"alias",true,false):"")));
		if (!empty($seo['analytics']))
			$view->headScript()->offsetSetScript(9999, "var _gaq=_gaq||[];_gaq.push(['_setAccount','{$seo['analytics']}']);_gaq.push(['_trackPageview']);(function(){var ga=document.createElement('script');ga.type='text/javascript';ga.async=true;ga.src=('https:'==document.location.protocol?'https://ssl':'http://www')+'.google-analytics.com/ga.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);})();");
		// add humans info
		$href = array_key_exists('canonical', $seo) && $seo['canonical'] ? "http://{$seo['canonical']}/humans.txt" : "http://{$_SERVER['HTTP_HOST']}/humans.txt";
		$view->headLink(array('rel' => 'author', 'href' => $href));
		// return data
		return $seo;
	}
}
