<?php
class Default_Model_Menu extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = "menu";

	/**
	 * Fetch menu items and construct a navigation object out of them.
	 *
	 * @param integer $id Menu ID to fetch
	 *
	 * @return Zend_Navigation
	 */
	public function getElements ($id) {
		$result = $this->_db->fetchAll(
			$this->select()
			->setIntegrityCheck(false)
			->from("menu_item")
			->where("mid = ?", $id)
			->order("left ASC")
		);

		// current uri
		list($uri) = explode("?",$_SERVER["REQUEST_URI"]);
		$uri = trim($uri,"/");

		$nav = new Zend_Navigation();
		$navItems = array();
		foreach ($result as $item) {
			$page = new Zend_Navigation_Page_Uri();
			$page->setLabel($item["label"]);
			$page->setUri($item["href"]);
			if (!empty($item["class"])) {
				$page->setClass($item["class"]);
			}
			$page->setActive($uri == trim($item["href"],"/"));

			// add to the menu items array
			$navItems[$item["id"]] = $page;

			// add to the actual menu structure
			if ($item["parent"] == 0) {
				$nav->addPage($page);
			} else {
				$navItems[$item["parent"]]->addPage($page);
			}
		}

		return $nav;
	}
}
