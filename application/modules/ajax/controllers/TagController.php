<?php
use Mingos\uCMS\Model\AclModel;

class Ajax_TagController extends Zend_Controller_Action {
	public function init() {
		// variables passed to the view object
		$this->view->vars = array();
		$this->view->vars['messages'] = array();

		// identity identity
		$auth = Zend_Auth::getInstance()->hasIdentity();
		if (!$auth) {
			$this->redirect("/default/identity/login");
		}
		$this->user = Zend_Auth::getInstance()->getIdentity();

		// flash messages
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) $this->view->vars['messages'] += $this->flash->getMessages();
		$this->flash->clearMessages();

		// title and layout
		$this->view->headTitle($this->view->translate('Administration'));
		$this->_helper->_layout->setLayout('admin');

		// make sure we're dealing with an AJAX call
		if (!$this->_request->isXmlHttpRequest()) {
			$this->flash->addMessage(array('error' => $this->view->translate('This controller is run via an AJAX call.')));
			$this->redirect("/admin");
		}

		// ACL
		if (!($this->acl = Zend_Registry::get("Cache")->load('acl'))) {
			$this->acl = AclModel::getInstance();
			Zend_Registry::get("Cache")->save($this->acl,'acl');
		}
		$this->view->vars['acl'] = $this->acl;
		$this->view->vars['identity'] = $this->user;
		if (!$this->acl->isAllowed($this->user->role,$this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action'))) {
			die($this->view->translate('You don\'t have enough privileges to access "%1$s".',$this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action')));
		}

		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);
	}

	/**
	 * Save tag order.
	 */
	public function orderAction () {
		$order = explode(',',$this->_request->getParam('order'));
		if (!$order) {
			die(Zend_Json::encode(array('result' => false, 'message' => 'invalid parametres')));
		}

		$model = new Model_Tag_List();
		$weight = 0;
		foreach($order as $item) {
			$item = intval($item);
			$model->update(array('weight' => $weight++),"id = {$item}");
		}
		die(Zend_Json::encode(array('result' => true)));
	}

	/**
	 * Search tags
	 */
	public function searchAction () {
		$tag = new Model_Tag_List();
		$tags = $tag->getTags(10,array('name' => $this->_request->getParam('tag'), 'language' => $this->_request->getParam('language')));
		$tags = array('tags' => $tags);
		echo Zend_Json::encode($tags);
    }

	/**
	 * Edit a field in a tag
	 */
	public function editAction () {
		$id = $this->_request->getParam('id',0);
		$value = $this->_request->getParam('value',null);
		$rel = $this->_request->getParam('rel',null);

		$filters = array(
			new Zend_Filter_StringTrim(),
			new Zend_Filter_StripTags(),
			new Zend_Filter_StripNewlines()
		);
		foreach($filters as $filter) {
			$value = $filter->filter($value);
		}

		if (empty($id) || empty($value) || empty($rel)) die(Zend_Json::encode(array('success' => false, 'message' => 'invalid parametres', 'id' => $id, 'value' => $value, 'rel' => $rel)));

		$model = new Model_Tag_List();

		switch($rel) {
			case 'name': $updated = $model->update(array('name' => $value),$model->getAdapter()->quoteInto('id = ?',$id)); break;
			case 'description': $updated = $model->update(array('description' => $value),$model->getAdapter()->quoteInto('id = ?',$id)); break;
			default: die(Zend_Json::encode(array('success' => false, 'message' => 'invalid parametres')));
		}

		if (!$updated) die(Zend_Json::encode(array('success' => false, 'message' => 'invalid parametres')));

		die(Zend_Json::encode(array('success' => true, 'val' => $value)));
	}
}
