<?php
use Mingos\uCMS\Model\PageModel;
use Mingos\uCMS\Model\NewsModel;
use Mingos\uCMS\Model\ProductModel;
use Mingos\uCMS\Model\ViewModel;
use Mingos\uCMS\Model\AclModel;

class Ajax_PageController extends Zend_Controller_Action {
	public function init() {
		// variables passed to the view object
		$this->view->vars = array();
		$this->view->vars['messages'] = array();

		// identity identity
		$auth = Zend_Auth::getInstance()->hasIdentity();
		if (!$auth) {
			$this->redirect("/default/identity/login");
		}
		$this->user = Zend_Auth::getInstance()->getIdentity();

		// flash messages
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) $this->view->vars['messages'] += $this->flash->getMessages();
		$this->flash->clearMessages();

		// title and layout
		$this->view->headTitle($this->view->translate('Administration'));
		$this->_helper->_layout->setLayout('admin');

		// make sure we're dealing with an AJAX call
		if (!$this->_request->isXmlHttpRequest()) {
			$this->flash->addMessage(array('error' => $this->view->translate('This controller is run via an AJAX call.')));
			$this->redirect("/admin");
		}

		// ACL
		if (!($this->acl = Zend_Registry::get("Cache")->load('acl'))) {
			$this->acl = AclModel::getInstance();
			Zend_Registry::get("Cache")->save($this->acl,'acl');
		}
		$this->view->vars['acl'] = $this->acl;
		$this->view->vars['identity'] = $this->user;
		if (!$this->acl->isAllowed($this->user->role,$this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action'))) {
			die($this->view->translate('You don\'t have enough privileges to access "%1$s".',$this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action')));
		}

		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);
	}

	/**
	 * Returns a list of available pages
	 *
	 * Used by the PageLink CKEditor plugin.
	 */
	public function ckeditorListAction () {
		$pageModel = new PageModel();
		$newsModel = new NewsModel();
		$productModel = new ProductModel();
		$viewModel = new ViewModel();

		$returnJson = array();

		foreach($pageModel->getAll() as $page) {
			array_push($returnJson, array($page['title'] . " (strona)", $page['alias']));
		}
		foreach($newsModel->getAll() as $page) {
			array_push($returnJson, array($page['title'] . " (post)", $page['alias']));
		}
		foreach($productModel->getAll() as $page) {
			array_push($returnJson, array($page['title'] . " (produkt)", $page['alias']));
		}
		foreach($viewModel->getAll() as $page) {
			array_push($returnJson, array($page['title'] . " (widok)", $page['alias']));
		}

		sort($returnJson);
		echo Zend_Json::encode(array('pages' => $returnJson));
	}
}
