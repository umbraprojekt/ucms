<?php
use Mingos\uCMS\Model\AclModel;
use Mingos\uCMS\Model\ImageModel;

class Ajax_ImageController extends Zend_Controller_Action {
	public function init() {
		// variables passed to the view object
		$this->view->vars = array();
		$this->view->vars['messages'] = array();

		// identity identity
		$auth = Zend_Auth::getInstance()->hasIdentity();
		if (!$auth) {
			$this->redirect("/default/identity/login");
		}
		$this->user = Zend_Auth::getInstance()->getIdentity();

		// flash messages
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) $this->view->vars['messages'] += $this->flash->getMessages();
		$this->flash->clearMessages();

		// title and layout
		$this->view->headTitle($this->view->translate('Administration'));
		$this->_helper->_layout->setLayout('admin');

		// make sure we're dealing with an AJAX call
		if (!$this->_request->isXmlHttpRequest()) {
			$this->flash->addMessage(array('error' => $this->view->translate('This controller is run via an AJAX call.')));
			$this->redirect("/admin");
		}

		// ACL
		if (!($this->acl = Zend_Registry::get("Cache")->load('acl'))) {
			$this->acl = AclModel::getInstance();
			Zend_Registry::get("Cache")->save($this->acl,'acl');
		}
		$this->view->vars['acl'] = $this->acl;
		$this->view->vars['identity'] = $this->user;
		if (!$this->acl->isAllowed($this->user->role,$this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action'))) {
			die($this->view->translate('You don\'t have enough privileges to access "%1$s".',$this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action')));
		}

		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);
	}

	/**
	 * Save image order.
	 */
	public function orderAction () {
		$order = explode(',',$this->_request->getParam('order'));
		$imageModel = new ImageModel();
		$weight = 0;
		foreach($order as $item) {
			$item = (int)$item;
			$imageModel->update(array('weight' => $weight++),"id = {$item}");
		}
	}

	/**
	 * Edit an image property
	 */
	public function editAction() {
		$val = $this->_request->getParam('val',null);
		$data = $this->_request->getParam('data',null);

		if ($data === null || $val === null) die(Zend_Json::encode(array('result' => false, 'message' => 'invalid parametres')));

		list($column, $id) = explode('-',$data);

		$imageModel = new ImageModel();

		switch($column) {
			case "filename":
				$val = trim($val,'/');
				if (empty($val)) die(Zend_Json::encode(array('result' => false, 'message' => 'invalid parametres')));
				$filename = $this->view->normaliseString($val);
				if (strlen($filename) > 128) die(Zend_Json::encode(array('result' => false, 'message' => 'filename too long')));
				$img = $imageModel->getById($id);
				if ($handle = opendir(PUBLIC_PATH."/upload/{$img['path']}")) {
					while (false !== ($file = readdir($handle))) {
						if (substr($file,0,strlen($img['filename'])) == $img['filename']) unlink(PUBLIC_PATH."/upload/{$img['path']}/{$file}");
					}
					closedir($handle);
				}
				if ($imageModel->update(array('filename' => $filename),"id = ".$imageModel->getAdapter()->quote($id))) {
					die(Zend_Json::encode(array('result' => true, 'val' => $filename)));
				} else {
					die(Zend_Json::encode(array('result' => false, 'message' => 'error while updating the filename')));
				}
				break;
			case "alt":
				$result = $imageModel->update(array(
					'alt' => $val
				), "id = ".$imageModel->getAdapter()->quote($id));
				if ($result != 0) {
					die(Zend_Json::encode(array('result' => true, 'val' => $val)));
				} else die(Zend_Json::encode(array('result' => false, 'message' => 'error while updating the alt attribute')));
				break;
			case "title":
				$result = $imageModel->update(array(
					'title' => $val
				), "id = ".$imageModel->getAdapter()->quote($id));
				if ($result != 0) {
					die(Zend_Json::encode(array('result' => true, 'val' => $val)));
				} else die(Zend_Json::encode(array('result' => false, 'message' => 'error while updating the title attribute')));
				break;
			case "cut":
				$result = $imageModel->update(array(
					'cut' => $val
				), "id = ".$imageModel->getAdapter()->quote($id));
				if ($result != 0) {
					die(Zend_Json::encode(array('result' => true, 'val' => $val)));
				} else die(Zend_Json::encode(array('result' => false, 'message' => 'error while updating the cut attribute')));
				break;
			case "url":
				if (!empty($val) && !preg_match('/^\//', $val) && !preg_match('/^https?:\/\//', $val)) {
					die(Zend_Json::encode(array('result' => false, 'message' => 'bad URL')));
				}
				$result = $imageModel->update(array(
					"url" => $val
				), "id = " . $imageModel->getAdapter()->quote($id));
				if ($result != 0) {
					die(Zend_Json::encode(array('result' => true, 'val' => $val)));
				} else die(Zend_Json::encode(array('result' => false, 'message' => 'error while updating the url attribute')));
				break;
			default:
				die(Zend_Json::encode(array('result' => false, 'message' => 'wrong column: '.$column)));
				break;
		}
	}

	/**
	 * Restore a manually cropped image
	 */
	public function resetAction () {
		$id = intval($this->_request->getParam('id',0));
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			die(Zend_Json::encode(array('success' => false, 'message' => 'invalid or missing ID parametre')));
		}

		$imageModel = new ImageModel();
		$image = $imageModel->getById($id);
		if (empty($image)) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			die(Zend_Json::encode(array('success' => false, 'message' => 'invalid or missing ID parametre')));
		}
		if ($handle = opendir(PUBLIC_PATH."/upload/{$image['path']}")) {
			while (false !== ($file = readdir($handle))) {
				if (substr($file,0,strlen($image['filename'])) == $image['filename']) unlink(PUBLIC_PATH."/upload/{$image['path']}/{$file}");
			}
			closedir($handle);
		}

		$imageModel->update(array('cut' => '0', 'crop' => ''),"id = {$id}");

		$this->flash->addMessage(array('success' => $this->view->translate('Image successfully reset.')));
		die(Zend_Json::encode(array('success' => true)));
	}
}
