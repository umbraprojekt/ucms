<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'Zend/View/Interface.php';
class Zend_View_Helper_DisplayMessages {
	/**
	 * @var Zend_View_Interface
	 */
	public $view;
	/**
	 * Display flash messages
	 * @param array $messages
	 * @return string output HTML
	 */
	public function displayMessages ($messages = array()) {
		if (!empty($messages)) {
			$ret = "<section id=\"messages\">";
			foreach($messages as $messageArray) {
				foreach($messageArray as $class => $message) {
					$ret .= "<p class=\"message $class\">$message</p>";
				}
			}
			$ret .= "</section>";
			$this->view->inlineScript()->appendScript(<<<EOS
				$(document).ready(function() {
					$("#messages").delay(10000).animate({ height: 0, opacity: 0 }, 1000, function() { $(this).remove(); });
				});
EOS
			);
			return $ret;
		} else return '';
	}

	/**
	 * Sets the view field
	 * @param $view Zend_View_Interface
	 */
	public function setView (Zend_View_Interface $view) {
		$this->view = $view;
	}
}

