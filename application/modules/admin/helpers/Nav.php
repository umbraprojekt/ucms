<?php
use Mingos\uCMS\Model\AclModel;

/**
 * Build the nav menu
 */
class Admin_Controller_Action_Helper_Nav extends Zend_Controller_Action_Helper_Abstract
{
	/**
	 * Build the main admin nav menu
	 *
	 * @return Zend_Navigation
	 */
	public function direct () {
		$acl = AclModel::getInstance();
		$request = $this->getActionController()->getRequest();

		$nav = new Zend_Navigation();

		// uCMS main menu branch
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "index", "action" => "index"))
			->setLabel("uCMS")
			->setClass("icon ucms")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "index", "action" => "config"))
				->setLabel("Content configuration")
				->setVisible($acl->isAllowed("admin/index/config"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "index", "action" => "seo"))
				->setLabel("Administer SEO settings")
				->setVisible($acl->isAllowed("admin/index/seo"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "image", "action" => "config"))
				->setLabel("Configure images")
				->setVisible($acl->isAllowed("admin/image/config"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "index", "action" => "clear-cache"))
				->setLabel("Clear cache")
				->setVisible($acl->isAllowed("admin/index/clear-cache"))
			;
			$page->addPage($subpage);

		// Content: pages
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "page", "action" => "index"))
			->setLabel("Simple pages")
			->setClass("icon page")
			->setVisible($acl->isAllowed("admin/page/index"))
			->setActive($request->getControllerName() == "page")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "page", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/page/add"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "page", "action" => "config"))
				->setLabel("Configure")
				->setVisible($acl->isAllowed("admin/page/config"))
			;
			$page->addPage($subpage);

		// Content: products
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "product", "action" => "index"))
			->setLabel("Products")
			->setClass("icon product")
			->setVisible($acl->isAllowed("admin/product/index"))
			->setActive($request->getControllerName() == "product")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "product", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/product/add"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "product", "action" => "config"))
				->setLabel("Configure")
				->setVisible($acl->isAllowed("admin/product/config"))
			;
			$page->addPage($subpage);

		// Content: news
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "news", "action" => "index"))
			->setLabel("News")
			->setClass("icon news")
			->setVisible($acl->isAllowed("admin/news/index"))
			->setActive($request->getControllerName() == "news")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "news", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/news/add"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "news", "action" => "config"))
				->setLabel("Configure")
				->setVisible($acl->isAllowed("admin/news/config"))
			;
			$page->addPage($subpage);

		// Content: galleries
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "gallery", "action" => "index"))
			->setLabel("Galleries")
			->setClass("icon gallery")
			->setVisible($acl->isAllowed("admin/gallery/index"))
			->setActive($request->getControllerName() == "gallery")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "gallery", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/gallery/add"))
			;
			$page->addPage($subpage);

		// Content: views
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "view", "action" => "index"))
			->setLabel("Views")
			->setClass("icon views")
			->setVisible($acl->isAllowed("admin/view/index"))
			->setActive($request->getControllerName() == "view")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "view", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/view/add"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "view", "action" => "config"))
				->setLabel("Configure")
				->setVisible($acl->isAllowed("admin/view/config"))
			;
			$page->addPage($subpage);

		// Content: blocks
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "block", "action" => "index"))
			->setLabel("Blocks")
			->setClass("icon block")
			->setVisible($acl->isAllowed("admin/block/index"))
			->setActive($request->getControllerName() == "block")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "block", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/block/add"))
			;
			$page->addPage($subpage);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "block", "action" => "config"))
				->setLabel("Configure")
				->setVisible($acl->isAllowed("admin/block/config"))
			;
			$page->addPage($subpage);

		// Comments
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "comment", "action" => "index"))
			->setLabel("Comments")
			->setClass("icon comments")
			->setVisible($acl->isAllowed("admin/comment/index"))
			->setActive($request->getControllerName() == "comment")
		;
		$nav->addPage($page);

		// Taxonomy
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "taxonomy", "action" => "index"))
			->setLabel("Taxonomy")
			->setClass("icon tag")
			->setVisible($acl->isAllowed("admin/taxonomy/index"))
			->setActive($request->getControllerName() == "taxonomy")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "taxonomy", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/taxonomy/add"))
			;
			$page->addPage($subpage);

		// Menu
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "menu", "action" => "index"))
			->setLabel("Menu")
			->setClass("icon menu")
			->setVisible($acl->isAllowed("admin/menu/index"))
			->setActive($request->getControllerName() == "menu")
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "menu", "action" => "add"))
				->setLabel("New menu")
				->setVisible($acl->isAllowed("admin/menu/add"))
			;
			$page->addPage($subpage);

		// Users
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "admin", "controller" => "user", "action" => "index"))
			->setLabel("Users")
			->setClass("icon users")
			->setVisible($acl->isAllowed("admin/user/index"))
		;
		$nav->addPage($page);

			$subpage = new Zend_Navigation_Page_Mvc();
			$subpage->setRoute("default")
				->setParams(array("module" => "admin", "controller" => "user", "action" => "add"))
				->setLabel("Add")
				->setVisible($acl->isAllowed("admin/menu/add"))
			;
			$page->addPage($subpage);

		// Misc links
		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "default", "controller" => "index", "action" => "index"))
			->setTarget("_blank")
			->setLabel("Home page")
			->setClass("icon home")
		;
		$nav->addPage($page);

		$page = new Zend_Navigation_Page_Mvc();
		$page->setRoute("default")
			->setParams(array("module" => "default", "controller" => "user", "action" => "logout"))
			->setLabel("Log out")
			->setClass("icon logout")
		;
		$nav->addPage($page);

		return $nav;
	}
}
