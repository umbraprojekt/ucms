<?php
use Mingos\uCMS\Model\GalleryModel;
use Mingos\uCMS\Model\ContentTypeModel;
use Mingos\uCMS\Model\ContentModel;

/**
 * Create a new gallery
 */
class Admin_Controller_Action_Helper_CreateGallery extends Zend_Controller_Action_Helper_Abstract
{
	public function direct (array $values)
	{
		$modelContent = new ContentModel();
		$content = $modelContent->createFromArray(ContentTypeModel::TYPE_GALLERY, $values);

		$modelNode = new GalleryModel();

		// node
		$insert = array(
			'id' => $content->id,
			'path' => !empty($values['path']) ? $values['path'] : $this->getActionController()->view->normaliseString($values['title'])
		);
		$modelNode->insert($insert);

		Zend_Registry::get('Cache')->clean(Zend_Cache::CLEANING_MODE_ALL);

		return $modelNode->getById($content->id);
	}
}
