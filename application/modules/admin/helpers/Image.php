<?php
use Mingos\uCMS\Model\GalleryModel;
use Mingos\uCMS\Model\FileModel;
use Mingos\uCMS\Model\ImageModel;

class Admin_Controller_Action_Helper_Image extends Zend_Controller_Action_Helper_Abstract
{
	public function direct()
	{
		throw new Zend_Controller_Exception("Cannot use this helper directly.");
	}

	/**
	 * Remove an image from the database and delete all uploaded files
	 * @param array $image
	 */
	public function deleteImage(array $image)
	{
		$this->deleteThumbnails($image);
		$this->deleteOriginal($image);

		$imageModel = new ImageModel();
		$imageModel->delete($imageModel->getAdapter()->quoteInto("id = ?", $image["id"]));

		$fileModel = new FileModel();
		$fileModel->delete($fileModel->getAdapter()->quoteInto("id = ?", $image["fid"]));

		// if the image was part of a gallery, update the gallery's image count
		if ($image["table"] == "node_gallery") {
			$modelGallery = new GalleryModel();
			$modelGallery->updateImageCount($image["nid"]);
		}
	}

	/**
	 * Delete all thumbnails for an image
	 * @param array $image
	 */
	public function deleteThumbnails(array $image)
	{
		if ($handle = opendir(PUBLIC_PATH."/upload/{$image['path']}")) {
			while (false !== ($file = readdir($handle))) {
				if (substr($file,0,strlen($image['filename'])) == $image['filename']) unlink(PUBLIC_PATH."/upload/{$image['path']}/{$file}");
			}
			closedir($handle);
		}
	}

	/**
	 * Delete original image
	 * @param array $image
	 */
	private function deleteOriginal(array $image)
	{
		unlink(DOC_ROOT . "/upload/{$image["path"]}/{$image["filename"]}.{$image["extension"]}");
	}
}
