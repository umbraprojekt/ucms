<?php
/* uCMS
 * Copyright (c) 2011-2012 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

require_once 'Zend/Loader/PluginLoader.php';
require_once 'Zend/Controller/Action/Helper/Abstract.php';

/**
 * Stores the referer in the session for later retrieval
 */
class Admin_Controller_Action_Helper_Referer extends Zend_Controller_Action_Helper_Abstract {
	/**
	 * Fetch referer
	 *
	 * @param string $default [optional] Default URI in case the referer has not been saved
	 *
	 * @return string
	 */
	public function direct ($default = null) {
		$referer = $default;
		if (array_key_exists("HTTP_REFERER",$_SESSION)) {
			$referer = $_SESSION["HTTP_REFERER"];
			unset($_SESSION["HTTP_REFERER"]);
		} else if (array_key_exists("HTTP_REFERER",$_SERVER)) {
			$referer = $_SERVER["HTTP_REFERER"];
		}

		return $referer;
	}

	/**
	 * Save the referer or hand-picked URI
	 *
	 * @param string $uri [optional] URI to save as referer
	 */
	public function set($uri = null) {
		if ($uri === null) {
			$uri = array_key_exists("HTTP_REFERER",$_SERVER) ? $_SERVER["HTTP_REFERER"] : null;
		}

		if ($uri !== null) {
			$_SESSION["HTTP_REFERER"] = $uri;
		}
	}
}
