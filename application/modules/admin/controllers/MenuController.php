<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\AddMenuForm;
use Mingos\uCMS\Admin\Form\AddMenuItemForm;
use Mingos\uCMS\Service\FileDeleter;
use Mingos\uCMS\Model\MenuModel;
use Mingos\uCMS\Model\MenuItemModel;
use Mingos\uCMS\Model\ContentModel;

class Admin_MenuController extends AbstractAdminController
{
	/**
	 * Display menu list
	 */
	public function indexAction () {
		$modelMenu = new MenuModel();
		$this->view->menu = $modelMenu->getAll();
	}

	/**
	 * Add a new menu
	 */
	public function addAction () {
		$form = new AddMenuForm();

		if ($this->_request->isPost()) {
			if ($this->_request->getParam('save',false)) {
				if ($form->isValid($this->_request->getPost())) {
					// add a menu
					$menuModel = new MenuModel();
					$menuModel->insert(array(
						"name" => $form->name->getValue()
					));

					$this->flash->addMessage(array('success' => $this->view->translate('New menu added.')));
					$this->redirect("/admin/menu");
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/menu");
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit a menu
	 */
	public function editAction () {
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->view->inlineScript()->appendScript("parent.window.location.reload();");
		}

		$form = new AddMenuForm();
		$modelMenu = new MenuModel();
		$menu = $modelMenu->fetchRow(array("id = ?" => $id));

		if($this->_request->isPost()) {
			if ($this->_request->getPost('save', false)) {
				if ($form->isValid($this->_request->getPost())) {
					$menu->name = $form->getValue('name');
					$menu->save();

					Zend_Registry::get('Cache')->remove('menu');

					$this->flash->addMessage(array('success' => $this->view->translate('Menu was updated: "%1$s".',$form->getValue('name'))));
					$this->redirect("/admin/menu");
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/menu");
			}
		} else {
			$form->populate($menu->toArray());
		}

		$this->view->form = $form;
	}

	/**
	 * Display & reorder menu items
	 */
	public function itemAction () {
		$mid = (int)$this->_request->getParam('mid',0);
		if (!$mid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu");
		}

		$menuModel = new MenuModel();
		$menuItemModel = new MenuItemModel();
		$this->view->menu = $menuModel->getById($mid);
		$this->view->items = $menuItemModel->getAll($mid);
	}

	/**
	 * Add a new menu item
	 */
	public function itemAddAction () {
		$mid = (int)$this->_request->getParam('mid',0);
		if (!$mid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu");
		}

		$form = new AddMenuItemForm();

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save')) {
				if ($this->_request->getParam('cid',0) != 0) {
					$form->getElement('href')->setRequired(false)->removeValidator('NotEmpty');
				}
				if ($form->isValid($this->_request->getPost())) {
					$values = $form->getValues();
					if ($values['cid'] == 0) {
						if (empty($values['label'])) $values['label'] = $values['href'];
						$values["cid"] = new Zend_Db_Expr("NULL");
					} else {
						$contentModel = new ContentModel();
						$node = $contentModel->getNode($values['cid']);
						if (empty($values['label'])) $values['label'] = $node['title'];
						$values['href'] = "/{$node['alias']}.html";
					}

					$modelMenuItem = new MenuItemModel();

					//check for parent element
					$parent = (int)$this->_request->getParam('parent',0);
					if ($parent) {
						$elt = $modelMenuItem->getById($parent);
						$values += array(
							'depth' => $elt['depth'] + 1,
							'left' => $elt['right'] + 1,
							'right' => $elt['right'] + 2,
							'parent' => $parent
						);
						$modelMenuItem->update(array(
							'left' => new Zend_Db_Expr("IF(`left` > {$elt['left']},`left` + 2,`left`)"),
							'right' => new Zend_Db_Expr("IF(`right` > {$elt['left']},`right` + 2,`right`)")
						), "mid = {$mid}");
					}

					$modelMenuItem->insert(array(
						'mid' => $mid
					) + $values);

					$this->flash->addMessage(array('success' => $this->view->translate('Menu item added successfully.')));
					$this->redirect("/admin/menu/item/mid/{$mid}");
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/menu/item/mid/{$mid}");
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit a menu item
	 */
	public function itemEditAction () {
		$mid = (int)$this->_request->getParam('mid',0);
		if (!$mid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu/item");
		}

		$iid = (int)$this->_request->getParam('iid',0);
		if (!$iid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu/item/mid/{$mid}");
		}

		$form = new AddMenuItemForm();

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save',false)) {
				if ($this->_request->getParam('cid',0) != 0) {
					$form->getElement('href')->setRequired(false)->removeValidator('NotEmpty');
				}
				if ($form->isValid($this->_request->getPost())) {
					$values = $form->getValues();
					if ($values['cid'] == 0) {
						if (empty($values['label'])) $values['label'] = $values['href'];
						$values["cid"] = new Zend_Db_Expr("NULL");
					} else {
						$contentModel = new ContentModel();
						$node = $contentModel->getNode($values['cid']);
						if (empty($values['label'])) $values['label'] = $node['title'];
						$values['href'] = "/{$node['alias']}.html";
					}

					$menuItemModel = new MenuItemModel();
					$menuItemModel->update($values, "id = {$iid}");

					Zend_Registry::get('Cache')->remove('menu');

					$this->flash->addMessage(array('success' => $this->view->translate('Menu item updated successfully.')));
					$this->redirect("/admin/menu/item/mid/{$mid}");
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/menu/item/mid/{$mid}");
			}
		} else {
			$menuItemModel = new MenuItemModel();
			$populate = $menuItemModel->getById($iid);
			if ($populate['cid'] != 0) unset($populate['href']);
			$form->populate($populate);
		}

		$this->view->form = $form;
	}

	/**
	 * Delete a menu item from the menu.
	 *
	 * Note: since the deletion is reversible, this action does not trigger a confirmation dialog!
	 */
	public function itemDeleteAction () {
		$mid = (int)$this->_request->getParam('mid',0);
		if (!$mid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu/index");
		}

		$iid = (int)$this->_request->getParam('iid',0);
		if (!$iid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu/item/mid/{$mid}");
		}

		$modelMenuItem = new MenuItemModel();
		$menuItem = $modelMenuItem->getById($iid);
		if ($menuItem["file_id"]) {
			$fileDeleter = new FileDeleter();
			$fileDeleter->deleteFile($menuItem["file_id"]);
		}

		$modelMenuItem->delete("id = {$iid}");
		$modelMenuItem->getAdapter()->query(
			"UPDATE menu_item AS m1
			LEFT JOIN menu_item AS m2 ON m1.parent = m2.id
			SET m1.parent = 0, m1.depth = 0
			WHERE m2.id IS NULL"
		);

		$this->flash->addMessage(array('success' => $this->view->translate('Menu item deleted successfully.')));
		$this->redirect("/admin/menu/item/mid/{$mid}");
	}

	/**
	 * Update the items order via AJAX
	 */
	public function itemAjaxAction () {
		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);

		$mid = (int)$this->_request->getParam('mid',0);
		if (!$mid) {
			echo Zend_Json::encode(array('result' => false, 'message' => 'unknown menu ID'));
			return;
		}

		$order = $this->_request->getParam('order',false);
		if (!$order) {
			echo Zend_Json::encode(array('result' => false, 'message' => 'unknown menu item order'));
			return;
		}

		$model = new MenuItemModel();

		$order = explode(';',$order);
		foreach($order as $item) {
			$values = explode(',',$item);
			$values[2] -= 1;
			$model->update(array(
				'parent' => $values[1],
				'depth' => $values[2],
				'left' => $values[3],
				'right' => $values[4]
			), "id = {$values[0]}");
		}

		echo Zend_Json::encode(array('result' => true));
	}

	public function itemImageAction()
	{
		if ($this->_request->isPut()) {
			$id = (int)$this->_request->getParam('id', 0);
			$menuItemModel = new MenuItemModel();
			$menuItem = $menuItemModel->getById($id);
			if (!$menuItem) {
				throw new Zend_Http_Exception("Unknown menu item.");
			}

			$body = Zend_Json::decode($this->_request->getRawBody());
			if (!array_key_exists("fileId", $body)) {
				throw new Zend_Http_Exception("Unknown file id.");
			}
			$fileId = $body["fileId"]; // may be integer or null

			if (!$fileId) {
				$fileDeleter = new FileDeleter();
				$fileDeleter->deleteFile($menuItem["file_id"]);
			}

			die($menuItemModel->update(array(
				"file_id" => $fileId
			), $menuItemModel->getAdapter()->quoteInto("id = ?", $id)));
		} else {
			$this->_response->setHttpResponseCode(405);
			$this->_response->sendResponse();
		}
		die();
	}

	/**
	 * Delete a menu
	 */
	public function deleteAction () {
		$mid = (int)$this->_request->getParam('mid',0);
		if (!$mid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/menu");
		}

		$model = new MenuModel();
		$model->delete("id = {$mid}");

		$this->flash->addMessage(array('success' => $this->view->translate('Menu successfully deleted.')));
		$this->redirect("/admin/menu");
	}
}
