<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\ConfigImageForm;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\ImageModel;

class Admin_ImageController extends AbstractAdminController
{
	/**
	 * Configuration options
	 */
	public function configAction()
	{
		$form = new ConfigImageForm();
		$modelConfig = new ConfigModel('image');

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$modelConfig->setParams($form->getValues());
				$this->flash->addMessage(array('success' => $this->view->translate('Confuration saved.')));
				$this->redirect("/admin/image/config");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		} else {
			$form->populate($modelConfig->getParams());
		}

		$this->view->form = $form;
	}

	/**
	 * Delete an image
	 */
	public function deleteAction () {
		$iid = (int)$this->_request->getParam('iid',0);
		if (!$iid) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/image/index");
		}

		$imageModel = new ImageModel();
		$image = $imageModel->getById($iid);

		$this->_helper->Image->deleteImage($image);

		$this->flash->addMessage(array('success' => $this->view->translate('Image successfully deleted.')));
		$this->redirect("/admin/gallery/detail/id/{$image['nid']}");
	}

	public function cutAction()
	{
		$id = intval($this->_request->getQuery('id', 0));
		if (!$id) {
			throw new Zend_Http_Exception("Param 'id' must be present.");
		}

		$cut = $this->_request->getQuery('cut', '0');
		if (!in_array($cut, array('0', 'S', 'E', 'N', 'W'))) {
			throw new Zend_Http_Exception("Invalid cut position.");
		}

		$imageModel = new ImageModel();
		$image = $imageModel->getById($id);
		if (!$image) {
			throw new Zend_Http_Exception("Invalid image requested.");
		}

		$imageModel->update(array(
			"cut" => $cut
		),"id = " . $imageModel->getAdapter()->quote($id));

		// remove old thumbnails
		if ($handle = opendir(PUBLIC_PATH."/upload/{$image['path']}")) {
			while (false !== ($file = readdir($handle))) {
				if (substr($file,0,strlen($image['filename'])) == $image['filename']) unlink(PUBLIC_PATH."/upload/{$image['path']}/{$file}");
			}
			closedir($handle);
		}

		// return OK status
		die();
	}

	public function deleteAjaxAction()
	{
		$id = intval($this->_request->getQuery('id', 0));
		if (!$id) {
			throw new Zend_Http_Exception("Param 'id' must be present.");
		}

		$imageModel = new ImageModel();
		$image = $imageModel->getById($id);
		if (!$image) {
			throw new Zend_Http_Exception("Invalid image requested.");
		}

		$this->_helper->Image->deleteImage($image);

		// return OK status
		die();
	}

	/**
	 * Get the URL of an image thumbnail or original image
	 * @throws Zend_Http_Exception
	 */
	public function urlAction()
	{
		$id = intval($this->_request->getQuery('id', 0));
		if (!$id) {
			throw new Zend_Http_Exception("Param 'id' must be present.");
		}

		$imageModel = new ImageModel();
		$image = $imageModel->getById($id);
		if (!$image) {
			throw new Zend_Http_Exception("Invalid image requested.");
		}

		// generate url
		$url = $this->view->image($image, $this->_request->getQuery());
		if (!$url) {
			throw new Zend_Http_Exception("Bad parametres.");
		}

		die($url);
	}
}
