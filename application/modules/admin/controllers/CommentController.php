<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\EditCommentForm;
use Mingos\uCMS\Model\CommentModel;

class Admin_CommentController extends AbstractAdminController
{
	/**
	 * Comments list
	 */
	public function indexAction () {
		$active = (int)$this->_request->getParam('active', -1);
		$column = $this->_request->getParam('column', 'created');
		$direction = $this->_request->getParam('direction', 'DESC');

		$model = new CommentModel();
		$select = $model->getAll(array(
			'column' => $column,
			'direction' => $direction,
			'paginator' => true,
			'active' => $active
		));
		$paginator = $this->makePaginator($select);

		$comments = array();
		foreach($paginator as $item) {
			$comments[$item['id']] = $model->getById($item['id']);
		}

		$this->view->paginator = $paginator;
		$this->view->column = $column;
		$this->view->direction = $direction;
		$this->view->limit = $paginator->getItemCountPerPage();
		$this->view->active = $active;
		$this->view->comments = $comments;
	}

	/**
	 * Comment list for a given content node
	 */
	public function contentAction ()
	{
		$id = (int)$this->_request->getParam("id",0);

		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/comment/index");
		}

		$commentModel = new CommentModel();
		$comments = $commentModel->getByCid($id);

		$this->view->comments = $comments;
		$this->view->title = $commentModel->getContentTitle($id);
	}

	/**
	 * Approve a comment
	 */
	public function approveAction () {
		$redirect = $_SERVER['HTTP_REFERER'];
		if (!$redirect) $redirect = "/admin/comment/index/active/0";

		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect($redirect);
		}

		$modelComment = new CommentModel();
		$modelComment->update(array(
			'active' => '1'
		), "id = {$id}");
		Zend_Registry::get('Cache')->clean(Zend_Cache::CLEANING_MODE_ALL);

		$this->flash->addMessage(array('success' => $this->view->translate('Comment approved.')));
		$this->redirect($redirect);
	}

	/**
	 * Edit a comment
	 */
	public function editAction() {
		$id = (int)$this->_request->getParam('id',0);
		$active = (int)$this->_request->getParam('active',-1);

		$redirect = "/admin/comment/index";
		if (in_array($active,array(0,1))) $redirect .= "/active/{$active}";

		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect($redirect);
		}

		$form = new EditCommentForm();
		$modelComment = new CommentModel();
		$comment = $modelComment->fetchRow($modelComment->getAdapter()->quoteInto("id = ?", $id));

		if($this->_request->isPost()) {
			if ($this->_request->getPost('save',0)) {
				if ($form->isValid($this->_request->getPost())) {
					$comment->body = $form->getValue('body');
					$comment->save();

					Zend_Registry::get('Cache')->clean(Zend_Cache::CLEANING_MODE_ALL);

					$this->flash->addMessage(array('success' => $this->view->translate('Comment updated successfully.')));
					$this->redirect($redirect);
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->flash->addMessage(array('info' => $this->view->translate('Action cancelled.')));
				$this->redirect($redirect);
			}
		} else {
			$form->populate(array('body' => $comment['body']));
		}
		$this->view->form = $form;
	}

	/**
	 * Delete a comment
	 */
	public function deleteAction () {
		$active = (int)$this->_request->getParam('active',-1);
		$id = (int)$this->_request->getParam('id',0);

		$redirect = "/admin/comment/index";
		if (in_array($active,array(0,1))) $redirect .= "/active/{$active}";

		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
		} else {
			$model = new CommentModel();
			$model->delete("id = {$id}");
			Zend_Registry::get('Cache')->clean(Zend_Cache::CLEANING_MODE_ALL);
			$this->flash->addMessage(array('success' => $this->view->translate('Comment successfully deleted.')));
		}

		$this->redirect($redirect);
	}
}
