<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\AddTaxonomyDictionaryForm;
use Mingos\uCMS\Admin\Form\AddTaxonomyItemForm;
use Mingos\uCMS\Model\TaxonomyModel;
use Mingos\uCMS\Model\TaxonomyDictionaryModel;

class Admin_TaxonomyController extends AbstractAdminController
{
	/**
	 * Taxonomy dictionaries list
	 */
	public function indexAction()
	{
		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();

		//fetch all dictionaries
		$dictionaries = $taxonomyDictionaryModel->getAll();

		$this->view->dictionaries = $dictionaries;
	}

	/**
	 * Add a new dictionary
	 */
	public function addAction()
	{
		$form = new AddTaxonomyDictionaryForm();

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
				$taxonomyDictionary = $taxonomyDictionaryModel->createRow(array(
					"title" => $form->title->getValue(),
					"slug" => $form->slug->getValue()
				));

				$taxonomyDictionary->save();

				$this->flash->addMessage(array('success' => $this->view->translate('New taxonomy dictionary created.')));
				$this->redirect("/admin/taxonomy");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit dictionary
	 */
	public function editAction()
	{
		$id = intval($this->_request->getParam("id", 0));
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$form = new AddTaxonomyDictionaryForm($id);

		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();

		$taxonomyDictionary = $taxonomyDictionaryModel->fetchRow(array("id = ?" => $id));

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$taxonomyDictionary->title = $form->title->getValue();
				$taxonomyDictionary->slug = $form->slug->getValue();
				$taxonomyDictionary->save();

				$this->flash->addMessage(array('success' => $this->view->translate('Taxonomy dictionary updated.')));
				$this->redirect("/admin/taxonomy");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		} else {
			$form->populate(array(
				"title" => $taxonomyDictionary->title,
				"slug" => $taxonomyDictionary->slug
			));
		}

		$this->view->form = $form;
		$this->view->taxonomyDictionary = $taxonomyDictionary;
	}

	/**
	 * Delete dictionary along with all terms
	 */
	public function deleteAction()
	{
		$id = intval($this->_request->getParam("id", 0));
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
		$taxonomyDictionary = $taxonomyDictionaryModel->fetchRow(array("id = ?" => $id));

		$taxonomyDictionary->delete();

		$this->flash->addMessage(array('success' => $this->view->translate('Taxonomy dictionary deleted.')));
		$this->redirect("/admin/taxonomy");
	}

	/**
	 * Order dictionaries
	 */
	public function orderAction()
	{
		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);

		$order = $this->_request->getQuery("order");
		if (!$order) {
			$this->getResponse()->setHttpResponseCode(500);
			$this->getResponse()->setBody("Invalid order.");
			return;
		}

		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
		$order = explode(",", $order);
		foreach ($order as $idx => $id) {
			$taxonomyDictionaryModel->update(array(
				"weight" => $idx
			), $taxonomyDictionaryModel->getAdapter()->quoteInto("id = ?", $id));
		}

		$this->getResponse()->setHttpResponseCode(200);
		$this->getResponse()->setBody("OK");
	}

	/**
	 * Show a list of terms within a dictionary
	 */
	public function termAction()
	{
		$id = intval($this->_request->getParam("id", 0));
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
		$taxonomyDictionary = $taxonomyDictionaryModel->fetchRow(array("id = ?" => $id));
		if (!$taxonomyDictionary) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$taxonomyModel = new TaxonomyModel();
		$taxonomyTerms = $taxonomyModel->getAllTerms($taxonomyDictionary->id);

		$this->view->dictionary = $taxonomyDictionary;
		$this->view->terms = $taxonomyTerms;
	}

	/**
	 * Add a new term to a dictionary
	 */
	public function termAddAction()
	{
		$id = intval($this->_request->getParam("id", 0));
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
		$taxonomyDictionary = $taxonomyDictionaryModel->fetchRow(array("id = ?" => $id));
		if (!$taxonomyDictionary) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$form = new AddTaxonomyItemForm();

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$taxonomyModel = new TaxonomyModel();
				$taxonomyTerm = $taxonomyModel->createRow(array(
					"title" => $form->title->getValue(),
					"slug" => $form->slug->getValue(),
					"did" => $taxonomyDictionary->id,
					"depth" => 1
				));
				$taxonomyTerm->save();

				$this->flash->addMessage(array('success' => $this->view->translate('New taxonomy term created.')));
				$this->redirect("/admin/taxonomy/term/id/{$taxonomyDictionary->id}");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit a term
	 */
	public function termEditAction()
	{
		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
		$taxonomyModel = new TaxonomyModel();

		$id = intval($this->_request->getParam("id", 0));
		$taxonomyDictionary = $taxonomyDictionaryModel->fetchRow(array("id = ?" => $id));
		if (!$taxonomyDictionary) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$tid = intval($this->_request->getParam("tid", 0));
		$taxonomyTerm = $taxonomyModel->fetchRow(array("id = ?" => $tid));
		if (!$taxonomyTerm) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy/term/id/{$taxonomyDictionary->id}");
		}

		$form = new AddTaxonomyItemForm($tid);

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$taxonomyTerm->title = $form->title->getValue();
				$taxonomyTerm->slug = $form->slug->getValue();
				$taxonomyTerm->save();

				$this->flash->addMessage(array('success' => $this->view->translate('Taxonomy term updated.')));
				$this->redirect("/admin/taxonomy/term/id/{$taxonomyDictionary->id}");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		} else {
			$form->populate(array(
				"title" => $taxonomyTerm->title,
				"slug" => $taxonomyTerm->slug
			));
		}

		$this->view->form = $form;
		$this->view->taxonomyTerm = $taxonomyTerm;
	}

	/**
	 * Delete a term
	 */
	public function termDeleteAction()
	{
		$taxonomyDictionaryModel = new TaxonomyDictionaryModel();
		$taxonomyModel = new TaxonomyModel();

		$id = intval($this->_request->getParam("id", 0));
		$taxonomyDictionary = $taxonomyDictionaryModel->fetchRow(array("id = ?" => $id));
		if (!$taxonomyDictionary) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy");
		}

		$tid = intval($this->_request->getParam("tid", 0));
		$taxonomyTerm = $taxonomyModel->fetchRow(array("id = ?" => $tid));
		if (!$taxonomyTerm) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/taxonomy/term/id/{$taxonomyDictionary->id}");
		}

		$taxonomyTerm->delete();

		$this->flash->addMessage(array('success' => $this->view->translate('Taxonomy term deleted.')));
		$this->redirect("/admin/taxonomy/term/id/{$taxonomyDictionary->id}");
	}

	/**
	 * Order terms within a dictionary
	 */
	public function termOrderAction()
	{
		$this->view->layout()->disableLayout(true);
		$this->_helper->viewRenderer->setNoRender(true);

		$order = $this->_request->getQuery("order");
		if (!$order) {
			$this->getResponse()->setHttpResponseCode(500);
			$this->getResponse()->setBody("Invalid order.");
			return;
		}

		$taxonomyModel = new TaxonomyModel();
		$order = explode(";", $order);
		foreach ($order as $val) {
			list($id, $left, $right, $parent, $depth) = explode(",", $val);
			$taxonomyModel->update(array(
				"left" => $left,
				"right" => $right,
				"parent" => $parent,
				"depth" => $depth
			), "id = " . intval($id));
		}

		$this->getResponse()->setHttpResponseCode(200);
		$this->getResponse()->setBody("OK");
	}
}
