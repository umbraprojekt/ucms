<?php
use Mingos\uCMS\Admin\Form\CreateAdminForm;
use Mingos\uCMS\Model\InstallModel;
use Mingos\uCMS\Model\UserModel;

class Admin_InstallController extends Zend_Controller_Action
{
	/**
	 * Begin install
	 */
	public function init () {
		$modelInstall = new InstallModel();
		$result = $modelInstall->verify();
		switch ($result) {
			case InstallModel::OK: $this->forward('done'); break;
			case InstallModel::NO_DATABASE: $this->forward('database'); break;
			case InstallModel::NO_TABLES: $this->forward('tables'); break;
			case InstallModel::USER_EMPTY: $this->forward('admin'); break;
			default: throw new Zend_Exception('Something unexpected has just happened o_O.',500);
		}
		$this->_helper->_layout->setLayout('install');
	}

	/**
	 * Display information about the necessity to create a database
	 */
	public function databaseAction () {
		$config = Zend_Registry::get("Config");
		$this->view->dbname = $config['resources']['db']['params']['dbname'];
	}

	/**
	 * Creates database tables
	 */
	public function tablesAction () {
		$model = new InstallModel();
		$model->createTables();
	}

	/**
	 * Adds a superuser
	 */
	public function adminAction () {
		$form = new CreateAdminForm();

		if ($this->_request->isPost() && $this->_request->getPost('adminSubmit',false)) {
			if ($form->isValid($this->_request->getPost())) {
				$salt = md5(time());
				$pass = md5($salt.$form->getValue('password'));
				$model = new UserModel();
				$model->insert(array(
					'login' => $form->getValue('login'),
					'salt' => $salt,
					'password' => $pass,
					'email' => $form->getValue('email'),
					'role' => 'god'
				));
				$this->redirect('/install');
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Well done screen
	 */
	public function doneAction () {}
}
