<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Filter\AliasFilter;
use Mingos\uCMS\Admin\Form\AddViewForm;
use Mingos\uCMS\Admin\Form\ConfigViewForm;
use Mingos\uCMS\Model\ViewModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\MenuItemModel;
use Mingos\uCMS\Model\AliasModel;
use Mingos\uCMS\Model\ContentTypeModel;
use Mingos\uCMS\Model\ContentModel;

class Admin_ViewController extends AbstractAdminController
{
	public function init()
	{
		parent::init();
		$this->nodeConfig = new ConfigModel("view");
	}

	/**
	 * Views list
	 */
	public function indexAction()
	{
		$column = $this->_request->getParam('column', 'title');
		$direction = $this->_request->getParam('direction', 'asc');

		$modelView = new ViewModel();
		$select = $modelView->getPaginator(ContentTypeModel::TYPE_VIEW, array('column' => $column, 'direction' => $direction));
		$paginator = $this->makePaginator($select);

		$nodes = array();
		foreach($paginator as $item) {
			$nodes[] = $modelView->getById($item['id']);
		}

		$this->view->paginator = $paginator;
		$this->view->column = $column;
		$this->view->direction = $direction;
		$this->view->limit = $paginator->getItemCountPerPage();
		$this->view->nodes = $nodes;
	}

	/**
	 * Configuration options
	 */
	public function configAction()
	{
		$this->configActionTemplate(new ConfigViewForm(), $this->nodeConfig);
	}

	/**
	 * Add a new view
	 */
	public function addAction()
	{
		$form = new AddViewForm();
		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save',false) || $this->_request->getPost('continue',false)) {
				if ($form->isValid($this->_request->getPost())) {
					$values = $form->getValues();

					// content
					$contentModel = new ContentModel();
					$content = $contentModel->createFromArray(ContentTypeModel::TYPE_VIEW, $values);

					// node
					$viewModel = new ViewModel();
					$node = $viewModel->createRow(array(
						'id' => $content->id,
						'published' => $values['published'],
						'input_format' => $values['input_format'],
						'teaser' => $values['teaser'],
						'body' => $values['body'],
						'seo_keywords' => $values['seo_keywords'],
						'seo_description' => $values['seo_description'],
						"seo_priority" => array_key_exists("seo_priority", $values) ? $values["seo_priority"] : $this->nodeConfig->getParam("seo_priority"),
						"seo_changefreq" => array_key_exists("seo_changefreq", $values) ? $values["seo_changefreq"] : $this->nodeConfig->getParam("seo_changefreq"),
						"seo_title" => array_key_exists("seo_title", $values) ? $values["seo_title"] : "",
						"seo_robots" => array_key_exists("seo_robots", $values) ? $values["seo_robots"] : "",
						"view_script" => array_key_exists("view_script", $values) ? $values["view_script"] : "product",
						"sitemap" => array_key_exists("sitemap", $values) ? $values["sitemap"] : "1",
						"search" => array_key_exists("search", $values) ? $values["search"] : "1"
					));
					$node->save();

					// alias
					$aliasModel = new AliasModel();
					$filter = new AliasFilter();
					$aliasModel->insert(array(
						'alias' => strlen($values['alias']) > 0 ? $values['alias'] : $filter->filter($this->view->getAliasFormat('view', $node->toArray(), array('title' => $content->title))),
						'id' => $content->id,
						'module' => $values['form_module'],
						'controller' => $values['form_controller'],
						'action' => $values['form_action']
					));

					if ($this->_request->getPost('save',false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('New content created.')));
						$this->redirect("/admin/view");
					} else {
						$this->flash->addMessage(array('success' => $this->view->translate('New content created.')));
						$this->redirect("/admin/view/edit/id/{$content->id}");
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/view");
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit a view
	 */
	public function editAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/view");
		}

		$form = new AddViewForm();
		$contentModel = new ContentModel();
		$viewModel = new ViewModel();

		$this->view->inlineScript()->appendScript("UCMS.set('cid', {$id});");

		if($this->_request->isPost()) {
			if ($this->_request->getPost('save',false) || $this->_request->getPost('continue',false)) {
				if ($form->isValid($this->_request->getPost())) {
					$values = $form->getValues();

					// content
					$content = $contentModel->updateFromArray($id, $values);

					// node
					$node = $viewModel->fetchRow(array("id = ?" => $id));
					$node->published = $values['published'];
					$node->input_format = $values['input_format'];
					$node->body = $values['body'];
					$node->teaser = $values['teaser'];
					$node->seo_keywords = $values['seo_keywords'];
					$node->seo_description = $values['seo_description'];
					$node->seo_priority = array_key_exists('seo_priority', $values) ? $values['seo_priority'] : $node->seo_priority;
					$node->seo_changefreq = array_key_exists('seo_changefreq', $values) ? $values['seo_changefreq'] : $node->seo_changefreq;
					$node->seo_title = array_key_exists('seo_title', $values) ? $values['seo_title'] : $node->seo_title;
					$node->seo_robots = array_key_exists('seo_robots', $values) ? $values['seo_robots'] : $node->seo_robots;
					$node->sitemap = array_key_exists('sitemap', $values) ? $values['sitemap'] : $node->sitemap;
					$node->search = array_key_exists('search', $values) ? $values['search'] : $node->search;
					$node->save();

					// alias
					$aliasModel = new AliasModel();
					$filter = new AliasFilter();
					$alias = $aliasModel->fetchRow(array("id = ?" => $id));
					$alias->alias = strlen($values['alias']) > 0 ? $values['alias'] : $filter->filter($this->view->getAliasFormat('view', $node, array('title' => $node['title'])));
					$alias->module = $values['form_module'];
					$alias->controller = $values['form_controller'];
					$alias->action = $values['form_action'];
					$alias->save();

					$modelMenuItem = new MenuItemModel();
					$modelMenuItem->updateHref($id);
					$modelMenuItem->updateLabel($id, $content['title'], $values['title']);

					if ($this->_request->getPost('save',false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('Content was updated: "%1$s".',$values['title'])));
						$this->redirect("/admin/view");
					} else {
						$this->view->messages[] = array('success' => $this->view->translate('Content updated.'));
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/view");
			}
		} else {
			$node = $viewModel->getById($id);
			$node['created'] = $node['created']->format('Y-m-d H:i:s');

			$mvc = array(
				'form_module' => $node['module'],
				'form_controller' => $node['controller'],
				'form_action' => $node['action']
			);

			$form->populate($node + $mvc);
		}
		$this->view->form = $form;
		$this->view->id = $id;
		$this->view->node = $node;
	}

	/**
	 * Delete a view
	 */
	public function deleteAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect('/admin/view/index');
		}

		$contentModel = new ContentModel();
		$contentModel->delete("id = {$id}");

		$modelMenuItem = new MenuItemModel();
		$modelMenuItem->delete("cid = {$id}");
		$modelMenuItem->updateOrphans();

		$this->flash->addMessage(array('success' => $this->view->translate('Content successfully deleted.')));
		$this->redirect('/admin/view/index');
	}

	public function galleryAction()
	{
		$this->galleryActionTemplate(new ViewModel());
	}
}
