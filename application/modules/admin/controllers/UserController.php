<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\AddUserForm;
use Mingos\uCMS\Model\UserModel;

class Admin_UserController extends AbstractAdminController
{
	/**
	 * Display users list
	 */
	public function indexAction () {
		$model = new UserModel();
		$users = $model->getAll(true);

		$this->view->paginator = $this->makePaginator($users);
	}

	/**
	 * Add a new identity
	 */
	public function addAction () {
		$form = new AddUserForm();

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save',false)) {
				if ($form->isValid($this->_request->getPost())) {
					$salt = md5(time());
					$pass = md5($salt.$form->getValue('password'));
					$model = new UserModel();
					$model->insert(array(
						'login' => $form->getValue('login'),
						'email' => $form->getValue('email'),
						'password' => $pass,
						'salt' => $salt,
						'role' => $form->getValue('role')
					));

					$this->flash->addMessage(array('success' => $this->view->translate('New user created successfully.')));

					// e-mail notification about creating an account
					if ($form->getValue('notify') == 1) {
						$mail = new Zend_Mail('utf-8');
						$mail
							->setFrom('admin@'.APPLICATION_DOMAIN)
							->addTo($form->getValue('email'))
							->setSubject($this->view->translate('User account at %1$s',APPLICATION_DOMAIN))
							->setBodyHtml(
								'<p>'.$this->view->translate('An account has been created for you at %1$s.',APPLICATION_DOMAIN).'</p>'.
								'<p>'.$this->view->translate('Login: %1$s, password: %2$s','<strong>'.$form->getValue('login').'</strong>','<strong>'.$pass.'</strong>').'</p>'.
								'<p>'.$this->view->translate('You may log in by typing the address %1$s in your browser\'s address bar.','http://'.APPLICATION_DOMAIN.'/admin').'</p>'.
								'<p><strong>'.$this->view->translate('This e-mail has been generated automatically. Please do not reply to it.').'</strong></p>'
							)
							->send()
						;
						$this->flash->addMessage(array('info' => $this->view->translate('The user %1$s has been notified by e-mail about the user account creation.',$form->getValue('login'))));
					}

					$this->redirect("/admin/user");
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/user");
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit an existing user
	 */
	public function editAction () {
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/user");
		}

		$form = new AddUserForm($id);
		$form->removeElement('notify');

		$model = new UserModel();
		$user = $model->getById($id);

		$canEditData = $this->acl->inheritsRole($this->identity->role, $user['role']) || $this->identity->role == $user['role'] || $this->identity->id == $user['id'];
		$canEditRole = $this->acl->inheritsRole($this->identity->role, $user['role']) && in_array($this->identity->role,array('admin','god'));

		if (!$canEditData) {
			$this->flash->addMessage(array('error' => $this->view->translate('Insufficient privileges.')));
			$this->view->inlineScript()->appendScript("parent.window.location.reload();");
		}

		if (!$canEditRole) {
			$form->removeElement('role');
		}

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save',false)) {
				if ($form->isValid($this->_request->getPost())) {
					if ($canEditRole) $role = $form->getValue('role');
					else $role = $user['role'];

					$model->update(array(
						'login' => $form->getValue('login'),
						'email' => $form->getValue('email'),
						'role' => $role
					),"id = {$id}");

					$password = $form->getValue('password');
					if (!empty($password)) {
						$salt = md5(time());
						$pass = md5($salt.$password);
						$model->update(array(
							'salt' => $salt,
							'password' => $pass
						),"id = {$id}");
					}

					$this->flash->addMessage(array('success' => $this->view->translate('User updated successfully.')));
					$this->redirect("/admin/user");
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/user");
			}
		} else {
			$model = new UserModel();
			$user = $model->getById($id);
			$populate = array(
				'login' => $user['login'],
				'email' => $user['email']
			);
			if ($canEditRole) $populate += array(
				'role' => $user['role']
			);
			$form->populate($populate);
		}

		$this->view->form = $form;
	}

	/**
	 * Delete a identity
	 */
	public function deleteAction () {
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/user");
		}

		$model = new UserModel();
		$user = $model->getById($id);

		if (!$this->acl->inheritsRole($this->identity->role, $user['role'])) {
			$this->flash->addMessage(array('error' => $this->view->translate('Insufficient privileges.')));
			$this->redirect("/admin/user");
		}

		$model = new UserModel();
		$model->getAdapter()->query("UPDATE user SET active = ? WHERE id = ?",array(0,$id));

		$this->flash->addMessage(array('success' => $this->view->translate('User deleted.')));
		$this->redirect("/admin/user");
	}
}
