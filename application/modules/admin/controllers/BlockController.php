<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\AddBlockForm;
use Mingos\uCMS\Model\BlockModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\ContentModel;
use Mingos\uCMS\Model\ContentTypeModel;

class Admin_BlockController extends AbstractAdminController
{
	public function init()
	{
		parent::init();
		$this->nodeConfig = $this->view->nodeConfig = new ConfigModel("block");
	}

	/**
	 * Blocks list
	 */
	public function indexAction()
	{
		$column = $this->_request->getParam('column', 'title');
		$direction = $this->_request->getParam('direction', 'asc');

		$modelBlock = new BlockModel();
		$select = $modelBlock->getPaginator(ContentTypeModel::TYPE_BLOCK, array('column' => $column, 'direction' => $direction));
		$paginator = $this->makePaginator($select);

		$nodes = array();
		foreach($paginator as $item) {
			$nodes[] = $modelBlock->getById($item['id']);
		}

		$this->view->paginator = $paginator;
		$this->view->column = $column;
		$this->view->direction = $direction;
		$this->view->limit = $paginator->getItemCountPerPage();
		$this->view->nodes = $nodes;
	}

	/**
	 * Add a new block
	 */
	public function addAction()
	{
		$form = new AddBlockForm();

		if ($this->_request->isPost()) {
			// validate the form
			$form->isValid($this->_request->getPost());

			// validate the image
			$file = $this->_validateFileUpload($form, "image");

			if (!$form->hasErrors()) {
				$values = $form->getValues();

				// content
				$contentModel = new ContentModel();
				$content = $contentModel->createFromArray(ContentTypeModel::TYPE_BLOCK, $values);

				// node
				$blockModel = new BlockModel();
				$node = $blockModel->createRow(array(
					'id' => $content->id,
					'input_format' => $values['input_format'],
					'body' => $values['body']
				));
				$node->save();

				$this->_receiveImageUpload($file, array("id" => $content->id), "node_block", "block");

				$this->flash->addMessage(array('success' => $this->view->translate('New content was added: "%1$s".', $content->content_title)));
				$this->redirect("/admin/block");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit block
	 */
	public function editAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/block");
		}

		$form = new AddBlockForm();
		$modelBlock = new BlockModel();
		$modelContent = new ContentModel();

		$this->view->inlineScript()->appendScript("UCMS.set('cid', {$id});");

		if($this->_request->isPost()) {
			// validate the form
			$form->isValid($this->_request->getPost());

			// validate the image
			$file = $this->_validateFileUpload($form, "image");

			if (!$form->hasErrors()) {
				$values = $form->getValues();

				// content
				$modelContent->updateFromArray($id, $values);

				// node
				$node = $modelBlock->fetchRow(array("id = ?" => $id));
				$node->body = $values["body"];
				$node->input_format = $values["input_format"];
				$node->save();

				$this->_receiveImageUpload($file, array("id" => $id), "node_block", "block");

				$this->flash->addMessage(array('success' => $this->view->translate('Content was updated: "%1$s".',$values['title'])));
				$this->redirect("/admin/block");
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		} else {
			$form->populate($modelBlock->getById($id));
		}

		$this->view->form = $form;
		$this->view->id = $id;
		$this->view->node = $modelBlock->getById($id);
	}

	/**
	 * Delete block
	 */
	public function deleteAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/block");
		}

		$contentModel = new ContentModel();
		$contentModel->delete("id = {$id}");

		$this->flash->addMessage(array('success' => $this->view->translate('Content successfully deleted.')));
		$this->redirect("/admin/block");
	}

	public function galleryAction()
	{
		$this->galleryActionTemplate(new BlockModel());
	}

	/**
	 * Returns a list of available blocks via AJAX (for CKEditor block macro plugin).
	 */
	public function listAjaxAction()
	{
		$model = new BlockModel();
		$blocks = $model->getAll();

		$returnJson = array();
		foreach($blocks as $block) {
			array_push($returnJson, array($block["content_title"], $block["id"]));
		}
		$this->_response->setHeader("Content-Type", "application/json");
		$this->_response->setBody(Zend_Json::encode(array("blocks" => $returnJson)));
		$this->_response->sendResponse();
		die;
	}
}
