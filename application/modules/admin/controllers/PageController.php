<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Filter\AliasFilter;
use Mingos\uCMS\Admin\Form\AddPageForm;
use Mingos\uCMS\Admin\Form\ConfigPageForm;
use Mingos\uCMS\Model\PageModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\MenuItemModel;
use Mingos\uCMS\Model\AliasModel;
use Mingos\uCMS\Model\AutopublishModel;
use Mingos\uCMS\Model\ContentTypeModel;
use Mingos\uCMS\Model\ContentModel;

class Admin_PageController extends AbstractAdminController
{
	public function init()
	{
		parent::init();
		$this->nodeConfig = $this->view->nodeConfig = new ConfigModel("page");
	}

	/**
	 * Pages list
	 */
	public function indexAction () {
		$column = $this->_request->getParam('column', 'title');
		$direction = $this->_request->getParam('direction', 'asc');

		$model = new PageModel();
		$select = $model->getPaginator(ContentTypeModel::TYPE_PAGE, array('column' => $column, 'direction' => $direction));
		$paginator = $this->makePaginator($select);

		$nodes = array();
		foreach($paginator as $item) {
			$nodes[] = $model->getById($item['id']);
		}

		$this->view->paginator = $paginator;
		$this->view->column = $column;
		$this->view->direction = $direction;
		$this->view->limit = $paginator->getItemCountPerPage();
		$this->view->nodes = $nodes;
	}

	/**
	 * Configuration options
	 */
	public function configAction()
	{
		$this->configActionTemplate(new ConfigPageForm(), $this->nodeConfig);
	}

	/**
	 * Add a new page
	 */
	public function addAction () {
		$form = new AddPageForm();

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save',false) || $this->_request->getPost('continue',false)) {
				// validate the form
				$form->isValid($this->_request->getPost());

				// validate the image
				$file = $this->_validateFileUpload($form, "image");

				if (!$form->hasErrors()) {
					$values = $form->getValues();

					// content
					$contentModel = new ContentModel();
					$content = $contentModel->createFromArray(ContentTypeModel::TYPE_PAGE, $values);

					// node
					$nodeModel = new PageModel();
					$node = $nodeModel->createRow(array(
						'id' => $content->id,
						'published' => $values['published'],
						'input_format' => $values['input_format'],
						'teaser' => $values['teaser'],
						'body' => $values['body'],
						'seo_keywords' => $values['seo_keywords'],
						'seo_description' => $values['seo_description'],
						"seo_priority" => array_key_exists("seo_priority", $values) ? $values["seo_priority"] : $this->nodeConfig->getParam("seo_priority"),
						"seo_changefreq" => array_key_exists("seo_changefreq", $values) ? $values["seo_changefreq"] : $this->nodeConfig->getParam("seo_changefreq"),
						"seo_title" => array_key_exists("seo_title", $values) ? $values["seo_title"] : "",
						"seo_robots" => array_key_exists("seo_robots", $values) ? $values["seo_robots"] : "",
						"view_script" => array_key_exists("view_script", $values) ? $values["view_script"] : "page",
						"sitemap" => array_key_exists("sitemap", $values) ? $values["sitemap"] : "1",
						"search" => array_key_exists("search", $values) ? $values["search"] : "1"
					));
					$node->save();

					// autopublish
					$modelAutopublish = new AutopublishModel();
					$modelAutopublish->set($content->id, 1, $values["autopublish"]);
					$modelAutopublish->set($content->id, 0, $values["autounpublish"]);

					// alias
					$aliasModel = new AliasModel();
					$filter = new AliasFilter();
					$alias = $aliasModel->createRow(array(
						'alias' => strlen($values['alias']) > 0 ? $values['alias'] : $filter->filter($this->view->getAliasFormat('page', $node->toArray(), array('title' => $content->title))),
						'id' => $content->id,
						'module' => 'default',
						'controller' => 'index',
						'action' => 'page'
					));
					$alias->save();

					$this->_receiveImageUpload($file, array("id" => $content->id), "node_page", "strona");

					if ($this->_request->getPost('save',false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('New content created.')));
						$this->redirect("/admin/page");
					} else {
						$this->flash->addMessage(array('success' => $this->view->translate('New content created.')));
						$this->redirect("/admin/page/edit/id/{$content->id}");
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/page");
			}
		} else {
			$configModel = new ConfigModel('page');
			if ($configModel->getParam('seo_changefreq') == '1') {
				$form->getElement('seo_changefreq')->setValue($this->nodeConfig['seo_changefreq']);
			}
			if ($configModel->getParam('seo_priority') == '1') {
				$form->getElement('seo_priority')->setValue($this->nodeConfig['seo_priority']);
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit page
	 */
	public function editAction () {
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/page");
		}
		$form = new AddPageForm();
		$contentModel = new ContentModel();
		$pageModel = new PageModel();

		$this->view->inlineScript()->appendScript("UCMS.set('cid', {$id});");

		if($this->_request->isPost()) {
			if ($this->_request->getParam('save',false) || $this->_request->getParam('continue',false)) {
				// validate the form
				$form->isValid($this->_request->getPost());

				// validate the image
				$file = $this->_validateFileUpload($form, "image");

				if (!$form->hasErrors()) {
					$values = $form->getValues();

					// content
					$content = $contentModel->updateFromArray($id, $values);

					// node
					$node = $pageModel->fetchRow(array("id = ?" => $id));
					$node->published = $values['published'];
					$node->input_format = $values['input_format'];
					$node->body = $values['body'];
					$node->teaser = $values['teaser'];
					$node->seo_keywords = $values['seo_keywords'];
					$node->seo_description = $values['seo_description'];
					$node->seo_priority = array_key_exists('seo_priority', $values) ? $values['seo_priority'] : $node->seo_priority;
					$node->seo_changefreq = array_key_exists('seo_changefreq', $values) ? $values['seo_changefreq'] : $node->seo_changefreq;
					$node->seo_title = array_key_exists('seo_title', $values) ? $values['seo_title'] : $node->seo_title;
					$node->seo_robots = array_key_exists('seo_robots', $values) ? $values['seo_robots'] : $node->seo_robots;
					$node->sitemap = array_key_exists('sitemap', $values) ? $values['sitemap'] : $node->sitemap;
					$node->search = array_key_exists('search', $values) ? $values['search'] : $node->search;
					$node->save();

					// autopublish
					$modelAutopublish = new AutopublishModel();
					$modelAutopublish->set($id, 1, $values["autopublish"]);
					$modelAutopublish->set($id, 0, $values["autounpublish"]);

					// alias
					$aliasModel = new AliasModel();
					$filter = new AliasFilter();
					$aliasModel->updateAlias($id, strlen($values['alias']) > 0
						? $values['alias']
						: $filter->filter($this->view->getAliasFormat('page', $node->toArray(), array('title' => $content->title))));

					$menuItemModel = new MenuItemModel();
					$menuItemModel->updateHref($id);
					$menuItemModel->updateLabel($id, $content['title'], $values['title']);

					$this->_receiveImageUpload($file, $node->toArray(), "node_page", "strona");

					if ($this->_request->getPost('save',false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('Content updated.')));
						$this->redirect("/admin/page");
					} else {
						$this->view->messages[] = array('success' => $this->view->translate('Content updated.'));
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/page");
			}
		} else {
			$node = $pageModel->getById($id);
			$node['created'] = $node['created']->format('Y-m-d H:i:s');
			$node['autopublish'] = $node["autopublish"] ? $node["autopublish"]->format('Y-m-d H:i:s') : "";
			$node['autounpublish'] = $node["autounpublish"] ? $node["autounpublish"]->format('Y-m-d H:i:s') : "";

			$form->populate($node);
		}
		$this->view->form = $form;
		$this->view->id = $id;
		$this->view->node = $pageModel->getById($id);
	}

	/**
	 * Delete page
	 */
	public function deleteAction () {
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/page/index");
		}

		$contentModel = new ContentModel();
		$contentModel->delete("id = {$id}");

		$menuItemModel = new MenuItemModel();
		$menuItemModel->delete("cid = {$id}");
		$menuItemModel->updateOrphans();

		$this->flash->addMessage(array('success' => $this->view->translate('Content deleted.')));
		$this->redirect("/admin/page/index");
	}

	public function taxonomyAction()
	{
		$this->taxonomyActionTemplate(new PageModel());
	}

	public function relatedAction()
	{
		$this->relatedActionTemplate();
	}

	public function galleryAction()
	{
		$this->galleryActionTemplate(new PageModel());
	}
}
