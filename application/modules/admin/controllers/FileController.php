<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\AddFileForm;
use Mingos\uCMS\Model\FileModel;

class Admin_FileController extends AbstractAdminController
{
	/**
	 * Add a new file
	 */
	public function addAction()
	{
		$form = new AddFileForm();

		if ($this->_request->isPost()) {
			$file = $this->_validateFileUpload($form, "file");
			$id = $this->_receiveFileUpload($file, "files");
			$this->_response->setHttpResponseCode(201);
			$this->_response->setBody(Zend_Json::encode(array("id" => intval($id))));
			$this->_response->setHeader("Content-Type", "application/json");
			$this->_response->sendResponse();
		} else {
			$this->_response->setHttpResponseCode(405);
			$this->_response->sendResponse();
		}

		die;
	}

	/**
	 * Fetch data about a file
	 * @throws Zend_Controller_Response_Exception
	 */
	public function fileAction()
	{
		$id = $this->_request->getQuery("id", 0);
		$model = new FileModel();
		$file = $model->getById($id);

		if ($file) {
			$this->_response->setBody(Zend_Json::encode($file));
			$this->_response->setHeader("Content-Type", "application/json");
		} else {
			$this->_response->setHttpResponseCode(404);
		}
		$this->_response->sendResponse();
		die();
	}
}
