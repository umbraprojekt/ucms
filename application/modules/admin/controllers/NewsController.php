<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Filter\AliasFilter;
use Mingos\uCMS\Admin\Form\AddNewsForm;
use Mingos\uCMS\Admin\Form\ConfigNewsForm;
use Mingos\uCMS\Model\NewsModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\AliasModel;
use Mingos\uCMS\Model\AutopublishModel;
use Mingos\uCMS\Model\ContentTypeModel;
use Mingos\uCMS\Model\ContentModel;

class Admin_NewsController extends AbstractAdminController
{
	public function init()
	{
		parent::init();
		$this->nodeConfig = $this->view->nodeConfig = new ConfigModel('news');
	}

	/**
	 * News list
	 */
	public function indexAction()
	{
		$column = $this->_request->getParam('column', 'created');
		$direction = $this->_request->getParam('direction', 'desc');

		$model = new NewsModel();
		$select = $model->getPaginator(ContentTypeModel::TYPE_NEWS, array('column' => $column, 'direction' => $direction));
		$paginator = $this->makePaginator($select);

		$nodes = array();
		foreach ($paginator as $item) {
			$nodes[] = $model->getById($item['id']);
		}

		$this->view->paginator = $paginator;
		$this->view->column = $column;
		$this->view->direction = $direction;
		$this->view->limit = $paginator->getItemCountPerPage();
		$this->view->nodes = $nodes;
	}

	/**
	 * Configuration options
	 */
	public function configAction()
	{
		$this->configActionTemplate(new ConfigNewsForm(), $this->nodeConfig);
	}

	/**
	 * Add a new news item
	 */
	public function addAction()
	{
		$form = new AddNewsForm();
		if ($this->_request->isPost()) {
			if ($this->_request->getParam('save', false) || $this->_request->getParam('continue', false)) {
				// validate the form
				$form->isValid($this->_request->getPost());

				// validate the image
				$file = $this->_validateFileUpload($form, "image");

				if (!$form->hasErrors()) {
					$values = $form->getValues();

					// content
					$contentModel = new ContentModel();
					$content = $contentModel->createFromArray(ContentTypeModel::TYPE_NEWS, $values);

					// node
					$nodeModel = new NewsModel();
					$node = $nodeModel->createRow(array(
						'id' => $content->id,
						'published' => $values['published'],
						'input_format' => $values['input_format'],
						'teaser' => $values['teaser'],
						'body' => $values['body'],
						'seo_keywords' => $values['seo_keywords'],
						'seo_description' => $values['seo_description'],
						"seo_priority" => array_key_exists("seo_priority", $values) ? $values["seo_priority"] : $this->nodeConfig->getParam("seo_priority"),
						"seo_changefreq" => array_key_exists("seo_changefreq", $values) ? $values["seo_changefreq"] : $this->nodeConfig->getParam("seo_changefreq"),
						"seo_title" => array_key_exists("seo_title", $values) ? $values["seo_title"] : "",
						"seo_robots" => array_key_exists("seo_robots", $values) ? $values["seo_robots"] : "",
						"view_script" => array_key_exists("view_script", $values) ? $values["view_script"] : "page",
						"sitemap" => array_key_exists("sitemap", $values) ? $values["sitemap"] : "1",
						"search" => array_key_exists("search", $values) ? $values["search"] : "1"
					));
					$node->save();

					// autopublish
					$modelAutopublish = new AutopublishModel();
					$modelAutopublish->set($content->id, 1, $values["autopublish"]);
					$modelAutopublish->set($content->id, 0, $values["autounpublish"]);

					// alias
					$aliasModel = new AliasModel();
					$filter = new AliasFilter();
					$aliasModel->insert(array(
						'alias' => strlen($values['alias']) > 0 ? $values['alias'] : $filter->filter($this->view->getAliasFormat('news', $node->toArray(), array('title' => $content->title))),
						'id' => $content->id,
						'module' => 'default',
						'controller' => 'index',
						'action' => 'news'
					));

					$this->_receiveImageUpload($file, $node->toArray(), "node_news", "news");

					if ($this->_request->getParam('save', false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('New content created.')));
						$this->redirect("/admin/news/index");
					} else {
						$this->flash->addMessage(array('success' => $this->view->translate('New content created.')));
						$this->redirect("/admin/news/edit/id/{$content->id}");
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/news/index");
			}
		}

		$this->view->form = $form;
	}

	/**
	 * Edit a news item
	 */
	public function editAction()
	{
		$id = (int)$this->_request->getParam('id', 0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->view->inlineScript()->appendScript("parent.window.location.reload();");
		}
		$form = new AddNewsForm();
		$contentModel = new ContentModel();
		$newsModel = new NewsModel();

		$this->view->inlineScript()->appendScript("UCMS.set('cid', {$id});");

		if ($this->_request->isPost()) {
			if ($this->_request->getParam('save', false) || $this->_request->getParam('continue', false)) {
				// validate the form
				$form->isValid($this->_request->getPost());

				// validate the image
				$file = $this->_validateFileUpload($form, "image");

				if (!$form->hasErrors()) {
					$values = $form->getValues();

					$content = $contentModel->updateFromArray($id, $values);

					// node
					$node = $newsModel->fetchRow(array("id = ?" => $id));
					$node->published = $values['published'];
					$node->input_format = $values['input_format'];
					$node->body = $values['body'];
					$node->teaser = $values['teaser'];
					$node->seo_keywords = $values['seo_keywords'];
					$node->seo_description = $values['seo_description'];
					$node->seo_priority = array_key_exists('seo_priority', $values) ? $values['seo_priority'] : $node->seo_priority;
					$node->seo_changefreq = array_key_exists('seo_changefreq', $values) ? $values['seo_changefreq'] : $node->seo_changefreq;
					$node->seo_title = array_key_exists('seo_title', $values) ? $values['seo_title'] : $node->seo_title;
					$node->seo_robots = array_key_exists('seo_robots', $values) ? $values['seo_robots'] : $node->seo_robots;
					$node->sitemap = array_key_exists('sitemap', $values) ? $values['sitemap'] : $node->sitemap;
					$node->search = array_key_exists('search', $values) ? $values['search'] : $node->search;
					$node->save();

					// autopublish
					$modelAutopublish = new AutopublishModel();
					$modelAutopublish->set($id, 1, $values["autopublish"]);
					$modelAutopublish->set($id, 0, $values["autounpublish"]);

					// alias
					$aliasModel = new AliasModel();
					$filter = new AliasFilter();
					$aliasModel->updateAlias($id, strlen($values['alias']) > 0
						? $values['alias']
						: $filter->filter($this->view->getAliasFormat('news', $node->toArray(), array('title' => $content->title))));

					$this->_receiveImageUpload($file, $node->toArray(), "node_news", "news");

					if ($this->_request->getParam('save', false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('Content updated.')));
						$this->redirect("/admin/news/index");
					} else {
						$this->flash->addMessage(array('success' => $this->view->translate('Content updated.')));
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/news/index");
			}
		} else {
			$node = $newsModel->getById($id);
			$node['created'] = $node['created']->format('Y-m-d H:i:s');
			$node['autopublish'] = $node["autopublish"] ? $node["autopublish"]->format('Y-m-d H:i:s') : "";
			$node['autounpublish'] = $node["autounpublish"] ? $node["autounpublish"]->format('Y-m-d H:i:s') : "";

			$form->populate($node);
		}

		$this->view->form = $form;
		$this->view->id = $id;
		$this->view->node = $newsModel->getById($id);;
	}

	/**
	 * Delete a news item
	 */
	public function deleteAction()
	{
		$id = (int)$this->_request->getParam('id', 0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/news/index");
		}

		$contentModel = new ContentModel();
		$contentModel->delete("id = {$id}");

		Zend_Registry::get('Cache')->remove('node_news');

		$this->flash->addMessage(array('success' => $this->view->translate('Content deleted.')));
		$this->redirect("/admin/news/index");
	}

	/**
	 * Toggle published/unpublished status of a news item
	 */
	public function publishAction()
	{
		$id = (int)$this->_request->getParam('id', 0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/news/index");
		}

		$model = new NewsModel();
		$model->getAdapter()->query("UPDATE node_news SET published = IF(published = '1', '0', '1') WHERE id = ?", array($id));

		Zend_Registry::get('Cache')->remove("node_news_{$id}");

		$this->redirect("/admin/news/index");
	}

	/**
	 * Assign taxonomy
	 */
	public function taxonomyAction()
	{
		$this->taxonomyActionTemplate(new NewsModel());
	}

	public function galleryAction()
	{
		$this->galleryActionTemplate(new NewsModel());
	}
}
