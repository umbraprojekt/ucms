<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Admin\Form\ConfigSeoForm;
use Mingos\uCMS\Admin\Form\ConfigSiteForm;
use Mingos\uCMS\Model\ConfigModel;

class Admin_IndexController extends AbstractAdminController
{
	/**
	 * Admin panel home page
	 */
	public function indexAction() {}

	/**
	 * Edit site title and SEO settings
	 */
	public function seoAction () {
		$form = new ConfigSeoForm();
		$modelConfig = new ConfigModel('seo');

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$modelConfig->setParams($form->getValues());
				Zend_Registry::get('Cache')->remove('config');

				$this->flash->addMessage(array('success' => $this->view->translate('SEO settings updated.')));
				$this->redirect($this->view->url());
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		} else {
			$form->populate($modelConfig->getParams());
		}

		$this->view->form = $form;
	}

	/**
	 * Edit site config
	 */
	public function configAction () {
		$form = new ConfigSiteForm();
		$modelConfig = new ConfigModel('site');

		if ($this->_request->isPost()) {
			if ($form->isValid($this->_request->getPost())) {
				$modelConfig->setParams($form->getValues());
				Zend_Registry::get('Cache')->remove('site');

				$this->flash->addMessage(array('success' => $this->view->translate('Content configuration updated.')));
				$this->redirect($this->view->url());
			} else {
				$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
			}
		} else {
			$form->populate($modelConfig->getParams());
		}

		$this->view->form = $form;
	}

	/**
	 * Clear all cached data
	 */
	public function clearCacheAction () {
		Zend_Registry::get('Cache')->clean(Zend_Cache::CLEANING_MODE_ALL);
		$this->flash->addMessage(array('info' => $this->view->translate('All caches have been cleared.')));

		$this->redirect($this->_helper->referer("/admin"));
	}
}
