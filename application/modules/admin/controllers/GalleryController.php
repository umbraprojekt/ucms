<?php
use Mingos\uCMS\Admin\Controller\AbstractAdminController;
use Mingos\uCMS\Validator\FileTypeValidator;
use Mingos\uCMS\Admin\Form\AddGalleryForm;
use Mingos\uCMS\Admin\Form\AddImageForm;
use Mingos\uCMS\Model\GalleryModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\MenuItemModel;
use Mingos\uCMS\Model\AliasModel;
use Mingos\uCMS\Model\ContentGalleryModel;
use Mingos\uCMS\Model\ContentModel;
use Mingos\uCMS\Model\FileModel;
use Mingos\uCMS\Model\ImageModel;
use Mingos\uCMS\Model\ContentTypeModel;

class Admin_GalleryController extends AbstractAdminController
{
	public function init()
	{
		parent::init();
		$this->nodeConfig = new ConfigModel('gallery');
	}

	/**
	 * Galleries list
	 */
	public function indexAction()
	{
		$column = $this->_request->getParam('column', 'title');
		$direction = $this->_request->getParam('direction', 'asc');

		$model = new GalleryModel();
		$select = $model->getPaginator(ContentTypeModel::TYPE_GALLERY, array('column' => $column, 'direction' => $direction));
		$paginator = $this->makePaginator($select);

		$nodes = array();
		foreach($paginator as $item) {
			$nodes[] = $model->getById($item['id'], true);
		}

		$this->view->paginator = $paginator;
		$this->view->column = $column;
		$this->view->direction = $direction;
		$this->view->limit = $paginator->getItemCountPerPage();
		$this->view->nodes = $nodes;
	}

	/**
	 * Returns a list of available galleries via AJAX (for CKEditor gallery macro plugin).
	 */
	public function listAjaxAction()
	{
		$model = new GalleryModel();
		$galleries = $model->getAll();

		$returnJson = array();
		foreach($galleries as $gallery) {
			array_push($returnJson, array($gallery['content_title'], $gallery['id']));
		}
		$this->_response->setHeader("Content-Type", "application/json");
		$this->_response->setBody(Zend_Json::encode(array('galleries' => $returnJson)));
		$this->_response->sendResponse();
		die;
	}

	/**
	 * Returns a list of available related galleries via AJAX (for CKEditor gallery macro plugin).
	 */
	public function listRelatedAjaxAction()
	{
		$id = $this->_request->getQuery("cid", 0);
		$returnJson = array();

		if ($id) {
			$model = new ContentGalleryModel();
			$galleries = $model->getRelatedGalleries($id);

			foreach ($galleries as $gallery) {
				array_push($returnJson, array($gallery['content_title'], $gallery['position']));
			}
		}
		$this->_response->setHeader("Content-Type", "application/json");
		$this->_response->setBody(Zend_Json::encode(array('galleries' => $returnJson)));
		$this->_response->sendResponse();
		die;
	}

	/**
	 * Returns a list of available galleries via AJAX.
	 */
	public function listAction()
	{
		$model = new GalleryModel();
		$galleries = $model->getAll();

		$this->_response->setHeader("Content-Type", "application/json");
		$this->_response->setBody(Zend_Json::encode(array('galleries' => $galleries)));
		$this->_response->sendResponse();
		die;
	}

	/**
	 * Relate gallery with content
	 */
	public function relateContentAction()
	{
		if (!$this->_request->isPost()) {
			throw new Zend_Http_Exception("Only POST is allowed.", 405);
		}

		$body = Zend_Json::decode($this->_request->getRawBody());
		if (!array_key_exists("ccid", $body) || !array_key_exists("gcid", $body)) {
			throw new Zend_Http_Exception("Bad request.", 400);
		}

		$contentGalleryModel = new ContentGalleryModel();
		$contentGalleryModel->relate($body["ccid"], $body["gcid"]);

		die;
	}

	/**
	 * Remove relation with content
	 */
	public function unrelateContentAction()
	{
		if (!$this->_request->isPost()) {
			throw new Zend_Http_Exception("Only POST is allowed.", 405);
		}

		$body = Zend_Json::decode($this->_request->getRawBody());
		if (!array_key_exists("ccid", $body) || !array_key_exists("gcid", $body)) {
			throw new Zend_Http_Exception("Bad request.", 400);
		}

		$contentGalleryModel = new ContentGalleryModel();
		$contentGalleryModel->unrelate($body["ccid"], $body["gcid"]);

		die;
	}

	/**
	 * Add a new gallery
	 */
	public function addAction()
	{
		$form = new AddGalleryForm();

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save',false) || $this->_request->getPost('continue',false)) {
				if ($form->isValid($this->_request->getPost())) {
					$gallery = $this->_helper->createGallery($form->getValues());

					$this->flash->addMessage(array('success' => $this->view->translate('New content was added: "%1$s".', $gallery['title'])));
					if ($this->_request->getPost('save',false)) {
						$this->redirect("/admin/gallery/detail/id/{$gallery["id"]}");
					} else {
						$this->redirect("/admin/gallery/edit/id/{$gallery["id"]}");
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/gallery");
			}
		}
		$this->view->form = $form;
	}

	/**
	 * Edit a gallery
	 */
	public function editAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/gallery");
		}
		$form = new AddGalleryForm();
		$contentModel = new ContentModel();
		$galleryModel = new GalleryModel();

		if($this->_request->isPost()) {
			if ($this->_request->getPost('save',false) || $this->_request->getPost('continue',false)) {
				if ($form->isValid($this->_request->getPost())) {
					$values = $form->getValues();

					$node = $galleryModel->fetchRow(array("id = ?" => $id));
					$contentModel->updateFromArray($id, $values);

					$node->path = !empty($values['path']) ? $values['path'] : $this->view->normaliseString($values['title']);
					$node->save();

					Zend_Registry::get('Cache')->clean(Zend_Cache::CLEANING_MODE_ALL);

					if ($this->_request->getPost('save',false)) {
						$this->flash->addMessage(array('success' => $this->view->translate('Content was updated: "%1$s".',$values['title'])));
						$this->redirect("/admin/gallery");
					} else {
						$this->view->messages[] = array('success' => $this->view->translate('Content was updated: "%1$s".',$values['title']));
					}
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect("/admin/gallery");
			}
		} else {
			$node = $galleryModel->getById($id);
			$node['created'] = $node['created']->format('Y-m-d H:i:s');

			$form->populate($node);
		}

		$this->view->form = $form;
	}

	/**
	 * Show the details about a gallery (info & images)
	 */
	public function detailAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/gallery/index");
		}

		$imageModel = new ImageModel();
		$fileModel = new FileModel();
		$model_node_gallery = new GalleryModel();

		$form = new AddImageForm(array(
			'path' => "gallery/{$id}",
			'table' => 'node_gallery',
			'id' => $id
		));

		$gallery = $model_node_gallery->getById($id);

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('upload',false)) {
				//prepare the files array
				$files = $this->_getFiles('image');

				$validator_type = new FileTypeValidator(array(FileTypeValidator::TYPE_IMAGE));

				foreach ($files as $file) {
					//validate the files
					if ($file['error'] != UPLOAD_ERR_OK) {
						$no_go = $this->view->translate('Could not upload "%1$s".',$file['name']);
						switch ($file['error']) {
							case UPLOAD_ERR_INI_SIZE:
								$this->flash->addMessage(array('error' => $no_go.' '.$this->view->translate('File size is expected to be under %1$s.',ini_get('upload_max_filesize'))));
								break;
							case UPLOAD_ERR_NO_FILE:
								$this->flash->addMessage(array('error' => $this->view->translate('Please choose the files for upload.')));
								break;
						}
					} else if (!$validator_type->isValid($file)) {
						$this->flash->addMessage(array('error' => $this->view->translate('Could not upload "%1$s".',$file['name']).' '.$this->view->translate('The file is expected to be an image.')));
					} else { // validation passed :)
						$filedata = $this->_helper->saveFile($file, DOC_ROOT."/upload/gallery/{$id}");
						$iid = $fileModel->insert(
							array(
								'created' => new Zend_Db_Expr('NOW()'),
								'uid' => $this->identity->id,
								'mime' => $filedata['mime'],
								'extension' => $filedata['extension'],
								'filename' => $filedata['filename'],
								'path' => "gallery/{$id}",
								'type' => 'image'
							)
						);
						$imageModel->insert(
							array(
								'iid' => $iid,
								'table' => 'node_gallery',
								'nid' => $id,
								'title' => $filedata['filename'],
								'alt' => $filedata['filename'],
								'path' => $gallery['path'],
								'filename' => $filedata['filename']
							)
						);

						$galleryModel = new GalleryModel();
						$galleryModel->updateImageCount($id);

						$this->flash->addMessage(array('success' => $this->view->translate('Image "%1$s" uploaded successfully.',$file['name'])));
					}
				}

				$this->redirect($this->view->url());
			}
		}

		$images = array();
		foreach ($imageModel->get(array('table' => 'node_gallery', 'nid' => $id)) as $item) {
			$images[] = $imageModel->getById($item['id']);
		}

		$this->view->images = $images;
		$this->view->gallery = $gallery;
		$this->view->form = $form;
	}

	/**
	 * Automatically order the images by filename
	 */
	public function autoorderAction()
	{
		$gid = (int)$this->_request->getParam("id",0);
		$modelGallery = new GalleryModel();
		$gallery = $modelGallery->getById($gid);
		if (!$gallery) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/gallery/index");
		}

		$imageModel = new ImageModel();
		$images = $imageModel->get(array("table" => "node_gallery", "nid" => $gid));

		usort($images,function($a,$b) {
			return strcmp($a["filename"],$b["filename"]);
		});

		foreach ($images as $weight => $image) {
			$imageModel->update(array(
				"weight" => $weight
			), $imageModel->getAdapter()->quoteInto("id = ?",$image["id"]));
		}

		$this->flash->addMessage(array('success' => $this->view->translate('Images reordered successfully.')));
		$this->redirect("/admin/gallery/detail/id/{$gid}");
	}

	/**
	 * Delete a gallery
	 */
	public function deleteAction()
	{
		$id = (int)$this->_request->getParam('id',0);
		if (!$id) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect("/admin/gallery/index");
		}

		$contentModel = new ContentModel();

		// remove images and the gallery directory
		$path = DOC_ROOT."/upload/gallery/{$id}";
		if (is_dir($path)) {
			$files = scandir($path);
			foreach ($files as $file) if (is_file("{$path}/{$file}")) unlink("{$path}/{$file}");
			rmdir($path);
		}
		$path = PUBLIC_PATH."/upload/gallery/{$id}";
		if (is_dir($path)) {
			$files = scandir($path);
			foreach ($files as $file) if (is_file("{$path}/{$file}")) unlink("{$path}/{$file}");
			rmdir($path);
		}

		$contentModel->getAdapter()->query("DELETE FROM image WHERE `table` = 'node_gallery' AND nid = ?", array($id));
		$contentModel->delete("id = {$id}");

		$menuItemModel = new MenuItemModel();
		$menuItemModel->delete("cid = {$id}");
		$menuItemModel->updateOrphans();

		Zend_Registry::get('Cache')->remove('menu');
		Zend_Registry::get('Cache')->remove("node_gallery");

		$this->flash->addMessage(array('success' => $this->view->translate('Content successfully deleted.')));
		$this->redirect("/admin/gallery/index");
	}
}
