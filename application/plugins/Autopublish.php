<?php
use Mingos\uCMS\Model\AutopublishModel;

class Plugin_Autopublish extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$modelAutopublish = new AutopublishModel();
		$modelAutopublish->execute();
	}
}
