<?php
/* uCMS
 * Copyright (c) 2011-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

class GdOverlayPlugin {
	/**
	 * Overlay an image over the working image
	 * @param string $filename file name (full path!) of the overlay image (required format: PNG)
	 * @return GdThumb
	 */
	public function overlay ($filename,&$that) {
		if (!file_exists($filename)) throw new Exception('Invalid overlay filename.',404);

		//background image
		$background = $that->getOldImage();
		imagealphablending($background, true);
		$dst_w = imagesx($background);
		$dst_h = imagesy($background);

		//overlay
		$overlay_big = imagecreatefrompng($filename);
		$src_w = imagesx($overlay_big);
		$src_h = imagesy($overlay_big);

		$overlay = imagecreatetruecolor($dst_w, $dst_h);
		imagealphablending($overlay, false);
		imagesavealpha($overlay,true);
		$transparent = imagecolorallocatealpha($overlay, 255, 255, 255, 127);
		imagefilledrectangle($overlay, 0, 0, $dst_w, $dst_h, $transparent);
		imagecopyresampled($overlay, $overlay_big, 0, 0, 0, 0, $dst_w, $dst_h, $src_w, $src_h);

		//overlay the images
		imagecopy($background,$overlay,0,0,0,0,$dst_w,$dst_h);

		//clean up
		imagedestroy($overlay);
		imagedestroy($overlay_big);

		return $that;
	}
}

PhpThumb::getInstance()->registerPlugin('GdOverlayPlugin', 'gd');
