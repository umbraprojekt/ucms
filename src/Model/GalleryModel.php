<?php
namespace Mingos\uCMS\Model;

class GalleryModel extends AbstractNodeModel
{
	protected $_name = "node_gallery";

	const TYPE = ContentTypeModel::TYPE_GALLERY;

	/**
	 * Fetch a single node by its ID (cached)
	 *
	 * @param  int $id
	 * @param  bool $noCache Override any caching
	 * @return array
	 */
	public function getById ($id, $noCache = false) {
		if ($noCache || !$node = \Zend_Registry::get('Cache')->load("{$this->_name}_{$id}")) {
			// node info from the database
			$node = $this->_db->fetchRow(
				$this
					->select()
					->setIntegrityCheck(false)
					->from(array('n' => $this->_name))
					->join(array('c' => 'content'), 'n.id = c.id', array("id", "content_title", "title", "created", "updated"))
					->join(array('u' => 'user'), 'u.id = c.uid', array('author' => 'u.login'))
					->joinLeft(array('u2' => 'user'), 'u2.id = c.updated_uid',array('updated_author' => 'u2.login'))
					->where('n.id = ?', $id)
			);
			$node['created'] = new \DateTime($node['created']);
			$node['updated'] = !empty($node['updated']) ? new \DateTime($node['updated']) : null;

			$node["image"] = $this->getNodeImage($id);

			if (!$noCache) \Zend_Registry::get('Cache')->save($node,"{$this->_name}_{$id}");
		}

		return $node;
	}

	/**
	 * Update the count of images in a gallery
	 * @param int $id
	 */
	public function updateImageCount ($id) {
		$this->update(array(
			'images' => new \Zend_Db_Expr("( SELECT COUNT(1) FROM image WHERE `table` = '{$this->_name}' AND nid = {$id} )")
		), "id = {$id}");

		$contentModel = new ContentModel();
		$contentModel->update(array(
			"updated" => new \Zend_Db_Expr("NOW()")
		), $this->_db->quoteInto("id = ?", $id));
	}
}
