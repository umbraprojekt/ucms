<?php
namespace Mingos\uCMS\Model;

class TaxonomyDictionaryModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "taxonomy_dictionary";

	/**
	 * Get a list of dictionaries
	 *
	 * @return array
	 */
	public function getAll()
	{
		$dictionaries = $this->fetchAll(
			$this->select()
				->order("weight ASC")
		);

		return $dictionaries;
	}
}
