<?php
namespace Mingos\uCMS\Model;

use Mingos\uCMS\Filter\AliasFilter;
use Mingos\uCMS\Service\Cache;

class AliasModel extends \Zend_Db_Table_Abstract
{
	/**
	 * @var Cache
	 */
	protected $cache;
	protected $_name = "alias";

	public function __construct()
	{
		$this->cache = new Cache();
		parent::__construct();
	}

	/**
	 * Check if a given alias already exists
	 * @param  string                            $alias    The path to be checked
	 * @param  \Zend_Controller_Request_Abstract $request  The request object, from which it can be deduced which node is being edited
	 * @return bool
	 */
	public function exists($alias, $request = null)
	{
		if (!$alias) return false;
		$sql = $this
			->select()
			->from(array('a' => 'alias'))
			->where('a.alias = ?',$alias)
		;

		if ($request && $request->id) {
			$table = '';
			switch ($request->controller) {
				case 'page': $table = 'node_page'; break;
				case 'product': $table = 'node_product'; break;
				case 'news': $table = 'node_news'; break;
				case 'gallery': $table = 'node_gallery'; break;
				case 'view': $table = 'node_view'; break;
				default: break;
			}
			$sql
				->join(array('n' => $table),'n.id = a.id',array())
				->where('n.id <> ?',$request->id)
			;
		}

		$result = $this->fetchRow($sql);

		if ($result === null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Fetch alias data based on a provided alias
	 *
	 * @param  string $alias
	 * @return array
	 */
	public function getByAlias($alias)
	{
		return $this->_db->fetchRow($this->select()->where("alias = ?", $alias));
	}

	public function updateAlias($id, $alias) {
		$row = $this->fetchRow(array("id = ?" => $id));
		$row->alias = $alias;
		return $row->save();
	}

	/**
	 * Inserts a new row.
	 *
	 * @param  array  $data  Column-value pairs.
	 * @return mixed         The primary key of the row inserted.
	 */
	public function insert(array $data)
	{
		$this->cache->clear();
		return parent::insert($data);
	}

	/**
	 * Updates existing rows.
	 *
	 * @param  array        $data  Column-value pairs.
	 * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
	 * @return int          The number of rows updated.
	 */
	public function update(array $data, $where)
	{
		$this->cache->clear();
		return parent::update($data, $where);
	}

	/**
	 * Deletes existing rows.
	 *
	 * @param  array|string $where SQL WHERE clause(s).
	 * @return int          The number of rows deleted.
	 */
	public function delete($where)
	{
		$this->cache->clear();
		return parent::delete($where);
	}
}
