<?php
namespace Mingos\uCMS\Model;

use Mingos\uCMS\Service\Cache;

abstract class AbstractNodeModel extends \Zend_Db_Table_Abstract
{
	/**
	 * @var Cache
	 */
	protected $cache;

	public function __construct()
	{
		$this->cache = new Cache();
		parent::__construct();
	}

	/**
	 * Fetch a single node by its ID (cached)
	 *
	 * @param  int $id
	 * @param  bool $noCache Override any caching
	 * @return array
	 */
	abstract public function getById($id, $noCache = false);

	/**
	 * Get the node's main image
	 *
	 * @param  integer $id
	 * @return array
	 */
	public function getNodeImage($id)
	{
		$imageModel = new ImageModel();
		$image = $imageModel->get(array("nid" => $id, "table" => $this->_name));
		return empty($image) ? array() : array_shift($image);
	}

	/**
	 * Ftetch all taxonomy bound to a given node
	 *
	 * @param  integer $cid
	 * @return array
	 */
	public function getNodeTaxonomy($cid)
	{
		// fetch all taxonomy term IDs bound to the content
		$taxonomy = $this->_db->fetchAll("SELECT tid FROM content_taxonomy WHERE cid = :cid", array("cid" => $cid));

		$return = array();
		foreach ($taxonomy as $tax) {
			// fetch the taxonomy term and dictionary data
			$taxInfo = $this->_db->fetchRow(
				$this->select()
					->setIntegrityCheck(false)
					->join(array("t" => "taxonomy"), "tl.tid = t.id", array("tax_id" => "id", "tax_title" => "title", "tax_slug" => "slug"))
					->join(array("d" => "taxonomy_dictionary"), "t.did = d.id", array("dict_id" => "id", "dict_title" => "title", "dict_slug" => "slug"))
					->where("t.id = ?", $tax["tid"])
			);

			// and save the data
			if (!array_key_exists($taxInfo["dict_id"], $return)) {
				$return[$taxInfo["dict_id"]] = array(
					"id" => $taxInfo["dict_id"],
					"slug" => $taxInfo["dict_slug"],
					"title" => $taxInfo["dict_title"],
					"terms" => array()
				);
			}
			$return[$taxInfo["dict_id"]]["terms"][$taxInfo["tax_id"]] = array(
				"id" => $taxInfo["tax_id"],
				"slug" => $taxInfo["tax_slug"],
				"title" => $taxInfo["tax_title"]
			);
		}

		return $return;
	}

	/**
	 * Fetch nodes with given taxonomy terms
	 * @param  string $type "AND" or "OR"
	 * @param  array $taxonomy Array of taxonomy term IDs
	 * @param  bool $published
	 * @return \Zend_Db_Select
	 */
	protected function getByTaxonomySql($type, $taxonomy, $published)
	{
		$sql = $this
			->select()
			->setIntegrityCheck(false)
			->from(array("n" => $this->_name), array("id"))
			->join(array("ct" => "content_taxonomy"), "n.id = ct.cid", array());
		if (null !== $published) {
			$sql->where("n.published = '?'", intval($published));
		}

		$where = array();
		foreach ($taxonomy as $termId) {
			$where[] = $this->_db->quoteInto("ct.tid = ?", $termId);
		}
		$where = implode(" {$type} ", $where);
		$sql->where($where);

		return $sql;
	}

	/**
	 * Get content nodes
	 * @param  \Zend_Db_Select $sql
	 * @return array
	 */
	protected function getByTaxonomy($sql)
	{
		$ids = $this->_db->fetchCol($sql);

		$result = array();
		foreach ($ids as $id) {
			$result[intval($id)] = $this->getById($id);
		}

		return $result;
	}

	/**
	 * Fetch nodes that have all of the provided taxonomy terms
	 * @param  array $taxonomy Array of taxonomy term IDs
	 * @param  bool $published
	 * @return array
	 */
	public function getByTaxonomyAnd($taxonomy, $published = null)
	{
		return $this->getByTaxonomy($this->getByTaxonomySql("AND", $taxonomy, $published));
	}

	/**
	 * Fetch nodes that have at least one of the provided taxonomy terms
	 * @param  array $taxonomy Array of taxonomy term IDs
	 * @param  bool $published
	 * @return array
	 */
	public function getByTaxonomyOr($taxonomy, $published = null)
	{
		return $this->getByTaxonomy($this->getByTaxonomySql("OR", $taxonomy, $published));
	}

	/**
	 * Fetch all nodes
	 *
	 * @param  int $published published status
	 * @return array
	 */
	public function getAll($published = null)
	{
		$sql = $this->select();

		if ($published === null) $result = $this->_db->fetchAll($sql);
		else $result = $this->_db->fetchAll($sql->where("published = ?", strval($published)));

		$imageModel = new ImageModel();

		$return = array();
		foreach ($result as $item) {
			$node = $this->getById($item['id']);
			$image = $imageModel->get(array("nid" => $item["id"], "table" => $this->_name));
			$node["image"] = !empty($image) ? array_shift($image) : array();
			$return[$node['id']] = $node;
		}

		return $return;
	}

	/**
	 * Fetch all nodes for the paginator display
	 *
	 * @param  integer $type   Content type
	 * @param  array   $params Additional parametres
	 * @return \Zend_Db_Table_Select
	 */
	public function getPaginator($type, array $params = array())
	{
		$sql = $this
			->select()
			->setIntegrityCheck(false)
			->from(array('c' => "content"), array('id'))
			->where("type = ?", $type);
		if (array_key_exists('column', $params)) {
			switch ($params['column']) {
				case 'title':
					$params['column'] = 'title';
					break;
				case 'content_title':
					$params['column'] = 'content_title';
					break;
				case 'created':
					$params['column'] = 'created';
					break;
			}
			if (array_key_exists('direction', $params)) $sql->order("{$params['column']} {$params['direction']}");
			else $sql->order("{$params['column']} DESC");
		} else {
			$sql->order("created DESC");
		}
		return $sql;
	}

	/**
	 * Fetch the content ID of a given item
	 *
	 * @param  int $id node ID
	 * @return int
	 */
	public function getCid($id)
	{
		$node = $this->getById($id);
		return array_key_exists('cid', $node) ? $node['cid'] : null;
	}

	/**
	 * Inserts a new row.
	 *
	 * @param  array $data Column-value pairs.
	 * @return mixed         The primary key of the row inserted.
	 */
	public function insert(array $data)
	{
		$this->cache->clear();
		return parent::insert($data);
	}

	/**
	 * Updates existing rows.
	 *
	 * @param  array $data Column-value pairs.
	 * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
	 * @return int          The number of rows updated.
	 */
	public function update(array $data, $where)
	{
		$this->cache->clear();
		return parent::update($data, $where);
	}

	/**
	 * @throws \Zend_Exception
	 */
	public function delete($where)
	{
		throw new \Zend_Exception("Removing nodes is allowed only through the removal of a content item.");
	}
}
