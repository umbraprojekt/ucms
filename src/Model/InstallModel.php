<?php
namespace Mingos\uCMS\Model;

class InstallModel extends \Zend_Db_Table_Abstract
{
	/**
	 * Database not found.
	 * @var int
	 */
	const NO_DATABASE = 0;
	/**
	 * Database found, but at least one table is missing.
	 * @var int
	 */
	const NO_TABLES = 1;

	/**
	 * Database found and confirmed to contain the required tables, but the "identity" table is empty.
	 * @var int
	 */
	const USER_EMPTY = 2;
	/**
	 * No errors.
	 * @var int
	 */
	const OK = 3;

	/**
	 * Drop table queries
	 * @var array
	 */
	private $tableQueries = array(
		"DROP TABLE IF EXISTS content_gallery",
		"DROP TABLE IF EXISTS autopublish",
		"DROP TABLE IF EXISTS content_taxonomy",
		"DROP TABLE IF EXISTS config",
		"DROP TABLE IF EXISTS `comment`",
		"DROP TABLE IF EXISTS alias",
		"DROP TABLE IF EXISTS menu_item",
		"DROP TABLE IF EXISTS menu",
		"DROP TABLE IF EXISTS image",
		"DROP TABLE IF EXISTS node_gallery",
		"DROP TABLE IF EXISTS node_news",
		"DROP TABLE IF EXISTS node_page",
		"DROP TABLE IF EXISTS node_product",
		"DROP TABLE IF EXISTS node_view",
		"DROP TABLE IF EXISTS node_block",
		"DROP TABLE IF EXISTS `user`",
		"DROP TABLE IF EXISTS content_relation",
		"DROP TABLE IF EXISTS content",
		"DROP TABLE IF EXISTS content_type",
		"DROP TABLE IF EXISTS taxonomy",
		"DROP TABLE IF EXISTS taxonomy_dictionary",
		"DROP TABLE IF EXISTS `file`",
	);

	/**
	 * Check the installation stage
	 *
	 * @return int
	 */
	public function verify () {
		if ($this->_db == null) return self::NO_DATABASE;

		try {
			$tables = $this->_db->fetchAll('SHOW TABLES');
			if (count($tables) < count($this->tableQueries)) return self::NO_TABLES;
		} catch(\Exception $e) {
			return self::NO_DATABASE;
		}

		$result = $this->_db->fetchOne("SELECT COUNT(*) FROM user");
		if ($result == '0') return self::USER_EMPTY;

		return self::OK;
	}

	public function createTables () {
		// remove tables
		foreach ($this->tableQueries as $query) {
			$this->_db->query($query);
		}

		// create tables

		// file
		$this->_db->query("
			CREATE TABLE file (
				id        INT(11)      NOT NULL  AUTO_INCREMENT,
				created   DATETIME     NOT NULL,
				uid       INT(11)      NOT NULL,
				path      VARCHAR(128) NOT NULL,
				filename  VARCHAR(128) NOT NULL,
				extension VARCHAR(4)   NOT NULL  DEFAULT 'jpg',
				mime      VARCHAR(128) NOT NULL  DEFAULT 'image/jpeg',
				type      VARCHAR(16)  NOT NULL  DEFAULT 'image',
				PRIMARY KEY (id)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// taxonomy
		$this->_db->query("
			CREATE TABLE taxonomy_dictionary (
				id          INT(11)       NOT NULL AUTO_INCREMENT,
				title       VARCHAR(64)   NOT NULL,
				slug        VARCHAR(64)   NOT NULL,
				weight      INT(11)       NOT NULL DEFAULT 0,
				UNIQUE KEY slug (slug),
				PRIMARY KEY (id)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			CREATE TABLE taxonomy (
				id          INT(11)       NOT NULL AUTO_INCREMENT,
				did         INT(11)       NOT NULL,
				title       VARCHAR(64)   NOT NULL,
				slug        VARCHAR(64)   NOT NULL,
				`left`      INT(11)       NOT NULL DEFAULT '0',
				`right`     INT(11)       NOT NULL DEFAULT '0',
				parent      INT(11)       NOT NULL DEFAULT '0',
				depth       INT(11)       NOT NULL DEFAULT '0',
				UNIQUE KEY slug (slug),
				PRIMARY KEY (id),
				INDEX did (did ASC)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			ALTER TABLE `taxonomy`
                ADD CONSTRAINT `taxonomy_ibfk_1`
                FOREIGN KEY (`did`) REFERENCES `taxonomy_dictionary` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
		");

		// content_type
		$this->_db->query("
			CREATE TABLE content_type (
				id      INT(11)     NOT NULL AUTO_INCREMENT,
				name    VARCHAR(32) NOT NULL,
				`table` VARCHAR(32) NOT NULL,
				PRIMARY KEY (id),
				UNIQUE KEY uq_name (name),
				UNIQUE KEY uq_table (`table`)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			INSERT INTO content_type (name, `table`) VALUES
			('content_type_page','node_page'),
			('content_type_news','node_news'),
			('content_type_gallery','node_gallery'),
			('content_type_view','node_view'),
			('content_type_block','node_block'),
			('content_type_product','node_product')
		");

		// content
		$this->_db->query("
			CREATE TABLE content (
				id            INT(11)       NOT NULL AUTO_INCREMENT,
				type          INT(11)       NOT NULL,
				content_title VARCHAR(128)  NOT NULL,
				title         VARCHAR(128)  NOT NULL,
				uid           INT(11)       NOT NULL,
				created       TIMESTAMP     NOT NULL  DEFAULT CURRENT_TIMESTAMP,
				updated       TIMESTAMP     NULL      DEFAULT NULL,
				updated_uid   INT(11)       NULL      DEFAULT NULL,
				PRIMARY KEY (id),
				KEY `type` (`type`)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			ALTER TABLE `content`
                ADD CONSTRAINT `content_ibfk_1`
                FOREIGN KEY (`type`) REFERENCES `content_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
		");

		// content relation
		$this->_db->query("
			CREATE TABLE IF NOT EXISTS `content_relation` (
				`cid` int(11) NOT NULL,
				`relation_cid` int(11) NOT NULL,
				`sticky` tinyint(1) NOT NULL DEFAULT '0',
				UNIQUE KEY `cid_2` (`cid`,`relation_cid`)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			ALTER TABLE `content_relation`
				ADD CONSTRAINT `content_relation_ibfk_2`
					FOREIGN KEY (`relation_cid`)
					REFERENCES `content` (`id`)
					ON DELETE CASCADE,
				ADD CONSTRAINT `content_relation_ibfk_1`
					FOREIGN KEY (`cid`)
					REFERENCES `content` (`id`)
					ON DELETE CASCADE
		");

		// alias
		$this->_db->query("
			CREATE TABLE alias (
				id         INT(11)      NOT NULL,
				alias      VARCHAR(128) NOT NULL,
				module     VARCHAR(32)  NOT NULL,
				controller VARCHAR(32)  NOT NULL,
				action     VARCHAR(32)  NOT NULL,
				PRIMARY KEY (id),
				UNIQUE KEY uq_route (alias),
				INDEX fk_alias_id (id ASC),
				CONSTRAINT fk_alias_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// menu
		$this->_db->query("
			CREATE TABLE menu (
				id     INT(11)     NOT NULL AUTO_INCREMENT,
				`name` VARCHAR(64) NOT NULL,
			 	PRIMARY KEY (id)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// menu_item
		$this->_db->query("
			CREATE TABLE menu_item (
				id       INT(11)      NOT NULL  AUTO_INCREMENT,
				mid      INT(11)      NOT NULL,
				file_id  INT(11)          NULL  DEFAULT NULL,
				parent   INT(11)      NOT NULL  DEFAULT '0',
				depth    INT(11)      NOT NULL  DEFAULT '0',
				`left`   INT(11)      NOT NULL  DEFAULT '0',
				`right`  INT(11)      NOT NULL  DEFAULT '0',
				cid      INT(11)          NULL  DEFAULT NULL,
				label    VARCHAR(64)  NOT NULL,
				href     VARCHAR(128) NOT NULL,
				class    VARCHAR(32)  NOT NULL  DEFAULT '',
				title    VARCHAR(64)  NOT NULL  DEFAULT '',
				PRIMARY KEY (id),
				INDEX idx_mid (mid ASC),
				KEY fk_menu_item_mid (mid),
				KEY fk_menu_item_file (file_id),
				KEY fk_menu_item_cid (cid),
				CONSTRAINT fk_menu_item_mid FOREIGN KEY (mid) REFERENCES menu (id) ON DELETE CASCADE ON UPDATE CASCADE,
				CONSTRAINT fk_menu_item_file FOREIGN KEY (file_id) REFERENCES `file` (id) ON DELETE SET NULL ON UPDATE CASCADE,
				CONSTRAINT fk_menu_item_cid FOREIGN KEY (cid) REFERENCES `content` (id) ON DELETE SET NULL ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// node_gallery
		$this->_db->query("
			CREATE TABLE node_gallery (
				id          INT(11)       NOT NULL,
				path        VARCHAR(128)  NOT NULL,
				images      INT(11)       NOT NULL  DEFAULT '0',
				PRIMARY KEY (id),
				INDEX fk_node_gallery_id (id ASC),
				CONSTRAINT fk_node_gallery_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// image
		$this->_db->query("
			CREATE TABLE image (
				id       INT(11)      NOT NULL  AUTO_INCREMENT,
				iid      INT(11)      NOT NULL,
				`table`  VARCHAR(32)  NOT NULL,
				nid      INT(11)      NOT NULL,
				weight   INT(11)      NOT NULL  DEFAULT '0',
				crop     VARCHAR(128) NOT NULL  DEFAULT '',
				cut      ENUM('S','E','N','W','0') NOT NULL  DEFAULT '0',
				title    VARCHAR(128) NOT NULL,
				alt      VARCHAR(128) NOT NULL,
				path     VARCHAR(128) NOT NULL,
				filename VARCHAR(128) NOT NULL,
				url      VARCHAR(128) NOT NULL  DEFAULT '',
				`update`   TIMESTAMP    NOT NULL  DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				PRIMARY KEY (id),
				INDEX idx_iid (iid ASC),
				INDEX idx_table_nid (`table` ASC, nid ASC)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// node_news
		$this->_db->query("
			CREATE TABLE node_news (
				id                INT(11)       NOT NULL,
				input_format      VARCHAR(8)    NOT NULL  DEFAULT 'html',
				teaser            TEXT          NOT NULL,
				body              MEDIUMTEXT    NOT NULL,
				published         ENUM('0','1') NOT NULL  DEFAULT '0',
				comments_active   INT(11)       NOT NULL  DEFAULT '0',
				comments_inactive INT(11)       NOT NULL  DEFAULT '0',
				seo_description   VARCHAR(155)  NOT NULL,
				seo_keywords      VARCHAR(256)  NOT NULL,
				seo_priority      DECIMAL(2,1)  NOT NULL  DEFAULT '0.1',
				seo_changefreq    ENUM('always','hourly','daily','weekly','monthly','yearly','never') NOT NULL DEFAULT 'never',
				seo_title         VARCHAR(64)   NOT NULL  DEFAULT '',
				seo_robots        VARCHAR(32)   NOT NULL  DEFAULT '',
				view_script       VARCHAR(32)   NOT NULL  DEFAULT 'news',
				sitemap           ENUM('0','1') NOT NULL  DEFAULT '1',
				search            ENUM('0','1') NOT NULL  DEFAULT '1',
				PRIMARY KEY (id),
				INDEX fk_node_news_id (id ASC),
				CONSTRAINT fk_node_news_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// comment
		$this->_db->query("
			CREATE TABLE comment (
				id              INT(11)       NOT NULL AUTO_INCREMENT,
				cid             INT(11)       NOT NULL,
				created         TIMESTAMP     NOT NULL  DEFAULT CURRENT_TIMESTAMP,
				uid             INT(11)       NOT NULL,
				name            VARCHAR(32)   NOT NULL,
				email           VARCHAR(60)   NOT NULL,
				ip              VARCHAR(16)   NOT NULL,
				active          ENUM('0','1') NOT NULL  DEFAULT '0',
				body            TEXT          NOT NULL,
				PRIMARY KEY (id),
				KEY fk_comment_cid (cid),
				CONSTRAINT fk_comment_cid FOREIGN KEY (cid) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// node_product
		$this->_db->query("
			CREATE TABLE node_product (
				id                INT(11)       NOT NULL,
				`order`           int(11)       NOT NULL  DEFAULT '0',
				input_format      VARCHAR(8)    NOT NULL  DEFAULT 'html',
				teaser            TEXT          NOT NULL,
				body              MEDIUMTEXT    NOT NULL,
				published         ENUM('0','1') NOT NULL  DEFAULT '1',
				comments_active   INT(11)       NOT NULL  DEFAULT '0',
				comments_inactive INT(11)       NOT NULL  DEFAULT '0',
				seo_description   VARCHAR(155)  NOT NULL,
				seo_keywords      VARCHAR(256)  NOT NULL,
				seo_priority      DECIMAL(2,1)  NOT NULL  DEFAULT '0.7',
				seo_changefreq    ENUM('always','hourly','daily','weekly','monthly','yearly','never') NOT NULL DEFAULT 'yearly',
				seo_title         VARCHAR(64)   NOT NULL  DEFAULT '',
				seo_robots        VARCHAR(32)   NOT NULL  DEFAULT '',
				view_script       VARCHAR(32)   NOT NULL  DEFAULT 'page',
				sitemap           ENUM('0','1') NOT NULL  DEFAULT '1',
				search            ENUM('0','1') NOT NULL  DEFAULT '1',
				PRIMARY KEY (id),
				INDEX fk_node_product_id (id ASC),
				CONSTRAINT fk_node_product_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// node_page
		$this->_db->query("
			CREATE TABLE node_page (
				id                INT(11)       NOT NULL,
				input_format      VARCHAR(8)    NOT NULL  DEFAULT 'html',
				teaser            TEXT          NOT NULL,
				body              MEDIUMTEXT    NOT NULL,
				published         ENUM('0','1') NOT NULL  DEFAULT '1',
				comments_active   INT(11)       NOT NULL  DEFAULT '0',
				comments_inactive INT(11)       NOT NULL  DEFAULT '0',
				seo_description   VARCHAR(155)  NOT NULL,
				seo_keywords      VARCHAR(256)  NOT NULL,
				seo_priority      DECIMAL(2,1)  NOT NULL  DEFAULT '0.5',
				seo_changefreq    ENUM('always','hourly','daily','weekly','monthly','yearly','never') NOT NULL DEFAULT 'yearly',
				seo_title         VARCHAR(64)   NOT NULL  DEFAULT '',
				seo_robots        VARCHAR(32)   NOT NULL  DEFAULT '',
				view_script       VARCHAR(32)   NOT NULL  DEFAULT 'page',
				sitemap           ENUM('0','1') NOT NULL  DEFAULT '1',
				search            ENUM('0','1') NOT NULL  DEFAULT '1',
				PRIMARY KEY (id),
				INDEX fk_node_page_id (id ASC),
				CONSTRAINT fk_node_page_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// node_view
		$this->_db->query("
			CREATE TABLE node_view (
				id                INT(11)       NOT NULL,
				input_format      VARCHAR(8)    NOT NULL  DEFAULT 'html',
				teaser            TEXT          NOT NULL,
				body              MEDIUMTEXT    NOT NULL,
				published         ENUM('0','1') NOT NULL  DEFAULT '1',
				seo_description   VARCHAR(155)  NOT NULL,
				seo_keywords      VARCHAR(256)  NOT NULL,
				seo_priority      DECIMAL(2,1)  NOT NULL  DEFAULT '0.8',
				seo_changefreq    ENUM('always','hourly','daily','weekly','monthly','yearly','never') NOT NULL DEFAULT 'monthly',
				seo_title         VARCHAR(64)   NOT NULL  DEFAULT '',
				seo_robots        VARCHAR(32)   NOT NULL  DEFAULT '',
				view_script       VARCHAR(32)   NOT NULL  DEFAULT 'view',
				sitemap           ENUM('0','1') NOT NULL  DEFAULT '1',
				search            ENUM('0','1') NOT NULL  DEFAULT '1',
				PRIMARY KEY (id),
				INDEX fk_node_view_id (id ASC),
				CONSTRAINT fk_node_view_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// node_block
		$this->_db->query("
			CREATE TABLE node_block (
				id                INT(11)       NOT NULL,
				input_format      VARCHAR(8)    NOT NULL  DEFAULT 'html',
				body              MEDIUMTEXT    NOT NULL,
				view_script       VARCHAR(32)   NOT NULL  DEFAULT 'block',
				PRIMARY KEY (id),
				INDEX fk_node_block_id (id ASC),
				CONSTRAINT fk_node_block_id FOREIGN KEY (id) REFERENCES content (id) ON DELETE CASCADE ON UPDATE CASCADE
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// user
		$this->_db->query("
			CREATE TABLE user (
				id INT(11)            NOT NULL AUTO_INCREMENT,
				created TIMESTAMP     NOT NULL DEFAULT CURRENT_TIMESTAMP,
				last_access TIMESTAMP NULL     DEFAULT NULL,
				login VARCHAR(32)     NOT NULL,
				email VARCHAR(64)     NOT NULL,
				password VARCHAR(32)  NOT NULL,
				salt VARCHAR(32)      NOT NULL,
				role VARCHAR(32)      NOT NULL,
				active ENUM('0','1')  NOT NULL DEFAULT '1',
				PRIMARY KEY (id),
				UNIQUE KEY uq_login (login),
				UNIQUE KEY uq_email (email)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");

		// config
		$this->_db->query("
			CREATE TABLE config (
				id       INT(11)      NOT NULL  AUTO_INCREMENT,
				type     VARCHAR(16)  NOT NULL,
				param    VARCHAR(32)  NOT NULL,
				value    VARCHAR(255) NOT NULL,
				PRIMARY KEY (id),
				UNIQUE KEY uq_type_param (type, param)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		// set config defaults
		$this->_db->query("
			INSERT INTO config (type, param, value) VALUES
			('site','created','0'),
			('site','seo_title','0'),
			('site','seo_priority','0'),
			('site','seo_changefreq','0'),
			('site','view_script','0'),
			('site','offline','0'),
			('site','sitemap','0'),
			('site','search','0'),
			('gallery','created','0'),
			('news','created','0'),
			('news','seo_title','0'),
			('news','seo_changefreq','never'),
			('news','seo_priority','0.5'),
			('news','view_script','0'),
			('news','sitemap','0'),
			('news','robots','0'),
			('news','search','0'),
			('page','created','0'),
			('page','seo_title','0'),
			('page','seo_changefreq','yearly'),
			('page','seo_priority','0.5'),
			('page','view_script','0'),
			('page','sitemap','0'),
			('page','robots','0'),
			('page','search','0'),
			('product','created','0'),
			('product','seo_title','0'),
			('product','seo_changefreq','monthly'),
			('product','seo_priority','0.7'),
			('product','view_script','0'),
			('product','sitemap','0'),
			('product','robots','0'),
			('product','search','0'),
			('view','created','0'),
			('view','seo_title','0'),
			('view','seo_changefreq','monthly'),
			('view','seo_priority','0.8'),
			('view','view_script','0'),
			('view','sitemap','0'),
			('view','robots','0'),
			('view','search','0'),
			('block','created','0'),
			('image','quality','75'),
			('image','unsafe_uploads','0'),
			('seo','keywords',''),
			('seo','description',''),
			('seo','analytics',''),
			('seo','robots','index, follow'),
			('seo','title','uCMS'),
			('seo','verification',''),
			('seo','canonical','')
		");

		//taxonomy_content
		$this->_db->query("
			CREATE TABLE IF NOT EXISTS `content_taxonomy` (
				`cid` int(11) NOT NULL,
				`tid` int(11) NOT NULL,
				PRIMARY KEY (`cid`,`tid`),
				KEY `cid` (`cid`),
				KEY `tid` (`tid`)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			ALTER TABLE `content_taxonomy`
			ADD CONSTRAINT `content_taxonomy_ibfk_2`
				FOREIGN KEY (`tid`)
				REFERENCES `taxonomy` (`id`)
				ON DELETE CASCADE,
			ADD CONSTRAINT `content_taxonomy_ibfk_1`
				FOREIGN KEY (`cid`)
				REFERENCES `content` (`id`)
				ON DELETE CASCADE
		");

		// autopublish
		$this->_db->query("
			CREATE TABLE IF NOT EXISTS `autopublish` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`cid` int(11) NOT NULL,
				`publish` tinyint(1) NOT NULL,
				`date` timestamp NOT NULL,
				PRIMARY KEY (`id`),
				UNIQUE KEY `cid_2` (`cid`,`publish`),
				KEY `cid` (`cid`)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci
		");
		$this->_db->query("
			ALTER TABLE `autopublish`
			ADD FOREIGN KEY (`cid`)
				REFERENCES `content` (`id`)
				ON DELETE CASCADE
				ON UPDATE RESTRICT
		");

		// content_gallery
		$this->_db->query("
			create table `content_gallery` (
				ccid INT(11) NOT NULL,
				gcid INT(11) NOT NULL,
				PRIMARY KEY (ccid, gcid)
			) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci");
		$this->_db->query("
			ALTER TABLE `content_gallery`
			ADD CONSTRAINT `fk_content_gallery_ccid`
			FOREIGN KEY (`ccid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");
		$this->_db->query("
			ALTER TABLE `content_gallery`
			ADD CONSTRAINT `fk_content_gallery_gcid`
			FOREIGN KEY (`gcid`) REFERENCES `content` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
		");
	}
}
