<?php
namespace Mingos\uCMS\Model;

class BlockModel extends AbstractNodeModel
{
	protected $_name = "node_block";

	const TYPE = ContentTypeModel::TYPE_BLOCK;

	/**
	 * Fetch a single node by its ID (cached)
	 *
	 * @param  int $id
	 * @param  bool $noCache Override any caching
	 * @return array
	 */
	public function getById ($id, $noCache = false) {
		if ($noCache || !$node = \Zend_Registry::get('Cache')->load("{$this->_name}_{$id}")) {
			// node info from the database
			$node = $this->_db->fetchRow(
				$this
					->select()
					->setIntegrityCheck(false)
					->from(array('n' => $this->_name))
					->join(array('c' => 'content'), 'n.id = c.id', array("id", "content_title", "title", "created", "updated"))
					->join(array('u' => 'user'), 'u.id = c.uid', array('author' => 'u.login'))
					->joinLeft(array('u2' => 'user'), 'u2.id = c.updated_uid', array('updated_author' => 'u2.login'))
					->where('n.id = ?', $id)
			);
			$node['created'] = new \DateTime($node['created']);
			$node['updated'] = !empty($node['updated']) ? new \DateTime($node['updated']) : null;

			$node["image"] = $this->getNodeImage($id);
			$node["taxonomy"] = $this->getNodeTaxonomy($id);

			if (!$noCache) \Zend_Registry::get('Cache')->save($node,"{$this->_name}_{$id}");
		}

		return $node;
	}
}
