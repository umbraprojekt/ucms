<?php
namespace Mingos\uCMS\Model;

class AclModel extends \Zend_Acl {
	/**
	 * Singletong instance of the ACL model
	 * @var AclModel
	 */
	protected static $_instance = null;

	/**
	 * Current identity's role
	 * @var string
	 */
	private $role = null;

	/**
	 * Get a singleton instance of the ACL
	 *
	 * @return AclModel
	 */
	public static function getInstance () {
		if (self::$_instance === null) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	public function __construct() {
		$ini = parse_ini_file(APPLICATION_PATH."/configs/acl.ini",true);

		// Roles
		foreach($ini['role'] as $role => $parents) {
			if (!empty($parents)) {
				$this->addRole(new \Zend_Acl_Role($role), explode(',', $parents));
			}
			else {
				$this->addRole(new \Zend_Acl_Role($role));
			}
			$this->deny($role);
		}
		$this->allow($role);

		// Resources
		foreach($ini['resource'] as $resource => $roles) {
			$this->addResource($resource);
			$this->allow(explode(',',$roles),$resource);
		}

		// initialise role
		$this->role = \Zend_Auth::getInstance()->hasIdentity() ? \Zend_Auth::getInstance()->getIdentity()->role : "guest";
	}

	/**
	 * Check whether the current role is allowed access to a resource.
	 * If the first argument is a role or a role identifier, the second one must be a resource (or an identifier
	 * thereof). If the first argument is a resource (identifier), the ACL assumes the requested role is the one the
	 * current identity possesses.
	 *
	 * @param null|string|\Zend_Acl_Role_Interface $role Either identity role or a resource
	 * @param null|string|\Zend_Acl_Role_Interface $resource A resource, a privilege or nothing
	 * @param null|string $privilege
	 *
	 * @return bool|void
	 */
	public function isAllowed ($role = null, $resource = null, $privilege = null) {
		if ($this->hasRole($role)) {
			return parent::isAllowed($role, $resource, $privilege);
		} else if ($this->has($role)) {
			return parent::isAllowed($this->role, $role, $resource);
		} else {
			return false;
		}
	}
}
