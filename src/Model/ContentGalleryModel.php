<?php
namespace Mingos\uCMS\Model;

class ContentGalleryModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "content_gallery";

	/**
	 * @var GalleryModel
	 */
	protected $modelGallery;

	public function init()
	{
		$this->modelGallery = new GalleryModel();
	}

	/**
	 * Fetch galleries related to a given content
	 * @param  integer $ccid     Content's CID
	 * @return array
	 */
	public function getRelatedGalleries($ccid)
	{
		$relations = $this->_db->fetchAll(
			$this->select()
				->where("ccid = ?", $ccid)
				->order("gcid ASC")
		);

		// transform to galleries and return
		$modelGallery = $this->modelGallery;
		$i = 1;
		return array_map(function ($relation) use ($modelGallery, &$i) {
			$gallery = $modelGallery->getById($relation["gcid"]);
			return array(
				"content_title" => $gallery["content_title"],
				"cid" => $gallery["id"],
				"id" => $gallery["id"],
				"position" => $i++
			);
		}, $relations);
	}

	/**
	 * Get related gallery by position
	 * @param  integer $position
	 * @param  integer $ccid
	 * @return array
	 */
	public function getByPosition($position, $ccid) {
		$relation = $this->_db->fetchRow(
			$this->select()
				->where("ccid = ?", $ccid)
				->order("gcid ASC")
				->limit(1, $position - 1)
		);

		return $this->modelGallery->getById($relation["gcid"]);
	}

	/**
	 * Fetch galleries not related to a given content
	 * @param  integer $ccid
	 * @return array
	 */
	public function getUnrelatedGalleries($ccid)
	{
		$galleries = $this->modelGallery->getAdapter()->fetchAll(
			$this->modelGallery->select()
				->where("id NOT IN ?", $this->select()->from($this->_name, array("gcid"))->where("ccid = ?", $ccid))
		);

		$modelGallery = $this->modelGallery;
		return array_map(function ($gallery) use ($modelGallery) {
			$gallery = $modelGallery->getById($gallery["id"]);
			return array(
				"content_title" => $gallery["content_title"],
				"cid" => $gallery["id"],
				"id" => $gallery["id"]
			);
		}, $galleries);
	}

	/**
	 * Relate a piece of content with a gallery
	 * @param  integer                      $ccid
	 * @param  integer                      $gcid
	 * @return \Zend_Db_Statement_Interface
	 */
	public function relate($ccid, $gcid)
	{
		return $this->_db->query(
			"INSERT IGNORE INTO {$this->_name} (ccid, gcid) VALUES (:ccid, :gcid)",
			array("ccid" => $ccid, "gcid" => $gcid)
		);
	}

	/**
	 * Remove relation between a piece of content and a gallery
	 * @param  integer $ccid
	 * @param  integer $gcid
	 * @return integer       Number of removed rows
	 */
	public function unrelate($ccid, $gcid)
	{
		return $this->delete("ccid = " . $this->_db->quote($ccid) . " AND gcid = " . $this->_db->quote($gcid));
	}
}
