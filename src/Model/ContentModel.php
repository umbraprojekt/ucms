<?php
namespace Mingos\uCMS\Model;

use Mingos\uCMS\Service\Cache;

class ContentModel extends \Zend_Db_Table_Abstract
{
	/**
	 * @var Cache
	 */
	protected $cache;

	/**
	 * The default table name
	 */
	protected $_name = 'content';

	public function init()
	{
		$this->cache = new Cache();
	}

	/**
	 * @param string $type Content type
	 * @param array  $data Content data
	 * @return \Zend_Db_Table_Row_Abstract
	 */
	public function createFromArray($type, array $data) {
		$data["type"] = $type;
		$data["title"] = $data["title"] ? $data["title"] : $data["content_title"];
		$data["uid"] = $data["uid"] ? $data["uid"] : \Zend_Auth::getInstance()->getIdentity()->id;
		$data["created"] = !empty($data["created"]) ? $data["created"] : new \Zend_Db_Expr("CURRENT_TIMESTAMP");

		$content = $this->createRow(array(
			"type" => $data["type"],
			"content_title" => $data["content_title"],
			"title" => $data["title"],
			"uid" => $data["uid"],
			"created" => $data["created"]
		));
		$content->save();

		return $content;
	}

	/**
	 * @param integer $id Content ID
	 * @param array $data Content data
	 * @return \Zend_Db_Table_Row_Abstract
	 * @throws \Zend_Db_Exception
	 */
	public function updateFromArray($id, array $data) {
		$data["title"] = $data["title"] ? $data["title"] : $data["content_title"];
		$data["created"] = !empty($data["created"]) ? $data["created"] : new \Zend_Db_Expr("CURRENT_TIMESTAMP");
		$data["updated"] = new \Zend_Db_Expr("CURRENT_TIMESTAMP");
		$data["updated_uid"] = \Zend_Auth::getInstance()->getIdentity()->id;

		$content = $this->fetchRow(array("id = ?" => $id));
		if (!$content) {
			throw new \Zend_Db_Exception("Content not found.");
		}

		$content->content_title = $data["content_title"];
		$content->title = $data["title"];
		$content->created = $data["created"];
		$content->updated = $data["updated"];
		$content->updated_uid = $data["updated_uid"];
		$content->save();

		return $content;
	}

	/**
	 * Fetch a node by its content ID
	 * This function is agnostic of what the actual content type is and executes
	 * an extra query to find out which table to query for the node.
	 *
	 * @param  int    $cid content ID
	 * @return array
	 */
	public function getNode($cid)
	{
		$db = $this->getAdapter();
		$table = $db->query("SELECT `table` FROM content_type INNER JOIN content ON content.type = content_type.id WHERE content.id = ?",array($cid))->fetchColumn(0);
		if (empty($table)) {
			return null;
		} else {
			return $db->query(
				"SELECT t.*, a.alias, c.title
				FROM {$table} AS t
				INNER JOIN alias AS a ON t.id = a.id
				INNER JOIN content AS c ON t.id = c.id
				WHERE t.id = ?",
				array($cid)
			)->fetch();
		}
	}

	/**
	 * Get taxonomy for a given piece of content
	 *
	 * @param  integer $cid
	 * @return array
	 */
	public function getTaxonomy($cid)
	{
		$taxonomy = $this->fetchAll(
			$this->select()
				->from(array("t" => "taxonomy"))
				->setIntegrityCheck(false)
				->join(array("ct" => "content_taxonomy"), "ct.tid = t.id", array())
				->where("ct.cid = ?", $cid)
		);

		$return = array();
		foreach ($taxonomy as $t) {
			$return[$t->id] = $t;
		}

		return $return;
	}

	/**
	 * Inserts a new row.
	 *
	 * @param  array $data  Column-value pairs.
	 * @return mixed The primary key of the row inserted.
	 */
	public function insert (array $data) {
		$this->cache->clear();
		return parent::insert($data);
	}

	/**
	 * Updates existing rows.
	 *
	 * @param  array        $data  Column-value pairs.
	 * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
	 * @return int          The number of rows updated.
	 */
	public function update (array $data, $where) {
		$this->cache->clear();
		return parent::update($data, $where);
	}

	/**
	 * Deletes existing rows.
	 *
	 * @param  array|string $where SQL WHERE clause(s).
	 * @return int          The number of rows deleted.
	 */
	public function delete ($where) {
		$this->cache->clear();
		return parent::delete($where);
	}
}
