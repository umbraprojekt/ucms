<?php
namespace Mingos\uCMS\Model;

class ContentTypeModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "content_type";

	const TYPE_PAGE = 1;
	const TYPE_NEWS = 2;
	const TYPE_GALLERY = 3;
	const TYPE_VIEW = 4;
	const TYPE_BLOCK = 5;
	const TYPE_PRODUCT = 6;
}
