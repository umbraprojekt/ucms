<?php
namespace Mingos\uCMS\Model;

class UserModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "user";

	/**
	 * Fetch all active users
	 * @param  bool $paginator return in the form of a SQL query object?
	 * @return array|\Zend_Db_Table_Select
	 */
	public function getAll($paginator = false)
	{
		if (!$paginator)
			return $this->_db->query("
				SELECT id, login, created, last_access, email, role
				FROM {$this->_name}
				WHERE active = '1'
				ORDER BY created ASC
			")->fetchAll();
		else
			return $this
				->select()
				->where("active = '1'")
				->order('created ASC')
			;
	}

	/**
	 * Fetch the data of a single identity
	 * @param  int $id User ID
	 * @return array
	 */
	public function getById($id)
	{
		return $this->_db->fetchRow("SELECT * FROM {$this->_name} WHERE id = ?",array($id));
	}

	/**
	 * Fetch only the identity name of a single identity
	 * @param  int $id User ID
	 * @return string User name
	 */
	public function getUsernameById($id)
	{
		return $this->_db->fetchOne("SELECT login FROM {$this->_name} WHERE id = ?",array($id));
	}
}
