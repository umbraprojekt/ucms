<?php
namespace Mingos\uCMS\Model;

use Mingos\uCMS\Service\Cache;

class ImageModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "image";

	/**
	 * @var Cache
	 */
	protected $cache;

	public function init()
	{
		$this->cache = new Cache();
	}

	public function getById($id)
	{
		if (!($return = $this->cache->get("{$this->_name}_{$id}"))) {
			$return = $this->_db->fetchRow(
				$this
				->select()
				->setIntegrityCheck(false)
				->from(array('i' => $this->_name), array(
					'i.id',
					'i.table',
					'i.crop',
					'i.cut',
					'i.title',
					'i.alt',
					'i.path',
					'i.filename',
					'i.url',
					'i.nid',
					"i.update"
				))
				->join(array('f' => 'file'), 'i.iid = f.id', array(
					'fid' => 'f.id',
					'orig_path' => 'f.path',
					'orig_filename' => 'f.filename',
					'f.extension',
					'f.mime'
				))
				->where('i.id = ?',$id)
			);
			$this->cache->put("{$this->_name}_{$id}", $return);
		}
		return $return;
	}

	public function getByFileId($fileId)
	{
		$id = $this->_db
			->query("SELECT id FROM {$this->_name} WHERE iid = :fileId", array("fileId" => $fileId))
			->fetchColumn();

		if ($id) {
			return $this->getById($id);
		}

		return null;
	}

	/**
	 * Fetch record IDs according to the specified set of rules
	 *
	 * @param  array $params Optional parametres<br>
	 *                       string [table]     => table name to which the images are assigned<br>
	 *                       int    [nid]       => content node ID to which the images are assigned<br>
	 *                       int    [id]        => the image_list ID<br>
	 *                       bool   [paginator] => whether to return a Zend_Db_Select
	 * @return array|\Zend_Db_Select
	 */
	public function get($params = array())
	{
		$sql = $this->select()->from($this->_name,array('id'));

		// table
		if (array_key_exists('table',$params) && !empty($params['table'])) {
			$sql->where('`table` = ?',$params['table']);
		}

		// node ID
		if (array_key_exists('nid',$params) && !empty($params['nid'])) {
			$sql->where('nid = ?',$params['nid']);
		}

		// image ID
		if (array_key_exists('id',$params) && !empty($params['id'])) {
			$sql->where('id = ?',$params['id']);
		}

		$sql->order('weight ASC');

		// return the paginator or an array
		if (array_key_exists('paginator',$params) && $params['paginator']) {
			return $sql;
		} else {
			$result = $this->_db->fetchAll($sql);
			$return = array();
			foreach ($result as $item) {
				$return[$item['id']] = $this->getById($item['id']);
			}
			return $return;
		}
	}

	/**
	 * Inserts a new row.
	 *
	 * @param  array  $data  Column-value pairs.
	 * @return mixed         The primary key of the row inserted.
	 */
	public function insert(array $data)
	{
		return parent::insert($data);
	}

	/**
	 * Updates existing rows.
	 *
	 * @param  array        $data  Column-value pairs.
	 * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
	 * @return int          The number of rows updated.
	 */
	public function update(array $data, $where)
	{
		$this->cache->clear();
		return parent::update($data, $where);
	}

	/**
	 * Deletes existing rows.
	 *
	 * @param  array|string $where SQL WHERE clause(s).
	 * @return int          The number of rows deleted.
	 */
	public function delete($where)
	{
		$this->cache->clear();
		return parent::delete($where);
	}
}
