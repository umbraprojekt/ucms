<?php
namespace Mingos\uCMS\Model;

class ContentTaxonomyModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "content_taxonomy";

	/**
	 * Assign taxonomy terms to a given content
	 * @param integer $cid
	 * @param array $tids
	 */
	public function assignContent($cid, $tids)
	{
		$this->delete($this->_db->quoteInto("cid = ?", $cid));

		$query = "INSERT INTO content_taxonomy (cid, tid) VALUES ";

		$cid = $this->_db->quote($cid);
		foreach ($tids as $idx => $tid) {
			$tids[$idx] = $this->_db->quoteInto("({$cid}, ?)", $tid);
		}
		$query .= implode(",", $tids);

		$this->_db->query($query);
	}
}
