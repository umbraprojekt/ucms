<?php
namespace Mingos\uCMS\Model;

class MenuModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "menu";

	/**
	 * Fetch all menus
	 * @return array
	 */
	public function getAll()
	{
		return $this->_db->fetchAll($this->select());
	}

	/**
	 * Fetch a menu by its unique ID
	 * @param int $id
	 * @return array
	 */
	public function getById($id)
	{
		return $this->_db->fetchRow($this->select()->where("id = ?", $id));
	}

	/**
	 * Inserts a new row.
	 *
	 * @param  array  $data  Column-value pairs.
	 * @return mixed         The primary key of the row inserted.
	 */
	public function insert(array $data)
	{
		\Zend_Registry::get('Cache')->remove('menu');
		return parent::insert($data);
	}

	/**
	 * Updates existing rows.
	 *
	 * @param  array        $data  Column-value pairs.
	 * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
	 * @return int          The number of rows updated.
	 */
	public function update(array $data, $where)
	{
		\Zend_Registry::get('Cache')->remove('menu');
		return parent::update($data, $where);
	}

	/**
	 * Deletes existing rows.
	 *
	 * @param  array|string $where SQL WHERE clause(s).
	 * @return int          The number of rows deleted.
	 */
	public function delete($where)
	{
		\Zend_Registry::get('Cache')->remove('menu');
		return parent::delete($where);
	}
}
