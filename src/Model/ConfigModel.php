<?php
namespace Mingos\uCMS\Model;

class ConfigModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "config";

	/**
	 * Parametre type that will be set/retrieved
	 * @var string
	 */
	private $_type;

	/**
	 * Set options
	 * @param string $type Paramtre type
	 */
	public function __construct($type = null)
	{
		$this->_type = $type;
		parent::__construct();
	}

	/**
	 * Set a single parametre value
	 * @param string $param
	 * @param mixed $value
	 * @param string $type Parametre type to set
	 * @throws \Zend_Db_Table_Exception
	 */
	public function setParam($param, $value, $type = null)
	{
		$this->ensureTypeIsPresent($type);
		$t = !empty($type) ? $type : $this->_type;
		$this->_db->query(
			"INSERT INTO {$this->_name} (type,param,value) VALUES (?,?,?)
			ON DUPLICATE KEY UPDATE value = VALUES(value)",
			array($t, $param, $value)
		);
	}

	/**
	 * Set parametres
	 * @param array $params Key-value pairs
	 * @param string $type Parametre type to set
	 * @throws \Zend_Db_Table_Exception
	 */
	public function setParams($params, $type = null)
	{
		$this->ensureTypeIsPresent($type);
		$t = !empty($type) ? $type : $this->_type;
		foreach ($params as $param => $value) {
			$this->_db->query(
				"INSERT INTO {$this->_name} (type,param,value) VALUES (?,?,?)
				ON DUPLICATE KEY UPDATE value = VALUES(value)",
				array($t, $param, $value)
			);
		}
	}

	/**
	 * Fetch all parametres for a given type
	 * @param  string $type Parametre type to retrieve
	 * @return array param-value pairs
	 * @throws \Zend_Db_Table_Exception if parametre type is not specified
	 */
	public function getParams($type = null)
	{
		$this->ensureTypeIsPresent($type);
		$t = !empty($type) ? $type : $this->_type;
		return $this->_db->fetchPairs("SELECT param, value FROM (
			SELECT * FROM {$this->_name} WHERE type = :type
		) AS t GROUP BY param", array("type" => $t));
	}

	/**
	 * Fetch a specific parametre value for a given type
	 * @param  string $param Parametre to retrieve
	 * @param  string $type Parametre type to retrieve
	 * @return string|bool   A string containing the parametre's value or false if such a parametre does not exist
	 * @throws \Zend_Db_Table_Exception if parametre type is not specified
	 */
	public function getParam($param, $type = null)
	{
		$this->ensureTypeIsPresent($type);
		$t = !empty($type) ? $type : $this->_type;
		return $this->_db->fetchOne("SELECT value FROM (
			SELECT * FROM {$this->_name} WHERE param = :param AND type = :type
		) AS t GROUP BY param", array("type" => $t, "param" => $param));
	}

	private function ensureTypeIsPresent($type) {
		if (empty($this->_type) && empty($type)) {
			throw new \Zend_Db_Table_Exception(
				\Zend_Registry::get("Zend_Translate")->getAdapter()->translate("Invalid parametres."),
				500
			);
		}
	}
}
