<?php
namespace Mingos\uCMS\Model;

class AutopublishModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "autopublish";

	/**
	 * @param integer          $cid     Content ID
	 * @param integer          $publish 0 or 1
	 * @param string|\DateTime $date    Either the date string or a DateTime object
	 */
	public function set($cid, $publish, $date = null)
	{
		if ($date) {
			if ($date instanceof \DateTime) {
				$date = $date->format("Y-m-d H:i:s");
			}
			$this->_db->query(<<<EOS
				INSERT INTO autopublish (cid, publish, date)
				VALUES (:cid, :publish, :date)
				ON DUPLICATE KEY UPDATE date = VALUES(date)
EOS
					, array("cid" => $cid, "date" => $date, "publish" => $publish)
				);
		} else {
			$this->delete(array("cid = ?" => $cid, "publish = ?" => $publish));
		}
	}

	/**
	 * @param  integer        $cid     Content ID
	 * @param  integer        $publish 0 or 1
	 * @return \DateTime|null
	 */
	public function get($cid, $publish)
	{
		$result = $this->_db->fetchRow(
			$this->select()
				->where("cid = ?", $cid)
				->where("publish = ?", $publish)
		);

		if ($result) {
			return new \DateTime($result["date"]);
		}

		return null;
	}

	/**
	 * Execute the autopublish and either publish or unpublish all affected nodes
	 */
	public function execute()
	{
		// we only want to execute things if the CMS is installed and the tables are in place...
		try {
			$results = $this->_db->fetchAll(
				$this->select()
					->where("date < CURRENT_TIMESTAMP")
			);
		} catch (\Exception $ex) {
			return;
		}

		if (empty($results)) {
			return;
		}

		$contentModel = new ContentModel();
		foreach ($results as $result) {
			$content = $contentModel->fetchRow(
				$contentModel->select()
					->setIntegrityCheck(false)
					->from(array("c" => "content"))
					->where("c.id = ?", $result["cid"])
					->join(array("t" => "content_type"), "c.type = t.id")
			);
			$contentModel->getAdapter()->query(
				"UPDATE `{$content["table"]}` SET published = :pub WHERE id = :id",
				array("pub" => $result["publish"], "id" => $result["cid"])
			);
			$this->set($result["cid"], $result["publish"], null);
		}
	}
}
