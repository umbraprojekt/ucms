<?php
namespace Mingos\uCMS\Model;

class NewsModel extends AbstractNodeModel
{
	protected $_name = "node_news";

	const TYPE = ContentTypeModel::TYPE_NEWS;

	/**
	 * Fetch a single node by its ID (cached)
	 *
	 * @param  int  $id
	 * @param  bool $noCache Override any caching
	 * @return array
	 */
	public function getById($id, $noCache = false)
	{
		if ($noCache || !$node = \Zend_Registry::get('Cache')->load("{$this->_name}_{$id}")) {
			// node info from the database
			$node = $this->_db->fetchRow(
				$this
					->select()
					->setIntegrityCheck(false)
					->from(array('n' => $this->_name))
					->join(array('c' => 'content'), 'n.id = c.id', array("id", "content_title", "title", "created", "updated"))
					->join(array('a' => 'alias'),'a.id = n.id',array('alias' => 'a.alias'))
					->join(array('u' => 'user'), 'u.id = c.uid', array('author' => 'u.login'))
					->joinLeft(array('u2' => 'user'), 'u2.id = c.updated_uid',array('updated_author' => 'u2.login'))
					->where('n.id = ?', $id)
			);
			$node['created'] = new \DateTime($node['created']);
			$node['updated'] = !empty($node['updated']) ? new \DateTime($node['updated']) : null;

			// autopublish
			$modelAutopublish = new AutopublishModel();
			$node["autopublish"] = $modelAutopublish->get($id, 1);
			$node["autounpublish"] = $modelAutopublish->get($id, 0);

			//info for the RSS feed
			$node['feed'] = array(
				'title' => $node['title'],
				'link' => "http://".APPLICATION_DOMAIN."/{$node['alias']}.html",
				'description' => $node['body'],
				'lastUpdate' => $node['created']->getTimestamp(),
				'author' => $node['author']
			);

			// comment count
			$commentModel = new CommentModel();
			$node["comments_active"] = $commentModel->getCommentCount($id, 1);
			$node["comments_inactive"] = $commentModel->getCommentCount($id, 0);

			//info for the sitemap
			$node['sitemap_data'] = array(
				'label' => $node['title'],
				'lastmod' => $node['updated'] == null ? $node['created']->format('Y-m-d') : $node['updated']->format('Y-m-d'),
				'uri' => '/'.$node['alias'].'.html',
				'priority' => $node['seo_priority'],
				'changefreq' => $node['seo_changefreq']
			);

			$node["image"] = $this->getNodeImage($id);
			$node["taxonomy"] = $this->getNodeTaxonomy($id);

			if (!$noCache) \Zend_Registry::get('Cache')->save($node,"{$this->_name}_{$id}");
		}

		return $node;
	}

	/**
	 * Fetch the next inserted node (if any)
	 *
	 * @param  int $id
	 * @return array
	 */
	public function getNext($id)
	{
		$result = $this->_db->fetchOne(
			$this->select()
				->setIntegrityCheck(false)
				->from(array("n" => $this->_name), array("id"))
				->join(array("c" => "content"), "n.id = c.id", array())
				->where("c.created > ?", new \Zend_Db_Expr($this->_db->quoteInto("(SELECT created FROM content WHERE id = ?)", $id)))
				->where("n.published = ?", "1")
				->order("c.created ASC")
		);
		if ($result) return $this->getById($result);
		else return array();
	}

	/**
	 * Fetch the previous inserted node (if any)
	 *
	 * @param  int $id
	 * @return array
	 */
	public function getPrev($id)
	{
		$result = $this->_db->fetchOne(
			$this->select()
				->setIntegrityCheck(false)
				->from(array("n" => $this->_name), array("id"))
				->join(array("c" => "content"), "n.id = c.id", array())
				->where("c.created < ?", new \Zend_Db_Expr($this->_db->quoteInto("(SELECT created FROM content WHERE id = ?)", $id)))
				->where("n.published = ?", "1")
				->order("c.created DESC")
		);
		if ($result) return $this->getById($result);
		else return array();
	}
}
