<?php
namespace Mingos\uCMS\Model;

class FileModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "file";

	public function getById($id)
	{
		if (!($return = \Zend_Registry::get("Cache")->load("{$this->_name}_{$id}"))) {
			$return = $this->_db->fetchRow("SELECT * FROM {$this->_name} WHERE id = ?",array($id));
			\Zend_Registry::get("Cache")->save($return, "{$this->_name}_{$id}");
		}
		return $return;
	}

	/**
	 * Fetch image or images
	 *
	 * @param  int|array $id Image ID or an array thereof
	 * @return array
	 */
	public function get($id)
	{
		if (!is_array($id)) {
			$return = $this->getById($id);
		} else {
			$return = array();
			foreach ($id as $item) {
				$return[$item] = $this->getById($item);
			}
		}
		return $return;
	}

	/**
	 * Inserts a new row.
	 *
	 * @param  array  $data  Column-value pairs.
	 * @return mixed         The primary key of the row inserted.
	 */
	public function insert(array $data)
	{
		\Zend_Registry::get('Cache')->remove($this->_name);
		return parent::insert($data);
	}

	/**
	 * Updates existing rows.
	 *
	 * @param  array        $data  Column-value pairs.
	 * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
	 * @return int          The number of rows updated.
	 */
	public function update(array $data, $where)
	{
		\Zend_Registry::get('Cache')->remove($this->_name);
		return parent::update($data, $where);
	}

	/**
	 * Deletes existing rows.
	 *
	 * @param  array|string $where SQL WHERE clause(s).
	 * @return int          The number of rows deleted.
	 */
	public function delete($where)
	{
		\Zend_Registry::get('Cache')->remove($this->_name);
		return parent::delete($where);
	}
}
