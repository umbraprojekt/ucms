<?php
namespace Mingos\uCMS\Model;

class ViewModel extends AbstractNodeModel
{
	protected $_name = 'node_view';

	const TYPE = ContentTypeModel::TYPE_VIEW;

	/**
	 * Fetch a single node by its ID (cached)
	 *
	 * @param  int $id
	 * @param  bool $noCache Override any caching
	 * @return array
	 */
	public function getById ($id, $noCache = false) {
		if ($noCache || !$node = \Zend_Registry::get('Cache')->load("{$this->_name}_{$id}")) {
			// node info from the database
			$node = $this->_db->fetchRow(
				$this
					->select()
					->setIntegrityCheck(false)
					->from(array('n' => $this->_name))
					->join(array('c' => 'content'), 'n.id = c.id', array("id", "content_title", "title", "created", "updated"))
					->join(array('a' => 'alias'),'a.id = n.id',array('alias' => 'a.alias', 'module' => 'a.module', 'controller' => 'a.controller', 'action' => 'a.action'))
					->join(array('u' => 'user'), 'u.id = c.uid', array('author' => 'u.login'))
					->joinLeft(array('u2' => 'user'), 'u2.id = c.updated_uid',array('updated_author' => 'u2.login'))
					->where('n.id = ?', $id)
			);
			$node['created'] = new \DateTime($node['created']);
			$node['updated'] = !empty($node['updated']) ? new \DateTime($node['updated']) : null;

			//info for the sitemap
			$node['sitemap_data'] = array(
				'label' => $node['title'],
				'lastmod' => $node['updated'] == null ? $node['created']->format('Y-m-d') : $node['updated']->format('Y-m-d'),
				'uri' => '/'.$node['alias'].'.html',
				'priority' => $node['seo_priority'],
				'changefreq' => $node['seo_changefreq']
			);

			$node["image"] = $this->getNodeImage($id);
			$node["taxonomy"] = $this->getNodeTaxonomy($id);

			if (!$noCache) \Zend_Registry::get('Cache')->save($node,"{$this->_name}_{$id}");
		}

		return $node;
	}
}
