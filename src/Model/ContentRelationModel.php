<?php
namespace Mingos\uCMS\Model;

class ContentRelationModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "content_relation";

	/**
	 * Get all related content
	 *
	 * @param int $id Content ID
	 * @return \Zend_Db_Table_Rowset_Abstract
	 */
	public function getRelated($id)
	{
		$sql = $this
			->select()
			->setIntegrityCheck(false)
			->from(array("cr" => $this->_name), array("sticky"))
			->join(array("c" => "content"), "cr.relation_cid = c.id", array("id", "title"))
			->join(array("ct" => "content_type"), "c.type = ct.id", array("content_type" => "name"))
			->where("cr.cid = ?", $id);

		$results = $this->fetchAll($sql);

		return $results;
	}

	/**
	 * Get all unrelated content
	 *
	 * @param int $id Content ID
	 * @return \Zend_Db_Table_Rowset_Abstract
	 */
	public function getUnrelated($id)
	{
		$sql = $this
			->select()
			->setIntegrityCheck(false)
			->from(array("cr" => $this->_name), array())
			->joinRight(array("c" => "content"), "cr.cid = c.id", array("id", "title"))
			->join(array("ct" => "content_type"), "c.type = ct.id", array("content_type" => "name"))
			->order("ct.id ASC")
			->order("c.title ASC")
			->where("c.id <> ?", $id)
			->where("c.id NOT IN(?)", new \Zend_Db_Expr($this->_db->quoteInto("SELECT relation_cid FROM content_relation WHERE cid = ?", $id)));

		return $this->fetchAll($sql);
	}

	/**
	 * Set a relation
	 *
	 * @param integer $ownerCid    Owner CID
	 * @param integer $relationCid Related CID
	 * @param integer $sticky      Sticky flag (0|1)
	 */
	public function set($ownerCid, $relationCid, $sticky = 0)
	{
		$this->_db->query(
			"INSERT INTO {$this->_name} (cid, relation_cid, sticky) VALUES (:cid, :rel, :sti)
			ON DUPLICATE KEY UPDATE sticky=VALUES(sticky)",
			array("cid" => $ownerCid, "rel" => $relationCid, "sti" => $sticky)
		);
	}

	/**
	 * @param integer $ownerCid    Owner CID
	 * @param integer $relationCid Relation CID
	 */
	public function remove($ownerCid, $relationCid)
	{
		$this->delete(array("cid = ?" => $ownerCid, "relation_cid = ?" => $relationCid));
	}
}
