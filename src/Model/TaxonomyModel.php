<?php
namespace Mingos\uCMS\Model;

class TaxonomyModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "taxonomy";

	/**
	 * Fetch all taxonomy terms belonging to a given dictionary
	 * @param integer $did Dictionary ID
	 * @return \Zend_Db_Table_Rowset_Abstract
	 */
	public function getAllTerms($did)
	{
		$results = $this->fetchAll(
			$this->select()
				->where("did = ?", $did)
				->order("left ASC")
		);

		return $results;
	}

	/**
	 * Fetch all taxonomy terms and their respective dictionaries
	 * @return array
	 */
	public function getAllByDictionary()
	{
		$dictionaries = $this->fetchAll(
			$this->select()
				->from(array("d" => "taxonomy_dictionary"))
				->setIntegrityCheck(false)
				->order("d.id asc")
		);

		$taxonomy = $this->fetchAll(
			$this->select()
				->from(array("t" => "taxonomy"))
				->setIntegrityCheck(false)
				->order("t.left ASC")
		);

		$return = array();
		foreach ($dictionaries as $d) {
			$return[$d->id] = array(
				"dictionary" => $d,
				"terms" => array()
			);
		}
		foreach ($taxonomy as $t) {
			$return[$t->did]["terms"][$t->id] = $t;
		}

		return $return;
	}
}
