<?php
namespace Mingos\uCMS\Model;

class MenuItemModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "menu_item";

	/**
	 * Fetch a menu item by its unique ID
	 * @param  int $id
	 * @return array
	 */
	public function getById($id)
	{
		return $this->_db->fetchRow($this->select()->where("id = ?", $id));
	}

	/**
	 * Fetch all items within a given menu
	 * @param  int    $mid menu ID
	 * @return array
	 */
	public function getAll($mid)
	{
		return $this->nestify($this->_db->fetchAll(
			$this->select()
				->setIntegrityCheck(false)
				->from(array("i" => $this->_name))
				->joinLeft(array("f" => "file"), "f.id = i.file_id", array("file" => new \Zend_Db_Expr("CONCAT(f.path,'/',f.filename,'.',f.extension)")))
				->where("i.mid = ?", $mid)
				->order("i.left ASC")
		));
	}

	/**
	 * Fetch internal links from the menu in order to create a sitemap.
	 * @param  int    $mid menu ID
	 * @return array
	 */
	public function getSitemap($mid)
	{
		$items = $this->getAll($mid);
		return $this->sitemap($items['items']);
	}

	/**
	 * Nests menu items within each other
	 * @param  array $array A flat array of menu items
	 * @return array
	 */
	private function nestify($array)
	{
		$nested = array();
		$processed = array();
		foreach ($array as $arr) {
			$arr['children'] = array();
			if ($arr['depth'] == 0) {
				$nested[$arr['id']] = $arr;
				$processed[$arr['id']] = &$nested[$arr['id']];
			}
			else {
				$processed[$arr['parent']]['children'][$arr['id']] = $arr;
				$processed[$arr['id']] = &$processed[$arr['parent']]['children'][$arr['id']];
			}
		}
		return $nested;
	}

	/**
	 * Generates items for Zend_Navigation
	 * @param  array $items Nestified array of menu items
	 * @return array
	 */
	private function sitemap($items)
	{
		$model = new ContentModel();
		$map = array();
		foreach ($items as $item) {
			if ($item['cid'] > 0) {
				$node = $model->getNode($item['cid']);
				$tmp = array(
					'label' => $item['label'],
					'uri' => $item['href'],
					'priority' => $node['seo_priority'],
					'changefreq' => $node['seo_changefreq']
				);
				if ($node['updated'] == null) $date = new \Zend_Date($node['created'], \Zend_Date::ISO_8601);
				else $date = new \Zend_Date($node['updated'], \Zend_Date::ISO_8601);
				$tmp['lastmod'] = $date->getIso();

				if (count($item['children']) > 0) $tmp['pages'] = $this->sitemap($item['children']);

				$map[] = $tmp;
			}
		}
		return $map;
	}

	/**
	 * Place menu items with no parent at depth 0
	 */
	public function updateOrphans()
	{
		$this->getAdapter()->query(
			"UPDATE menu_item AS m1
			LEFT JOIN menu_item AS m2 ON m1.parent = m2.id
			SET m1.parent = 0, m1.depth = 0
			WHERE m2.id IS NULL"
		);
	}

	/**
	 * Update the href of a menu item when an alias is updated
	 * @param integer $cid Content ID
	 */
	public function updateHref($cid)
	{
		$this->getAdapter()->query(
			"UPDATE menu_item
			SET href = CONCAT('/', (
				SELECT alias
				FROM alias
				WHERE id = :cid
			), '.html')
			WHERE cid = :cid",
			array("cid" => $cid)
		);
	}

	/**
	 * Updates the labels of menu items when an alias is updated
	 * @param int    $cid      Content ID
	 * @param string $oldLabel Node title before the update
	 * @param string $newLabel Node title after the update
	 */
	public function updateLabel($cid, $oldLabel, $newLabel)
	{
		$this->getAdapter()->query(
			"UPDATE menu_item
			SET label = :label
			WHERE cid = :cid AND label = :oldlabel",
			array("label" => $newLabel, "cid" => $cid, "oldlabel" => $oldLabel));
	}

	/**
	 * Overridden insert method.
	 */
	public function insert(array $data)
	{
		\Zend_Registry::get('Cache')->remove('menu');
		return parent::insert($data);
	}

	/**
	 * Overridden update method.
	 */
	public function update(array $data, $where)
	{
		\Zend_Registry::get('Cache')->remove('menu');
		return parent::update($data, $where);
	}

	/**
	 * Overridden delete method.
	 */
	public function delete($where)
	{
		\Zend_Registry::get('Cache')->remove('menu');
		return parent::delete($where);
	}
}
