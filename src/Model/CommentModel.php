<?php
namespace Mingos\uCMS\Model;

class CommentModel extends \Zend_Db_Table_Abstract
{
	protected $_name = "comment";

	/**
	 * Fetch a given comment by its ID
	 *
	 * @param  int   $id Comment ID
	 * @return array
	 */
	public function getById($id)
	{
		if (!$comment = \Zend_Registry::get('Cache')->load("{$this->_name}_{$id}")) {
			$sql =
				$this
					->select()
					->setIntegrityCheck(false)
					->from(array("com" => $this->_name))
					->join(array("con" => "content"), "com.cid = con.id", array("title"))
					->where('com.id = ?',$id)
			;
			$comment = $this->_db->fetchRow($sql);
			$comment['created'] = new \DateTime($comment['created']);
			\Zend_Registry::get('Cache')->save($comment,"{$this->_name}_{$id}");
		}

		return $comment;
	}

	/**
	 * Fetch all comments for a given content node
	 *
	 * @param  int      $cid    The node's content ID
	 * @param  int|null $active
	 * @return array
	 */
	public function getByCid($cid, $active = null)
	{
		$sql =
			$this
			->select()
			->from($this->_name, array('id'))
			->where('cid = ?', $cid)
			->order('created ASC')
		;
		if ($active !== null) $sql->where("active = '?'",intval($active));

		$return = array();
		foreach ($this->_db->fetchAll($sql) as $item) {
			$return[] = $this->getById($item['id']);
		}

		return $return;
	}

	/**
	 * Fetch all comments according to optional parametres
	 *
	 * @param  array                 $params An array of additional parametres<br>
	 *                                       bool [paginator] => return a SQL statement instead of an array<br>
	 *                                       int  [active]    => fetch only comments with a given active state
	 * @return array|\Zend_Db_Select
	 */
	public function getAll($params = array())
	{
		$sql =
			$this
			->select()
			->from($this->_name, array('id'))
		;

		if (array_key_exists('active', $params) && in_array($params['active'],array(0,1))) $sql->where('active = ?',strval($params['active']));
		if (array_key_exists('paginator', $params) && $params['paginator']) {
			return $sql;
		} else {
			$return = array();
			foreach($this->_db->fetchAll($sql) as $item) {
				$return[] = $this->getById($item['id']);
			}
			return $return;
		}
	}

	/**
	 * Fetch the title of the content node the comment refers to.
	 *
	 * @param  int    $cid The node's content ID
	 * @return string
	 */
	public function getContentTitle($cid)
	{
		$col = $this->_db->fetchCol(
			$this->select()
				->setIntegrityCheck(false)
				->from(array("c" => "content"), array("title"))
				->where("id = ?", $cid)
		);
		return $col[0];
	}

	/**
	 * Approves/disapproves a comment
	 *
	 * @param  int $id Comment ID
	 * @return int Comment's new active status
	 */
	public function toggleActive($id)
	{
		$this->update(array(
			'active' => new \Zend_Db_Expr("IF(active = '0','1','0')")
		), "id = ".$this->_db->quote($id));
		\Zend_Registry::get('Cache')->clean(\Zend_Cache::CLEANING_MODE_ALL);
		return $this->_db->fetchOne("SELECT active FROM {$this->_name} WHERE id = ?",array($id));
	}

	/**
	 * Get the comment count for a given node
	 * @param  integer $cid
	 * @param  integer $active
	 * @return integer
	 */
	public function getCommentCount($cid, $active = -1)
	{
		$sql = $this->select()
			->setIntegrityCheck(false)
			->from($this->_name, array("count" => new \Zend_Db_Expr("COUNT(1)")))
			->where("cid = ?", $cid);
		if (in_array($active, array(0, 1))) {
			$sql->where("active = ?", "{$active}");
		}

		return intval($this->getAdapter()->fetchOne($sql));
	}
}
