<?php
namespace Mingos\uCMS\Validator;

class FileTypeValidator extends \Zend_Validate_Abstract
{
	/**
	 * @var string Error: file type not within allowed types
	 */
	const ERROR_FALSE_TYPE = 'fileTypeFalse';

	/**
	 * @var string Error: file not found
	 */
	const ERROR_NOT_FOUND = 'fileTypeNotFound';

	/**
	 * @var array Error message templates
	 */
	protected $_messageTemplates = array(
		self::ERROR_FALSE_TYPE => "Wrong MIME type",
		self::ERROR_NOT_FOUND => "File unreadable or nonexistent",
	);

	/**
	 * File type (audio or video)
	 * @var string
	 */
	protected $_type = '';

	/**
	 * @var string Image file type (jpg or png)
	 */
	const TYPE_IMAGE = 0;

	/**
	 * @var string Audio file type
	 */
	const TYPE_AUDIO = 1;

	/**
	 * @var string Compressed archive
	 */
	const TYPE_ARCHIVE = 2;

	/**
	 * @var string Document file type
	 */
	const TYPE_DOCUMENT = 3;

	/**
	 * @var string Text file type
	 */
	const TYPE_TEXT = 4;

	/**
	 * Available file types, along with their MIME types
	 */
	private $_mimes = array(
		self::TYPE_IMAGE => array(
			'image/jpeg',
			'image/png'
		),
		self::TYPE_AUDIO => array(
			'audio/mpeg',
			'audio/x-mpeg',
			'audio/mp3',
			'audio/x-mp3',
			'audio/mpeg3',
			'audio/x-mpeg3',
			'audio/mpg',
			'audio/x-mpg',
			'audio/x-mpegaudio'
		),
		self::TYPE_ARCHIVE => array(
			'application/x-7z-compressed',
			'application/zip',
			'application/x-zip',
			'application/x-zip-compressed',
			'application/x-compress',
			'application/x-compressed',
			'multipart/x-zip',
			'application/rar',
			'application/x-rar-compressed',
			'application/x-gzip',
			'application/x-bzip2'
		),
		self::TYPE_DOCUMENT => array(
			'application/msword',
			'application/vnd.ms-excel',
			'application/vnd.ms-powerpoint',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'application/pdf',
			'application/x-pdf',
			'application/acrobat',
			'applications/vnd.pdf',
			'text/pdf',
			'text/x-pdf',
			'application/vnd.oasis.opendocument.text',
			'application/x-vnd.oasis.opendocument.text',
			'application/vnd.oasis.opendocument.spreadsheet',
			'application/x-vnd.oasis.opendocument.spreadsheet',
			'application/vnd.oasis.opendocument.presentation',
			'application/x-vnd.oasis.opendocument.presentation'
		),
		self::TYPE_TEXT => array(
			'text/plain',
			'text/anytext',
			'text/html',
			'text/css',
			'application/css-stylesheet',
			'text/javascript',
			'application/x-javascript',
			'text/x-c',
			'text/x-c++',
			'text/x-csrc',
			'text/x-chdr',
			'application/x-c'
		)
	);

	/**
	 * Set validator options
	 * @param  string|array|\Zend_Config $options
	 */
	public function __construct($options) {
		if ($options instanceof \Zend_Config) {
			$options = $options->toArray();
		}

		$types = array_keys($this->_mimes);

		if (is_array($options)) {
			if (array_key_exists('type',$options)) {
				if (is_array($options['type'])) {
					foreach ($options['type'] as $type) {
						if (in_array($type,$types)) {
							$this->_type[] = $type;
						} else {
							throw new \Zend_Validate_Exception('Unsupported file type: '.$type,500);
						}
					}
				} else {
					if (in_array($options['type'],$types)) {
						$this->_type[] = $options['type'];
					} else {
						throw new \Zend_Validate_Exception('Unsupported file type: '.$options['type'],500);
					}
				}
			} else {
				foreach ($options as $type) {
					if (in_array($type,$types)) {
						$this->_type[] = $type;
					} else {
						throw new \Zend_Validate_Exception('Unsupported file type: '.$type,500);
					}
				}
			}
		} else {
			throw new \Zend_Validate_Exception('Unsupported file type',500);
		}
	}

	/**
	 * Check whether the uploaded file is of a given type
	 *
	 * @param string|array $value the input file name or the array of values from $_FILES
	 * @return bool
	 */
	public function isValid($value) {
		// get the file name
		$filename = is_array($value) ? $value['tmp_name'] : $value;

		// is the file readable?
		if (!\Zend_Loader::isReadable($filename)) {
			$this->_error(self::ERROR_NOT_FOUND);
			return false;
		}

		if (is_array($value)) {
			$mime = $value['type'];
		} else {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mime = finfo_file($finfo, $value);
		}

		$type_correct = false;
		foreach ($this->_type as $type) {
			$type_correct |= in_array($mime,$this->_mimes[$type]);
		}

		if ($type_correct) return true;
		else {
			$this->_error(self::ERROR_FALSE_TYPE);
			return false;
		}
	}
}
