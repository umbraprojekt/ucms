<?php
namespace Mingos\uCMS\Validator;

use Mingos\uCMS\Model\AliasModel;

class AliasFreeValidator extends \Zend_Validate_Abstract {
	const OCCUPIED = "OCCUPIED";

	protected $_messageTemplates= array(
		self::OCCUPIED => "Your alias is already used.",
	);

	/**
	 * Check if an alias is already in use
	 * @param string $value
	 * @return bool
	 */
	public function isValid($value) {
		$model = new AliasModel();
		$request = \Zend_Controller_Front::getInstance()->getRequest();

		$exists = $model->exists($value, $request);
		if ($exists) {
			$this->_error(self::OCCUPIED);
			return false;
		}

		return true;
	}
}
