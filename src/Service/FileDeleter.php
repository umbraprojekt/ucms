<?php
namespace Mingos\uCMS\Service;

use Mingos\uCMS\Model\GalleryModel;
use Mingos\uCMS\Model\FileModel;
use Mingos\uCMS\Model\ImageModel;

class FileDeleter
{
	/**
	 * Completely delete a file along with all the files stored on disc
	 * @param integer $fileId
	 */
	public function deleteFile($fileId) {
		$fileModel = new FileModel();
		$imageModel = new ImageModel();

		$file = $fileModel->getById($fileId);
		$image = $imageModel->getByFileId($fileId);

		if ($image) {
			$this->deletePublicFiles($image);
			$imageModel->delete($imageModel->getAdapter()->quoteInto("id = ?", $image["id"]));

			// if the image was part of a gallery, update the gallery's image count
			if ($image["table"] == "node_gallery") {
				$modelGallery = new GalleryModel();
				$modelGallery->updateImageCount($image["nid"]);
			}
		}
		$this->deleteOriginalFile($file);
		$fileModel->delete($fileModel->getAdapter()->quoteInto("id = ?", $fileId));
	}

	/**
	 * Delete the original file from the uploads directory
	 * @param array $file File or Image
	 */
	public function deleteOriginalFile($file)
	{
		$path = array_key_exists("orig_path", $file) ? $file["orig_path"] : $file["path"];
		$filename = array_key_exists("orig_filename", $file) ? $file["orig_filename"] : $file["filename"];

		unlink(DOC_ROOT . "/upload/{$path}/{$filename}.{$file["extension"]}");
	}

	/**
	 * Delete files in the public uploads folder
	 * @param array $file
	 */
	public function deletePublicFiles($file)
	{
		$path = $file["path"];
		$filename = $file["filename"];
		$filenameLength = strlen($filename);

		$handle = opendir(PUBLIC_PATH . "/upload/{$path}");
		if ($handle) {
			while (false !== ($foundFileName = readdir($handle))) {
				if (substr($foundFileName, 0, $filenameLength) === $filename) {
					unlink(PUBLIC_PATH . "/upload/{$path}/{$foundFileName}");
				}
			}
			closedir($handle);
		}
	}
}
