<?php
namespace Mingos\uCMS\Service;

class Cache
{
	/**
	 * @var \Zend_Cache_Core
	 */
	private $cache;

	public function __construct()
	{
		$this->cache = \Zend_Registry::get("Cache");
	}

	public function clear()
	{
		$this->cache->clean();
	}

	public function get($key)
	{
		return $this->cache->load($key);
	}

	public function put($key, $value)
	{
		return $this->cache->save($value, $key);
	}
}
