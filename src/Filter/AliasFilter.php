<?php
namespace Mingos\uCMS\Filter;

use Mingos\uCMS\Model\AliasModel;

class AliasFilter implements \Zend_Filter_Interface {
	/**
	 * Filter an alias to make sure it is unique
	 * @param  string $string
	 * @return string
	 */
	public function filter($string) {
		$model = new AliasModel();
		$request = \Zend_Controller_Front::getInstance()->getRequest();

		$cur_value = $string;
		$cur_iter = 0;

		while ($model->exists($cur_value, $request)) {
			++$cur_iter;
			$cur_value = "{$string}-{$cur_iter}";
		}

		return $cur_value;
	}
}
