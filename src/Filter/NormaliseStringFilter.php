<?php
namespace Mingos\uCMS\Filter;

class NormaliseStringFilter implements \Zend_Filter_Interface
{
	/**
	 * Normalise a string
	 * @param  string $string
	 * @return string
	 */
	public function filter($string)
	{
		$cache = \Zend_Registry::get('Cache');
		// replace characters
		if (!$translations = $cache->load('i18n')) {
			$translations = parse_ini_file(APPLICATION_PATH . '/configs/i18n-ascii.ini');
			$cache->save($translations, 'i18n');
		}
		$output = strtr($string, $translations);
		// replace whitespace
		$output = preg_replace('/\s+/', '-', $output);
		// change uppercase to lowercase
		$output = strtolower($output);
		// remove punctuation
		$output = preg_replace('/\/+/', '/', $output);
		$output = preg_replace('/_/', '-', $output);
		$output = preg_replace('/[^a-z0-9\-\/]/', '', $output);
		// trim dashes
		$output = trim($output, '/-');
		// return the normalised string
		return $output;
	}
}
