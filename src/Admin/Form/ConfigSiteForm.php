<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Edit preferences for the entire site
 */
class ConfigSiteForm extends AbstractForm
{
	public function init()
	{
		$created = new \Zend_Form_Element_Checkbox('created');
		$created
			->setLabel('Enable creation date editing')
		;

		$title = new \Zend_Form_Element_Checkbox('seo_title');
		$title
			->setLabel('Enable <title> tag editing')
		;

		$priority = new \Zend_Form_Element_Checkbox('seo_priority');
		$priority
			->setLabel('Enable node priority')
		;

		$changefreq = new \Zend_Form_Element_Checkbox('seo_changefreq');
		$changefreq
			->setLabel('Enable node change frequency')
		;

		$search = new \Zend_Form_Element_Checkbox('search');
		$search
			->setLabel('Enable inclusion in search results')
		;

		$sitemap = new \Zend_Form_Element_Checkbox('sitemap');
		$sitemap
			->setLabel('Enable node inclusion in sitemap')
		;

		$this->addElements(array($created,$title,$priority,$changefreq,$search,$sitemap));

		$this->addSaveButton();
	}
}
