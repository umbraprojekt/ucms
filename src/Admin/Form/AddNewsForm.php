<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\ConfigModel;

/**
 * Add news post form
 */
class AddNewsForm extends AbstractForm
{
	/**
	 * Site configuration
	 * @var ConfigModel
	 */
	private $_config;

	public function __construct()
	{
		$this->_config = new ConfigModel();
		parent::__construct();
	}

	public function init()
	{
		$this->addContentTitleInput();
		$this->addTitleInput();
		$this->addImageInput();
		$this->addInputFormatSelect();
		$this->addTeaserInput();
		$this->addBodyInput();
		$this->addAliasInput();

		if ($this->_config->getParam('created', 'site') == '1' && $this->_config->getParam('created', 'news') == '1') {
			$this->addCreationDateInput();
		}

		$this->addPublishedSwitch()->setChecked(false);
		$this->addAutopublishInput();
		$this->addAutounpublishInput();

		if ($this->_config->getParam('search','site') == '1' && $this->_config->getParam('search','page') == '1') {
			$this->addIncludeInSearchSwitch()->setChecked(true);
		}

		if ($this->_config->getParam('seo_title','site') == '1' && $this->_config->getParam('seo_title','news') == '1') {
			$this->addSEOTitleInput();
		}

		$this->addSEOKeywordsInput();
		$this->addSEODescriptionInput();

		if ($this->_config->getParam('seo_priority','site') == '1') {
			$this->addSEOPrioritySelect()->setValue("0.5");
		}

		if ($this->_config->getParam('seo_changefreq','site') == '1') {
			$this->addSEOChangeFrequencySelect()->setValue("yearly");
		}

		if ($this->_config->getParam('robots','news') == '1') {
			$this->addSEORobotsSelect();
		}

		if ($this->_config->getParam('sitemap', 'site') == '1' && $this->_config->getParam('sitemap', 'news') == '1') {
			$this->addIncludeInXMLSitemapChoice();
		}

		$this->addSaveButton();
		$this->addSaveAndCountinueButton();
		$this->addCancelButton();
	}
}
