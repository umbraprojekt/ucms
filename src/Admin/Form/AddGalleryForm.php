<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Filter\NormaliseStringFilter;
use Mingos\uCMS\Model\ConfigModel;

/**
 * Add gallery form
 */
class AddGalleryForm extends AbstractForm
{
	/**
	 * Site configuration
	 * @var ConfigModel
	 */
	private $_config;

	public function __construct()
	{
		$this->_config = new ConfigModel();
		parent::__construct();
	}

	public function init()
	{
		$view = $this->getView();

		$this->addContentTitleInput();
		$this->addTitleInput();

		// path to the images
		$path = new \Zend_Form_Element_Text('path');
		$path
			->setLabel('Path to the images')
			->setDescription('Path to the images contained in the gallery, without leading or trailing slashes, e.g. "gallery/segways".')
			->addValidator('StringLength',true,array('min' => 1, 'max' => 120, 'encoding' => 'utf-8', 'messages' => array('stringLengthTooShort' => $view->translate('Minimum of %1$s characters.',1), 'stringLengthTooLong' => $view->translate('Maximum of %1$s characters.',120))))
			->addValidator('Regex',true,array('pattern' => '/^[a-z0-9\/\-]*$/', 'messages' => array('regexNotMatch' => $view->translate('The field contains forbidden characters.'))))
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim(),
				new NormaliseStringFilter()
			))
		;
		$this->addElement($path);
		$this->addToDisplayGroup($path, "group_content");


		$this->addSaveButton();
		$this->addSaveAndCountinueButton();
		$this->addCancelButton();
	}
}
