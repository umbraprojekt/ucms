<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\AclModel;
use Mingos\uCMS\Model\UserModel;

class AddUserForm extends AbstractForm
{
	private $_uid;
	private $_user;
	private $_acl;

	private $_canEditData;
	private $_canEditRole;

	public function __construct($uid = 0)
	{
		$this->_uid = $uid;
		$this->_user = \Zend_Auth::getInstance()->getIdentity();
		$this->_acl = AclModel::getInstance();

		if ($uid > 0) {
			$userModel = new UserModel();
			$edited = $userModel->getById($this->_uid);

			$this->_canEditData = $this->_acl->inheritsRole($this->_user->role, $edited['role']) || $this->_user->role == $edited['role'] || $this->_uid == $this->_user->id;
			$this->_canEditRole = $this->_acl->inheritsRole($this->_user->role, $edited['role']) && in_array($this->_user->role,array('admin','god'));
		} else {
			$this->_canEditData = true;
			$this->_canEditRole = true;
		}
		parent::__construct();
	}

	public function init()
	{
		$login = new \Zend_Form_Element_Text('login');
		$login
			->setLabel('User name')
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(3, 32), true)
			->addValidator('Db_NoRecordExists', true, array(
				'table' => 'user',
				'field' => 'login',
				'exclude' => "id <> {$this->_uid}",
				'messages' => array(
					'recordFound' => $this->getView()->translate('This name is already used by another user.')
				)
			))
		;

		$password = new \Zend_Form_Element_Password('password');
		$password
			->setLabel('Password')
			->addValidator('NotEmpty', true, array(
				'messages' => array(
					'isEmpty' => $this->getView()->translate('This field cannot be empty.')
				)
			))
		;

		$email = new \Zend_Form_Element_Text('email');
		$email
			->setLabel('E-mail address')
			->setRequired()
			->addValidator('NotEmpty', true, array(
				'messages' => array(
					'isEmpty' => $this->getView()->translate('This field cannot be empty.')
				)
			))
			->addValidator('Db_NoRecordExists', true, array(
				'table' => 'user',
				'field' => 'email',
				'exclude' => "id <> {$this->_uid}",
				'messages' => array(
					'recordFound' => $this->getView()->translate('This e-mail address is already used by another user.')
				)
			))
			->addValidator('Regex', true, array(
				'pattern' => "/^[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$/",
				'messages' => array(
					'regexNotMatch' => $this->getView()->translate('This e-mail address is invalid.')
				)
			))
		;

		$role = new \Zend_Form_Element_Select('role');
		$role->setLabel('Role');
		foreach (array('user','admin','god') as $r) {
			if ($this->_acl->inheritsRole($this->_user->role,$r)) $role->addMultiOption($r,$r);
		}

		$notify = new \Zend_Form_Element_Checkbox('notify');
		$notify
			->setLabel('Send e-mail notification?')
		;

		if ($this->_canEditData) $this->addElements(array($login,$password,$email));
		if ($this->_canEditRole) $this->addElements(array($role));

		$this->addElement($notify);

		$this->addSaveButton();
		$this->addCancelButton();
	}
}
