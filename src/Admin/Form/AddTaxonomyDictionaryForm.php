<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Filter\NormaliseStringFilter;

/**
 * Add/edit dictionary form
 */
class AddTaxonomyDictionaryForm extends AbstractForm
{
	/**
	 * Taxonomy dictionary ID
	 * @var int
	 */
	private $_id;

	/**
	 * Database adapter (for quoting)
	 * @var \Zend_Db_Adapter_Abstract
	 */
	private $_db = null;

	/**
	 * @param int $id
	 */
	public function __construct($id = 0)
	{
		$this->_id = intval($id);
		$this->_db = \Zend_Db_Table_Abstract::getDefaultAdapter();
		parent::__construct();
	}

	public function init()
	{
		$nameLang = new \Zend_Form_Element_Text('title');
		$nameLang
			->setLabel('Name')
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(2, 64), true)
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($nameLang);

		$slug = new \Zend_Form_Element_Text('slug');
		$slug
			->setLabel('Path alias')
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(2, 64), true)
			->addValidator('Regex',true,array('pattern' => '/^[a-z0-9\/\-]*$/', 'messages' => array('regexNotMatch' => $this->getView()->translate('The field contains forbidden characters.'))))
			->addValidator('Db_NoRecordExists', true, array(
				"table" => "taxonomy_dictionary",
				"field" => "slug",
				"exclude" => "id <> " . $this->_db->quote($this->_id),
				"messages" => array(
					\Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => $this->getView()->translate("This alias already exists.")
				)
			))
			->addFilters(array(
				new NormaliseStringFilter(),
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($slug);

		$this->addSaveButton();
		$this->addCancelButton();
	}
}
