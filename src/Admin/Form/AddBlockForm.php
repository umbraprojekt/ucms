<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\ConfigModel;

/**
 * Add block form
 */
class AddBlockForm extends AbstractForm
{
	/**
	 * Site configuration
	 * @var ConfigModel
	 */
	private $_config;

	public function __construct()
	{
		$this->_config = new ConfigModel();
		parent::__construct();
	}

	public function init()
	{
		$elements = array();
		$names = array();

		$this->addContentTitleInput();
		$this->addTitleInput();
		$this->addImageInput();
		$this->addInputFormatSelect();
		$this->addBodyInput();

		// view script
		if ($this->_config->getParam("view_script", "site") == "1" && $this->_config->getParam("view_script", "block") == "1") {
			$scripts = array();
			if ($handle = opendir(DOC_ROOT."/views/index")) {
				while (false !== ($file = readdir($handle))) {
					$script = str_replace('.phtml','',$file);
					if ($file != $script && !empty($file)) {
						$script = substr($file,0,-6);
						$scripts[$script] = $file;
					}
				}
				closedir($handle);
				ksort($scripts);
			}

			$view_script = new \Zend_Form_Element_Select('view_script');
			$view_script
				->setLabel('View script file name')
				->addFilters(array(
					new \Zend_Filter_StripTags(),
					new \Zend_Filter_StripNewlines(),
					new \Zend_Filter_StringTrim()
				))
				->addMultiOptions($scripts)
				->setValue('page')
			;
			$elements['content'][] = $view_script;
			$names['content'][] = 'view_script';
		}

		$this->addSaveButton();
	}
}
