<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\PageModel;
use Mingos\uCMS\Model\ProductModel;
use Mingos\uCMS\Model\ViewModel;

/**
 * Add new menu item form
 */
class AddMenuItemForm extends AbstractForm
{
	private $_nodes = array();

	public function __construct()
	{
		$nodes = array('0' => $this->getView()->translate('Specify URL'));

		$model = new PageModel();
		foreach ($model->getAll(1) as $node) {
			$nodes[$this->getView()->translate('Simple pages')][$node['id']] = $node['content_title'];
		}

		$model = new ProductModel();
		foreach ($model->getAll(1) as $node) {
			$nodes[$this->getView()->translate('Products')][$node['id']] = $node['content_title'];
		}

		$model = new ViewModel();
		foreach ($model->getAll(1) as $node) {
			$nodes[$this->getView()->translate('Views')][$node['id']] = $node['content_title'];
		}

		$this->_nodes = $nodes;

		parent::__construct();
	}

	public function init()
	{
		$view = $this->getView();

		// menu item label
		$label = new \Zend_Form_Element_Text('label');
		$label
			->setLabel('Label')
			->setDescription('The label of the menu item. If left blank, uCMS will fill it automatically with either the selected content title or the specified URL.')
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StringTrim()
			))
		;

		// content
		$content = new \Zend_Form_Element_Select('cid');
		$content
			->setLabel('Content')
			->setDescription('Choose the content you wish to appear in the menu or select "Specify URL" to manually add an URL to the menu.')
			->addMultiOptions($this->_nodes)
		;

		// href
		$href = new \Zend_Form_Element_Text('href');
		$desc =
			$view->translate('URL that you wish your menu item to link to.').'<br />'.
			$view->translate('Allowed formats:').'<br />'.
				'* '.$view->translate('internal link: relative path with trailing slash, e.g. <em>/segways.html</em>').'<br />'.
				'* '.$view->translate('external link: full path with protocol name, e.g. <em>http://www.google.com</em>')
		;
		$href
			->setLabel('URL')
			->setDescription($desc)
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator('Regex', true, array('pattern' => '/^(https?:\/\/|\/)/', 'messages' => array('regexNotMatch' => $view->translate('Invalid URL.'))))
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StringTrim()
			))
			->getDecorator('Description')->setOption('escape', false)
		;

		// menu item class
		$class = new \Zend_Form_Element_Text("class");
		$class
			->setLabel("Class")
			->setDescription("HTML attribute added to the menu item. Used for customising the look of the menu item. If you are unsure what this does, leave this field empty.")
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StringTrim()
			))
		;

		// menu item title
		$title = new \Zend_Form_Element_Text("title");
		$title
			->setLabel("Title")
			->setDescription("Menu item's title attribute.")
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StringTrim()
			))
		;

		// add elements
		$this->addElements(array($content, $label, $href, $class, $title));

		$this->addSaveButton();
		$this->addCancelButton();

		// disable & blank the href field if there's any content selected
		$view->inlineScript()->appendScript("
			$(document).ready(function() {
				var
					content = $('#cid')
					href = $('#href')
				;
				if (content.val() != 0) {
					href.attr('disabled', 'disabled');
				}
				content.change(function() {
					if ($(this).val() != 0) {
						href.get(0).defaultValue = href.val();
						href
							.val('')
							.attr('disabled', 'disabled')
						;
					} else {
						href
							.val(href.get(0).defaultValue)
							.removeAttr('disabled')
						;
					}
				});
			});
		");
	}
}
