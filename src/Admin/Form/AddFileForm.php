<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * File upload form
 */
class AddFileForm extends AbstractForm
{
	private $_path;

	/**
	 * Constructor for the form
	 * @param string $path File upload path
	 */
	public function __construct($path = "files") {

		$this->_path = trim($path,'/');
		if (!is_dir(DOC_ROOT."/upload/{$this->_path}")) {
			mkdir(DOC_ROOT."/upload/{$this->_path}",octdec('0777'),true);
		}
		parent::__construct();
	}

	public function init() {
		$file = new \Zend_Form_Element_File('file');
		$file
			->setLabel('Upload file')
			->setDestination(DOC_ROOT."/upload/{$this->_path}")
		;
		$this->addElement($file);

		$this->addSaveButton()
			->setLabel('Upload')
			->setAttrib('class', 'button-upload');

	}
}
