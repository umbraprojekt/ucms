<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\ConfigModel;

/**
 * Edit preferences for a node
 */
abstract class AbstractConfigNodeForm extends AbstractForm
{
	/**
	 * Description for the alias field
	 * @var string
	 */
	protected $_desc;

	/**
	 * Site configuration
	 * @var ConfigModel
	 */
	protected $_config;

	public function __construct()
	{
		$this->_desc = $this->getView()->translate('You may use placeholder values from the list below. They will be substituted for appropriate values automatically.');
		$date = new \DateTime();
		$this->_desc .= '<br />'.$this->getView()->translate('For instance, <em>blog/%%year%%/%%month%%/%%title%%</em> would result in <em>blog/%1$s/example-title</em>.',$date->format('Y/m'));
		$this->_desc .= '<br />'.$this->getView()->translate('Available placeholders').':<br />';
		$this->_desc .= '- <span class="alias-placeholder">%title%</span> - '.$this->getView()->translate('The content\'s normalised title').'<br />';
		$this->_desc .= '- <span class="alias-placeholder">%year%</span> - '.$this->getView()->translate('Posting year (YYYY)').'<br />';
		$this->_desc .= '- <span class="alias-placeholder">%month%</span> - '.$this->getView()->translate('Posting month (MM)').'<br />';
		$this->_desc .= '- <span class="alias-placeholder">%day%</span> - '.$this->getView()->translate('Posting day (DD)').'<br />';
		$this->_desc .= '- <span class="alias-placeholder">%userid%</span> - '.$this->getView()->translate('Content author\'s user ID').'<br />';
		$this->_desc .= '- <span class="alias-placeholder">%username%</span> - '.$this->getView()->translate('Content author\'s user name');

		$this->_config = new ConfigModel('site');

		parent::__construct();
	}

	public function init() {
		$alias = new \Zend_Form_Element_Text('alias');
		$alias
			->setLabel('Default alias')
			->setDescription($this->_desc)
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->getDecorator('Description')->setOption('escape', false)
		;
		$this->addElement($alias);
		$this->getView()->inlineScript()->appendScript(
			"jQuery(document).ready(function() {
				jQuery('.alias-placeholder').on({
					click: function() {
						jQuery('#alias').val(jQuery('#alias').val() + jQuery(this).html());
					}
				});
			});
		");

		if ($this->_config->getParam('created') == '1') {
			$created = new \Zend_Form_Element_Checkbox('created');
			$created
				->setLabel('Enable creation date editing')
			;
			$this->addElement($created);
		}

		if ($this->_config->getParam('seo_title') == '1') {
			$seotitle = new \Zend_Form_Element_Checkbox('seo_title');
			$seotitle
				->setLabel('Enable <title> tag editing')
			;
			$this->addElement($seotitle);
		}

		if ($this->_config->getParam('seo_priority') == '1') {
			$priority = new \Zend_Form_Element_Select('seo_priority');
			$priority
				->setLabel('Default priority')
				->setDescription('The priority setting is used in the XML sitemap to inform the search engine about the importance of a page within a website.')
				->addMultiOptions(array(
					'1.0' => '1.0',
					'0.9' => '0.9',
					'0.8' => '0.8',
					'0.7' => '0.7',
					'0.6' => '0.6',
					'0.5' => '0.5',
					'0.4' => '0.4',
					'0.3' => '0.3',
					'0.2' => '0.2',
					'0.1' => '0.1',
					'0.0' => '0.0'
				))
				->setRequired()
				->addValidator($this->getNotEmptyValidator(), true)
			;
			$this->addElement($priority);
		}

		if ($this->_config->getParam('seo_changefreq') == '1') {
			$changefreq = new \Zend_Form_Element_Select('seo_changefreq');
			$changefreq
				->setLabel('Default change frequency')
				->setDescription('How often the content is going to be updated. This setting is used by the XML sitemap.')
				->addMultiOptions(array(
					'always' => $this->getView()->translate('always'),
					'hourly' => $this->getView()->translate('hourly'),
					'daily' => $this->getView()->translate('daily'),
					'weekly' => $this->getView()->translate('weekly'),
					'monthly' => $this->getView()->translate('monthly'),
					'yearly' => $this->getView()->translate('yearly'),
					'never' => $this->getView()->translate('never')
				))
				->setRequired()
				->addValidator($this->getNotEmptyValidator(), true)
			;
			$this->addElement($changefreq);
		}

		$robots = new \Zend_Form_Element_Checkbox('robots');
		$robots
			->setLabel('Allow per-node robots overriding')
		;
		$this->addElement($robots);

		if ($this->_config->getParam('search') == '1') {
			$search = new \Zend_Form_Element_Checkbox('search');
			$search
				->setLabel('Enable inclusion in search results')
			;
			$this->addElement($search);
		}

		if ($this->_config->getParam('sitemap') == '1') {
			$sitemap = new \Zend_Form_Element_Checkbox('sitemap');
			$sitemap
				->setLabel('Enable inclusion in sitemap')
			;
			$this->addElement($sitemap);
		}

		$this->addSaveButton();
		$this->addCancelButton();
	}
}
