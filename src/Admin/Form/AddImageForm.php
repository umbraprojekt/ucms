<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Image upload form
 */
class AddImageForm extends AbstractForm
{
	private $_path;
	private $_table;
	private $_id;

	/**
	 * Constructor for the form
	 * @param array $params Parametres:<br>
	 *                      string [path] => the path, relative to DOC_ROOT/upload directory, where the images will be uploaded (mandatory, no loading or trailing slashes)<br>
	 *                      string [table] => the table name to be stored in the image table record<br>
	 *                      int [id] => the node ID to be stored in the image table record
	 */
	public function __construct($params = array())
	{
		$defaults = array(
			'path' => '',
			'table' => '',
			'id' => 0
		);

		$params += $defaults;

		$this->_path = trim($params['path'],'/');
		if (!is_dir(DOC_ROOT."/upload/{$params['path']}")) {
			mkdir(DOC_ROOT."/upload/{$params['path']}",octdec('0777'),true);
		}
		$this->_table = $params['table'];
		$this->_id = $params['id'];
		parent::__construct();
	}

	public function init()
	{
		$image = new \Zend_Form_Element_File('image');
		$image
			->setLabel('Upload images')
			->setDestination(DOC_ROOT."/upload/{$this->_path}")
			->setAttrib('multiple', 'multiple')
			->setIsArray(true)
		;

		$save = new \Zend_Form_Element_Submit('upload');
		$save
			->setLabel('Upload')
			->setAttrib('class', 'button-upload')
			->setDecorators(array('ViewHelper'))
		;

		$this->addElements(array($image, $save));
	}
}
