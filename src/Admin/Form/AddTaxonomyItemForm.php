<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Filter\NormaliseStringFilter;

/**
 * Add/edit taxonomy term form
 */
class AddTaxonomyItemForm extends AbstractForm
{
	/**
	 * Taxonomy term ID
	 * @var int
	 */
	private $_id;

	/**
	 * Database adapter (for quoting)
	 * @var \Zend_Db_Adapter_Abstract
	 */
	private $_db = null;

	/**
	 * @param int $tid
	 */
	public function __construct($tid = 0)
	{
		$this->_id = intval($tid);
		$this->_db = \Zend_Db_Table_Abstract::getDefaultAdapter();
		parent::__construct();
	}

	public function init()
	{
		$name = new \Zend_Form_Element_Text('title');
		$name
			->setLabel('Name')
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(2, 64), true)
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->getView()->inlineScript()->appendScript(
			"$(document).ready(function() {
				$('#title').focus();
			});"
		);
		$this->addElement($name);

		$slug = new \Zend_Form_Element_Text('slug');
		$slug
			->setLabel('Path alias')
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(2, 64), true)
			->addValidator('Regex',true,array('pattern' => '/^[a-z0-9\/\-]*$/', 'messages' => array('regexNotMatch' => $this->getView()->translate('The field contains forbidden characters.'))))
			->addValidator('Db_NoRecordExists', true, array(
				"table" => "taxonomy",
				"field" => "slug",
				"exclude" => "id <> " . $this->_db->quote($this->_id),
				"messages" => array(
					\Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => $this->getView()->translate("This alias already exists.")
				)
			))
			->addFilters(array(
				new NormaliseStringFilter(),
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($slug);

		$this->addSaveButton();
		$this->addCancelButton();
	}
}
