<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Edit preferences for a node
 */
class ConfigNewsForm extends AbstractConfigNodeForm
{
	public function init()
	{
		parent::init();
		if ($this->getElement('alias'))
			$this->getElement('alias')->setValue($this->getView()->translate('news').'/%year%/%month%/%title%');
		if ($this->getElement('seo_priority'))
			$this->getElement('seo_priority')->setValue('0.5');
		if ($this->getElement('seo_changefreq'))
			$this->getElement('seo_changefreq')->setValue('yearly');
		if ($this->getElement('robots'))
			$this->getElement('robots')->setChecked(false);
		if ($this->getElement('sitemap'))
			$this->getElement('sitemap')->setChecked(false);
		if ($this->getElement('search'))
			$this->getElement('search')->setChecked(false);
	}
}
