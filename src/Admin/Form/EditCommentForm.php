<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Edit comment form
 */
class EditCommentForm extends AbstractForm
{
	public function init()
	{
		$view = $this->getView();

		// comment body
		$body = new \Zend_Form_Element_Textarea('body');
		$body
			->setLabel('Body')
			->addValidators(array(
				array('NotEmpty',true,array('messages' => array('isEmpty' => $view->translate('This field cannot be empty.')))),
			))
			->addFilter('StripTags')
			->setRequired()
		;
		$this->addElement($body);

		$this->addSaveButton();
		$this->addCancelButton();
	}
}
