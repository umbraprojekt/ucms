<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\ConfigModel;

/**
 * Add view form
 */
class AddViewForm extends AbstractForm
{
	/**
	 * Site configuration
	 * @var ConfigModel
	 */
	private $_config;

	public function __construct()
	{
		$this->_config = new ConfigModel();
		parent::__construct();
	}

	public function init()
	{
		$this->addContentTitleInput();
		$this->addTitleInput();
		$this->addInputFormatSelect();
		$this->addTeaserInput();
		$this->addBodyInput();

		// mvc module
		$module = new \Zend_Form_Element_Text('form_module');
		$module
			->setLabel('Module')
			->addValidator($this->getNotEmptyValidator(), true)
			->setRequired()
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($module);
		$this->addToDisplayGroup($module, "group_content");

		// mvc controller
		$controller = new \Zend_Form_Element_Text('form_controller');
		$controller
			->setLabel('Controller')
			->addValidator($this->getNotEmptyValidator(), true)
			->setRequired()
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($controller);
		$this->addToDisplayGroup($controller, "group_content");

		// mvc action
		$action = new \Zend_Form_Element_Text('form_action');
		$action
			->setLabel('Action')
			->addValidator($this->getNotEmptyValidator(), true)
			->setRequired()
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($action);
		$this->addToDisplayGroup($action, "group_content");

		$this->addAliasInput();
		$this->addPublishedSwitch()->setChecked(true);

		if ($this->_config->getParam('search', 'site') == '1' && $this->_config->getParam('search', 'page') == '1') {
			$this->addIncludeInSearchSwitch();
		}

		if ($this->_config->getParam('seo_title', 'site') == '1' && $this->_config->getParam('seo_title', 'view') == '1') {
			$this->addSEOTitleInput();
		}

		$this->addSEOKeywordsInput();
		$this->addSEODescriptionInput();

		if ($this->_config->getParam('seo_priority', 'site') == '1') {
			$this->addSEOPrioritySelect();
		}

		if ($this->_config->getParam('seo_changefreq', 'site') == '1') {
			$this->addSEOChangeFrequencySelect();
		}

		if ($this->_config->getParam('robots', 'view') == '1') {
			$this->addSEORobotsSelect();
		}

		if ($this->_config->getParam('sitemap', 'site') == '1' && $this->_config->getParam('sitemap', 'view') == '1') {
			$this->addIncludeInXMLSitemapChoice();
		}

		$this->addSaveButton();
		$this->addSaveAndCountinueButton();
		$this->addCancelButton();
	}
}
