<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Filter\NormaliseStringFilter;
use Mingos\uCMS\Validator\AliasFreeValidator;
use Mingos\uCMS\Model\ConfigModel;

class AbstractForm extends \Zend_Form
{
	protected function getNotEmptyValidator()
	{
		$validator = new \Zend_Validate_NotEmpty();
		$validator->setMessages(array(
			\Zend_Validate_NotEmpty::IS_EMPTY => $this->getView()->translate("This field cannot be empty.")
		));
		return $validator;
	}

	protected function getStringLengthValidator($min, $max)
	{
		$validator = new \Zend_Validate_StringLength(array(
			"min" => $min,
			"max" => $max,
			"encoding" => "utf-8"
		));
		$validator->setMessages(array(
			\Zend_Validate_StringLength::TOO_SHORT => $this->getView()->translate('Minimum of %1$s characters.', $min),
			\Zend_Validate_StringLength::TOO_LONG => $this->getView()->translate('Maximum of %1$s characters.', $max)
		));
		return $validator;
	}

	protected function addToDisplayGroup(\Zend_Form_Element $element, $groupName)
	{
		$group = $this->getDisplayGroup($groupName);
		if (!$group) {
			$this->createDisplayGroup($element, $groupName);
		} else {
			$group->addElement($element);
		}
	}

	private function createDisplayGroup(\Zend_Form_Element $element, $groupName)
	{
		$groups = array(
			"group_content" => $this->getView()->translate("Content"),
			"group_seo" => $this->getView()->translate("SEO")
		);

		$this->addDisplayGroup(array($element), $groupName);
		$this->getDisplayGroup($groupName)->setLegend($groups[$groupName]);

		return $this->getDisplayGroup($groupName);
	}

	protected function addContentTitleInput()
	{
		$min = 3;
		$max = 120;

		$element = new \Zend_Form_Element_Text("content_title");
		$element
			->setLabel("Admin title")
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator($min, $max), true)
			->setRequired()
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
			->setDescription("The admin title is displayed in the admin panel.")
		;

		$this->getView()->inlineScript()->appendScript(
			"$(document).ready(function() {
				$('#content_title').counter({ min: {$min}, max: {$max} });
				$('#content_title').focus();
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addTitleInput()
	{
		$min = 3;
		$max = 120;

		$element = new \Zend_Form_Element_Text("title");
		$element
			->setLabel("Title")
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator($min, $max), true)
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
			->setDescription("The title gets localised in case of a miltilingual site. This is what gets displayed to the end user.")
		;

		$this->getView()->inlineScript()->appendScript(
			"jQuery(document).ready(function() {
				$('#title').counter({ min: {$min}, max: {$max} });
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addImageInput()
	{
		if (!is_dir(DOC_ROOT . "/tmp")) {
			mkdir(DOC_ROOT . "/tmp", octdec("0777"), true);
		}
		$element = new \Zend_Form_Element_File("image");
		$element
			->setLabel("Image")
			->setDescription("Upload an image. If an image has already been uploaded, it will be overwritten.")
			->addValidator(
				"Extension",
				true,
				array(
					"extension" => "jpg,png",
					"messages" => array(
						\Zend_Validate_File_Extension::FALSE_EXTENSION => $this->getView()->translate("Only jpg and png images are supported.")
					)
				)
			)
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addInputFormatSelect()
	{
		$config = \Zend_Registry::get("Config");

		$element = new \Zend_Form_Element_Select("input_format");
		$element
			->setLabel("Input format")
			->addMultiOptions(array(
				"html" => "HTML",
				"md" => "Markdown"
			))
			->setValue($config["site"]["defaultInputFormat"])
			->setAttrib("class", "chosen-disable");

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addTeaserInput()
	{
		$element = new \Zend_Form_Element_Textarea("teaser");
		$element->setLabel("Teaser");

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addBodyInput()
	{
		$element = new \Zend_Form_Element_Textarea("body");
		$element->setLabel("Body");

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addAliasInput()
	{
		$min = 3;
		$max = 120;

		$element = new \Zend_Form_Element_Text("alias");
		$element
			->setLabel("Path alias")
			->setDescription('Path to your content, without leading or trailing slashes, e.g. "about-us" or "services/accountancy".<br />If you leave this field empty, uCMS will generate a default alias.<br />Allowed characters: lowercase a-z with no diacritics, digits 0-9, forward slash "/", dash "-".')
			->addValidator($this->getStringLengthValidator($min, $max), true)
			->addValidator(
				"Regex",
				true,
				array(
					"pattern" => '/^[a-z0-9\/\-]*$/',
					"messages" => array(
						"regexNotMatch" => $this->getView()->translate("The field contains forbidden characters.")
					)
				)
			)
			->addValidator(new AliasFreeValidator(), true)
			->addFilters(array(
				new NormaliseStringFilter(),
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
			->getDecorator("Description")->setOption("escape", false)
		;

		$this->getView()->inlineScript()->appendScript(
			"$(document).ready(function() {
				$('#alias').counter({ min: {$min}, max: {$max}, allowEmpty: true });
			});"
		);


		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addCreationDateInput()
	{
		$element = new \Zend_Form_Element_Text("created");
		$element
			->setLabel("Set the creation time")
			->setDescription("Content creation date and time. If empty, the current time will be used.")
			->addValidator(
				"Regex",
				true,
				array(
					"pattern" => '/^\d{4}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}$/',
					"messages" => array(
						\Zend_Validate_Regex::NOT_MATCH => $this->getView()->translate("The date must be in YYYY-MM-DD HH:MM:SS format.")
					)
				)
			)
		;
		$this->getView()->inlineScript()->appendScript(
			"$(document).ready(function() {
				$('#created').datepicker({
					changeMonth: true,
					changeYear: true,
					showAnim: 'slideDown',
					dateFormat: 'yy-mm-dd 00:00:00'
				});
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addPublishedSwitch()
	{
		$element = new \Zend_Form_Element_Checkbox("published");
		$element
			->setLabel("Publish content")
			->setDescription("Published content is accessible by the website visitors.")
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addAutopublishInput()
	{
		$element = new \Zend_Form_Element_Text("autopublish");
		$element->setLabel("Automatically publish");
		$this->getView()->inlineScript()->appendScript(
			"$(document).ready(function() {
				$('#autopublish').datetimepicker({
					changeMonth: true,
					changeYear: true,
					showAnim: 'slideDown',
					dateFormat: 'yy-mm-dd',
					timeFormat: 'HH:mm:ss'
				});
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addAutounpublishInput()
	{
		$element = new \Zend_Form_Element_Text("autounpublish");
		$element
			->setLabel("Automatically unpublish")
			->setDescription("Content can be published and unpublished automatically at a selected time, making it available or unavailable to visitors without the need to manually publish or unpublish it.");
		$this->getView()->inlineScript()->appendScript(
			"$(document).ready(function() {
				$('#autounpublish').datetimepicker({
					changeMonth: true,
					changeYear: true,
					showAnim: 'slideDown',
					dateFormat: 'yy-mm-dd',
					timeFormat: 'HH:mm:ss'
				});
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addIncludeInSearchSwitch()
	{
		$element = new \Zend_Form_Element_Checkbox("search");
		$element
			->setLabel("Include in search results")
			->setDescription("Content shown in search results will be accessible via site-wide search form.")
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_content");

		return $element;
	}

	protected function addSEOTitleInput()
	{
		$min = 3;
		$max = 120;

		$element = new \Zend_Form_Element_Text('seo_title');
		$element
			->setLabel("Title")
			->setDescription("The HTML <title> tag. If left empty, the content's title will be used.")
			->addValidator($this->getStringLengthValidator($min, $max), true)
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->getView()->inlineScript()->appendScript(
			"jQuery(document).ready(function() {
				jQuery('#seo_title').counter({ min: {$min}, max: {$max}, allowEmpty: true });
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addSEOKeywordsInput()
	{
		$keywordsDescription = $this->getView()->translate("A comma-separated list of keywords, e.g. <em>shop, clothing, trousers, jeans, denim, jackets</em>. Keywords from this field will be added to the global ones. <strong>Remember not to repeat them!</strong><br /><br />Current global keywords:") . "<br />";
		$seoModel = new ConfigModel("seo");
		$globals = $seoModel->getParams();
		if (!empty($globals["keywords"])) $keywordsDescription .= "<em><strong>{$globals["keywords"]}</strong></em>";
		else $keywordsDescription .= $this->getView()->translate("none");

		$element = new \Zend_Form_Element_Text("seo_keywords");
		$element
			->setLabel("Keywords")
			->setDescription($keywordsDescription)
			->addFilter("StringToLower", array("encoding" => "utf-8"))
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
			->getDecorator("Description")->setOption("escape", false)
		;
		$this->getView()->inlineScript()->appendScript(
			"jQuery(document).ready(function() {
				jQuery('#seo_keywords').counter();
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addSEODescriptionInput()
	{
		$max = 160;

		$element = new \Zend_Form_Element_Text("seo_description");
		$element
			->setLabel("Description")
			->setDescription('The description will overwrite the default site description. Maximum length is %1$s characters.', $max)
			->addValidator($this->getStringLengthValidator(0, $max), true)
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->getView()->inlineScript()->appendScript(
			"jQuery(document).ready(function() {
				jQuery('#seo_description').counter({ max: {$max} });
			});"
		);

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addSEOPrioritySelect()
	{
		$element = new \Zend_Form_Element_Select("seo_priority");
		$element
			->setLabel("Priority")
			->setDescription("The priority setting is used in the XML sitemap to inform the search engine about the importance of a page within a website.")
			->addMultiOptions(array(
				"1.0" => "1.0",
				"0.9" => "0.9",
				"0.8" => "0.8",
				"0.7" => "0.7",
				"0.6" => "0.6",
				"0.5" => "0.5",
				"0.4" => "0.4",
				"0.3" => "0.3",
				"0.2" => "0.2",
				"0.1" => "0.1",
				"0.0" => "0.0"
			))
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addSEOChangeFrequencySelect()
	{
		$element = new \Zend_Form_Element_Select("seo_changefreq");
		$element
			->setLabel("Change frequency")
			->setDescription("How often the content is going to be updated. This setting is used by the XML sitemap.")
			->addMultiOptions(array(
				"always" => $this->getView()->translate("always"),
				"hourly" => $this->getView()->translate("hourly"),
				"daily" => $this->getView()->translate("daily"),
				"weekly" => $this->getView()->translate("weekly"),
				"monthly" => $this->getView()->translate("monthly"),
				"yearly" => $this->getView()->translate("yearly"),
				"never" => $this->getView()->translate("never")
			))
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addSEORobotsSelect()
	{
		$element = new \Zend_Form_Element_Select("seo_robots");
		$element
			->setLabel("Robots")
			->addMultiOptions(array(
				"" => $this->getView()->translate("Use uCMS default robots setting"),
				"index, follow" => $this->getView()->translate("Index the site and follow outgoing links"),
				"index, nofollow" => $this->getView()->translate("Index the site, but do not follow outgoing links"),
				"noindex, nofollow" => $this->getView()->translate("Do not index the site")
			))
			->setDescription("The robots settings control how the website is indexed by the search engines.")
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addIncludeInXMLSitemapChoice()
	{
		$element = new \Zend_Form_Element_Checkbox("sitemap");
		$element
			->setLabel("Include in XML sitemap")
			->setChecked(true)
		;

		$this->addElement($element);
		$this->addToDisplayGroup($element, "group_seo");

		return $element;
	}

	protected function addSaveButton()
	{
		$save = new \Zend_Form_Element_Submit("save");
		$save
			->setLabel("Save")
			->setAttrib("class", "button-save")
			->removeDecorator("DtDdWrapper")
		;

		$this->addElement($save);

		return $save;
	}
	protected function addSaveAndCountinueButton()
	{
		$continue = new \Zend_Form_Element_Submit("continue");
		$continue
			->setLabel("Save and continue")
			->setAttrib("class", "button-save")
			->removeDecorator("DtDdWrapper")
		;

		$this->addElement($continue);

		return $continue;
	}

	protected function addCancelButton()
	{
		$cancel = new \Zend_Form_Element_Submit("cancel");
		$cancel
			->setLabel("Cancel")
			->setAttrib("class", "button-cancel")
			->removeDecorator("DtDdWrapper")
		;

		$this->addElement($cancel);

		return $cancel;
	}
}
