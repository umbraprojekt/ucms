<?php
namespace Mingos\uCMS\Admin\Form;

use Mingos\uCMS\Model\ConfigModel;

/**
 * Add page form
 */
class AddPageForm extends AbstractForm
{
	/**
	 * Site configuration
	 * @var ConfigModel
	 */
	private $_config;

	public function __construct()
	{
		$this->_config = new ConfigModel();
		parent::__construct();
	}

	public function init()
	{
		$this->addContentTitleInput();
		$this->addTitleInput();
		$this->addImageInput();
		$this->addInputFormatSelect();
		$this->addTeaserInput();
		$this->addBodyInput();
		$this->addAliasInput();

		if ($this->_config->getParam('created', 'site') == '1' && $this->_config->getParam('created', 'page') == '1') {
			$this->addCreationDateInput();
		}

		// view script
		if ($this->_config->getParam('view_script', 'site') == '1' && $this->_config->getParam('view_script', 'page') == '1') {
			$scripts = array();
			if ($handle = opendir(DOC_ROOT."/views/index")) {
				while (false !== ($file = readdir($handle))) {
					$script = str_replace('.phtml','',$file);
					if ($file != $script && !empty($file)) {
						$script = substr($file,0,-6);
						$scripts[$script] = $file;
					}
				}
				closedir($handle);
				ksort($scripts);
			}
			$view_script = new \Zend_Form_Element_Select('view_script');
			$view_script
				->setLabel('View script file name')
				->addFilters(array(
					new \Zend_Filter_StripTags(),
					new \Zend_Filter_StripNewlines(),
					new \Zend_Filter_StringTrim()
				))
				->addMultiOptions($scripts)
				->setValue('page')

			;
			$this->addElement($view_script);
			$this->getDisplayGroup("group_content")->addElement($view_script);
		}

		$this->addPublishedSwitch()->setChecked(true);
		$this->addAutopublishInput();
		$this->addAutounpublishInput();

		if ($this->_config->getParam('search', 'site') == '1' && $this->_config->getParam('search', 'page') == '1') {
			$this->addIncludeInSearchSwitch()->setChecked(true);
		}

		if ($this->_config->getParam('seo_title', 'site') == '1' && $this->_config->getParam('seo_title', 'page') == '1') {
			$this->addSEOTitleInput();
		}

		$this->addSEOKeywordsInput();
		$this->addSEODescriptionInput();

		if ($this->_config->getParam('seo_priority', 'site') == '1') {
			$this->addSEOPrioritySelect()->setValue("0.5");
		}

		if ($this->_config->getParam('seo_changefreq', 'site') == '1') {
			$this->addSEOChangeFrequencySelect()->setValue("yearly");
		}

		if ($this->_config->getParam('robots', 'page') == '1') {
			$this->addSEORobotsSelect();
		}

		if ($this->_config->getParam('sitemap', 'site') == '1' && $this->_config->getParam('sitemap', 'page') == '1') {
			$this->addIncludeInXMLSitemapChoice();
		}

		$this->addSaveButton();
		$this->addSaveAndCountinueButton();
		$this->addCancelButton();
	}
}
