<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Add new menu form
 */
class AddMenuForm extends AbstractForm
{
	public function init()
	{
		// menu name
		$name = new \Zend_Form_Element_Text('name');
		$name
			->setLabel('Menu name')
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(3, 60), true)
			->setRequired()
			->addFilters(array(
				new \Zend_Filter_StripTags(),
				new \Zend_Filter_StripNewlines(),
				new \Zend_Filter_StringTrim()
			))
		;
		$this->addElement($name);

		$this->addSaveButton();
		$this->addCancelButton();
	}
}
