<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Edit preferences for images
 */
class ConfigImageForm extends AbstractForm
{
	public function init()
	{
		$tags = new \Zend_Form_Element_Checkbox('tags');
		$tags
			->setLabel('Enable tags')
		;

		$categories = new \Zend_Form_Element_Checkbox('categories');
		$categories
			->setLabel('Enable categories')
		;

		$quality = new \Zend_Form_Element_Select('quality');
		$quality
			->setLabel('Jpeg compression quality')
			->setDescription('The quality ofthe processed images. Higher quality means larger size. Quality values of 70 or 80 give optimal results.')
			->addMultiOptions(array(
				"50" => "50",
				"55" => "55",
				"60" => "60",
				"65" => "65",
				"70" => "70",
				"75" => "75",
				"80" => "80",
				"85" => "85",
				"90" => "90",
				"95" => "95",
				"100" => "100"
			))
			->setValue(80)
		;

		$unsafe_uploads = new \Zend_Form_Element_Checkbox('unsafe_uploads');
		$unsafe_uploads
			->setLabel('Unsafe image uploads')
			->setDescription('With this option checked, newly uploaded images will not undergo initial processing.')
		;

		$save = new \Zend_Form_Element_Submit('save');
		$save
			->setLabel('Save')
			->setAttrib('class', 'button-save')
			->removeDecorator('DtDdWrapper')
		;

		$this->addElements(array($quality,$unsafe_uploads,$save));
	}
}
