<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Edit SEO preferences form
 */
class ConfigSeoForm extends AbstractForm
{
	public function init()
	{
		$view = $this->getView();

		$title = new \Zend_Form_Element_Text('title');
		$title
			->setLabel('Site title')
		;

		$canonical = new \Zend_Form_Element_Text('canonical');
		$canonical
			->setLabel('Canonical URL')
			->setDescription('Canonical URLs help prevent the site from being flagged as containing duplicate content. Expected format: <em>hostname.tld</em>, for instance <em>example.com</em> or <em>www.example.com</em>')
			->addValidator('Hostname',true,array('messages' => array(
				'hostnameInvalidHostname' => $view->translate('Wrong DNS host name structure.'),
				'hostnameInvalidLocalName' => $view->translate('Local hosts are not allowed.'),
				'hostnameUnknownTld' => $view->translate('Wrong TLD.'),
				'hostnameLocalNameNotAllowed' => $view->translate('Local network names are not allowed.'),
				'hostnameInvalidHostnameSchema' => $view->translate('Invalid DNS host name schema.'),
				'hostnameIpAddressNotAllowed' => $view->translate('IP addresses are not allowed.')
			)))
			->addFilter('StringTrim',array('charlist' => '/'))
			->getDecorator('Description')->setOption('escape', false)
		;

		$keywords = new \Zend_Form_Element_Text('keywords');
		$keywords
			->setLabel('Global keywords')
			->setDescription('A comma-separated list of keywords, e.g. <em>shop, clothing, trousers, jeans, denim, jackets</em>. Keywords from this field will be used on every subpage.')
			->addFilter('StringToLower',array('encoding' => 'utf-8'))
			->getDecorator('Description')->setOption('escape', false)
		;

		$description = new \Zend_Form_Element_Text('description');
		$description
			->setLabel('Description')
			->setDescription('The description is shown in search results under the website\'s title. Maximum length is %1$s characters.', 160)
			->addValidator($this->getStringLengthValidator(0, 160))
		;

		$robots = new \Zend_Form_Element_Select('robots');
		$robots
			->setLabel('Robots')
			->addMultiOptions(array(
				'index, follow' => $view->translate('Index the site and follow outgoing links'),
				'index, nofollow' => $view->translate('Index the site, but do not follow outgoing links'),
				'noindex, nofollow' => $view->translate('Do not index the site')
			))
			->setDescription('The robots settings control how the website is indexed by the search engines.')
		;

		$analytics = new \Zend_Form_Element_Text('analytics');
		$analytics
			->setLabel('Google Analytics tracking code')
			->setDescription('Code provided by Google Analytics in order to track site traffic.')
		;

		$verification = new \Zend_Form_Element_Text('verification');
		$verification
			->setLabel('Google site verification')
			->setDescription('Meta tag verification code provided by Google Webmaster Tools.')
		;

		$this->addElements(array($title));
		$this->addDisplayGroup(array('title'),'info');
		$this->getDisplayGroup('info')->setLegend('Site info');

		$this->addElements(array($canonical,$keywords,$description,$robots,$analytics,$verification));
		$this->addDisplayGroup(array('canonical','keywords','description','robots','analytics','verification'),'seo');
		$this->getDisplayGroup('seo')->setLegend('SEO');

		$this->addSaveButton();
	}
}
