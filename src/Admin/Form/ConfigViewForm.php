<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Edit preferences for a node
 */
class ConfigViewForm extends AbstractConfigNodeForm
{
	public function init()
	{
		parent::init();
		if ($this->getElement('alias'))
			$this->getElement('alias')->setValue('%title%');
		if ($this->getElement('seo_priority'))
			$this->getElement('seo_priority')->setValue('0.8');
		if ($this->getElement('seo_changefreq'))
			$this->getElement('seo_changefreq')->setValue('monthly');
		if ($this->getElement('robots'))
			$this->getElement('robots')->setChecked(false);
		if ($this->getElement('sitemap'))
			$this->getElement('sitemap')->setChecked(false);
		if ($this->getElement('search'))
			$this->getElement('search')->setChecked(false);
	}
}
