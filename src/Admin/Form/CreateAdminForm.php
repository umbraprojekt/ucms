<?php
namespace Mingos\uCMS\Admin\Form;

/**
 * Admin account creation form
 */
class CreateAdminForm extends AbstractForm
{
	public function init()
	{
		$strings = array(
			0 => "Site administrator's username",
			1 => "Site administrator's password",
			2 => "Site administrator's e-mail address",
			3 => "The specified e-mail address is not valid.",
			4 => "Save site admin data"
		);

		$login = new \Zend_Form_Element_Text('login');
		$login
			->setLabel($strings[0])
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator($this->getStringLengthValidator(3, 32), true)
			->addValidator('Db_NoRecordExists', true, array('table' => 'user', 'field' => 'login'))
		;

		$password = new \Zend_Form_Element_Password('password');
		$password
			->setLabel($strings[1])
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
		;

		$email = new \Zend_Form_Element_Text('email');
		$email
			->setLabel($strings[2])
			->setRequired()
			->addValidator($this->getNotEmptyValidator(), true)
			->addValidator('Db_NoRecordExists', true, array('table' => 'user', 'field' => 'email'))
			->addValidator('Regex', true, array('pattern' => "/^[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+(\.[a-z0-9,!#\$%&'\*\+\/\=\?\^_`\{\|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.([a-z]{2,})$/", 'messages' => array('regexNotMatch' => $strings[3])))
		;

		$submit = new \Zend_Form_Element_Submit('adminSubmit');
		$submit->setLabel($strings[4])->setDecorators(array('ViewHelper'));

		$this->addElements(array($login, $password, $email, $submit));
	}
}
