<?php
namespace Mingos\uCMS\Admin\Controller;

use Mingos\uCMS\Admin\Form\AbstractConfigNodeForm;
use Mingos\uCMS\Helper\PaginatorHelper;
use Mingos\uCMS\Filter\NormaliseFilenameFilter;
use Mingos\uCMS\Model\AbstractNodeModel;
use Mingos\uCMS\Model\ConfigModel;
use Mingos\uCMS\Model\AclModel;
use Mingos\uCMS\Model\ContentGalleryModel;
use Mingos\uCMS\Model\ContentModel;
use Mingos\uCMS\Model\ContentRelationModel;
use Mingos\uCMS\Model\ContentTaxonomyModel;
use Mingos\uCMS\Model\FileModel;
use Mingos\uCMS\Model\ImageModel;
use Mingos\uCMS\Model\TaxonomyModel;

abstract class AbstractAdminController extends \Zend_Controller_Action
{
	/**
	 * Flash messenger
	 * @var \Zend_Controller_Action_Helper_FlashMessenger
	 */
	protected $flash;

	/**
	 * User identity
	 * @var array
	 */
	protected $identity;

	/**
	 * Site configuration
	 * @var array
	 */
	protected $siteConfig;

	/**
	 * @var ConfigModel
	 */
	protected $nodeConfig;

	/**
	 * Access Control List
	 * @var AclModel
	 */
	protected $acl;

	/**
	 * Initialise everything
	 */
	public function init() {
		// identity identity
		$auth = \Zend_Auth::getInstance()->hasIdentity();
		if (!$auth) {
			$this->redirect("/default/user/login");
		}
		$this->identity = $this->view->identity = \Zend_Auth::getInstance()->getIdentity();

		// flash messages
		$this->flash = $this->_helper->flashMessenger;
		if ($this->flash->hasMessages()) {
			$this->view->messages = $this->flash->getMessages();
		} else {
			$this->view->messages = array();
		}
		$this->flash->clearMessages();

		// title and layout
		$this->view->headTitle($this->view->translate('Administration'));

		// config
		$this->siteConfig = $this->view->siteConfig = new ConfigModel("site");

		// ACL
		$this->acl = $this->view->acl = AclModel::getInstance();
		if (!$this->acl->isAllowed($this->_request->getParam('module')."/".$this->_request->getParam('controller')."/".$this->_request->getParam('action'))) {
			$this->flash->addMessage(array('error' => $this->view->translate('You don\'t have enough privileges to access this page.')));
			$this->redirect("/admin");
		}

		// nav menu
		$this->view->nav = $this->_helper->nav();
	}

	protected function makePaginator(\Zend_Db_Select $nodes) {
		return PaginatorHelper::make(
			new \Zend_Paginator_Adapter_DbSelect($nodes),
			$this->_request->getParam("limit", 25),
			$this->_request->getParam("page", 1)
		);
	}

	/**
	 * Retrieve files stored within the $_FILES superglobal
	 * @param string $name The name of the file input the files were uploaded through
	 * @return array
	 */
	protected function _getFiles ($name) {
		$files = array();
		if (array_key_exists($name,$_FILES)) {
			if (is_array($_FILES[$name]['name'])) {
				for ($i = 0; $i < count($_FILES[$name]['name']); ++$i) {
					$files[] = array(
						'name' => $_FILES[$name]['name'][$i],
						'type' => $_FILES[$name]['type'][$i],
						'tmp_name' => $_FILES[$name]['tmp_name'][$i],
						'error' => $_FILES[$name]['error'][$i],
						'size' => $_FILES[$name]['size'][$i]
					);
				}
			} else {
				$files[] = $_FILES[$name];
			}
		}
		return $files;
	}

	/**
	 * Check if a file has been uploaded successfully
	 * @param \Zend_Form $form
	 * @param string $fieldName
	 * @return array
	 */
	protected function _validateFileUpload($form, $fieldName)
	{
		$file = $this->_getFiles($fieldName);
		$file = $file[0];

		switch ($file['error']) {
			case UPLOAD_ERR_NO_FILE:
				if ($form->getElement($fieldName)->isRequired()) {
					$form->getElement($fieldName)->setErrors(array($this->view->translate('Please choose a file to upload.')));
				}
				break;
			case UPLOAD_ERR_INI_SIZE:
				$form->getElement($fieldName)->setErrors(array($this->view->translate('The file is too large.')));
				break;
			case UPLOAD_ERR_OK:
				$filter = new NormaliseFilenameFilter();
				$file['name'] = $filter->filter($file['name']);
				if (
					copy($file['tmp_name'], DOC_ROOT . "/tmp/{$file['name']}") === false
					|| !is_file($file['tmp_name'])
				) {
					$form->getElement($fieldName)->setErrors(array($this->view->translate('The file could not be uploaded.')));
				}
				break;
			default:
				$form->getElement($fieldName)->setErrors(array($this->view->translate('An error has occurred.')));
				break;
		}

		return $file;
	}

	/**
	 * Receive a regular file (not an image) upload
	 * @param  array   $file
	 * @param  string  $path
	 * @return integer       Inserted file ID
	 */
	protected function _receiveFileUpload($file, $path)
	{
		if ($file['error'] == UPLOAD_ERR_OK) {
			$filedata = $this->_helper->saveFile(DOC_ROOT . "/tmp/{$file['name']}", DOC_ROOT . "/upload/{$path}");
			unlink(DOC_ROOT . "/tmp/{$file['name']}");

			$modelFile = new FileModel();
			return $modelFile->insert(
				array(
					'created' => new \Zend_Db_Expr('CURRENT_TIMESTAMP'),
					'uid' => $this->identity->id,
					'mime' => $filedata['mime'],
					'extension' => $filedata['extension'],
					'filename' => $filedata['filename'],
					'path' => "{$path}",
					'type' => 'file'
				)
			);
		}
	}

	/**
	 * Receive and save the uploaded file
	 * @param array $file
	 * @param array $node
	 * @param string $tableName
	 * @param string $filePath
	 */
	protected function _receiveImageUpload($file, $node, $tableName, $filePath)
	{
		if ($file['error'] == UPLOAD_ERR_OK) {
			$filedata = $this->_helper->saveFile(DOC_ROOT . "/tmp/{$file['name']}", DOC_ROOT . "/upload/{$filePath}/{$node['id']}");
			unlink(DOC_ROOT . "/tmp/{$file['name']}");

			$fileModel = new FileModel();
			$imageModel = new ImageModel();

			$i = $imageModel->getAdapter()->fetchRow(
				"SELECT * FROM image
				WHERE nid = {$node['id']} AND `table` = '{$tableName}'"
			);

			if (!empty($i)) {
				$f = $imageModel->getAdapter()->fetchRow(
					"SELECT * FROM file WHERE id = {$i['id']}"
				);
				unlink(DOC_ROOT . "/upload/{$f['path']}/{$f['filename']}.{$f['extension']}");
				if ($handle = opendir(PUBLIC_PATH . "/upload/{$i['path']}")) {
					while (false !== ($entry = readdir($handle))) {
						if (strstr($entry, $i['filename'])) {
							unlink(PUBLIC_PATH . "/upload/{$i['path']}/{$entry}");
						}
					}
					closedir($handle);
				}

				$fileModel->update(
					array(
						'created' => new \Zend_Db_Expr('CURRENT_TIMESTAMP'),
						'uid' => $this->identity->id,
						'mime' => $filedata['mime'],
						'extension' => $filedata['extension'],
						'filename' => $filedata['filename'],
					),
					"id = {$f['id']}"
				);
				$imageModel->update(
					array(
						'title' => $filedata['filename'],
						'alt' => $filedata['filename'],
						'filename' => $filedata['filename']
					),
					"id = {$i['id']}"
				);
			} else {
				$iid = $fileModel->insert(
					array(
						'created' => new \Zend_Db_Expr('CURRENT_TIMESTAMP'),
						'uid' => $this->identity->id,
						'mime' => $filedata['mime'],
						'extension' => $filedata['extension'],
						'filename' => $filedata['filename'],
						'path' => "{$filePath}/{$node['id']}",
						'type' => 'image'
					)
				);
				$imageModel->insert(
					array(
						'iid' => $iid,
						'table' => $tableName,
						'nid' => $node['id'],
						'title' => $filedata['filename'],
						'alt' => $filedata['filename'],
						'path' => "{$filePath}/{$node['id']}",
						'filename' => $filedata['filename']
					)
				);
			}
		}
	}

	protected function galleryActionTemplate(AbstractNodeModel $model)
	{
		$id = (int)$this->_request->getParam('id', 0);

		$node = $this->view->node = $model->getById($id);
		if (!$node) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect($this->view->url(array(
				"module" => "admin",
				"controller" => $this->_request->getControllerName(),
				"action" => "index"
			), "default", true));
		}

		// get the related and unrelated galleries
		$contentGalleryModel = new ContentGalleryModel();

		$scope = \Zend_Json::encode(array(
			"node" => $node,
			"related" => $contentGalleryModel->getRelatedGalleries($id),
			"unrelated" => $contentGalleryModel->getUnrelatedGalleries($id)
		));
		$this->view->inlineScript()->appendScript("window.scope = {$scope};");
	}

	/**
	 * @param AbstractConfigNodeForm $form
	 * @param ConfigModel $configModel Node configuration (needs to have type set)
	 * @throws \Zend_Form_Exception
	 */
	protected function configActionTemplate(AbstractConfigNodeForm $form, ConfigModel $configModel)
	{
		$redirectUrl = $this->view->url(array(
			"module" => "admin",
			"controller" => $this->_request->getControllerName(),
			"action" => "config"
		), "default", true);

		if ($this->_request->isPost()) {
			if ($this->_request->getPost('save', false)) {
				if ($form->isValid($this->_request->getPost())) {
					$configModel->setParams($form->getValues());
					$this->flash->addMessage(array('success' => $this->view->translate('Confuration saved.')));
					$this->redirect($redirectUrl);
				} else {
					$this->view->messages[] = array('error' => $this->view->translate('Please make sure all fields are filled in correctly.'));
				}
			} else {
				$this->redirect($redirectUrl);
			}
		} else {
			$form->populate($configModel->getParams());
		}

		$this->view->form = $form;
	}

	public function taxonomyActionTemplate(AbstractNodeModel $model)
	{
		$id = (int)$this->_request->getParam("id", 0);

		$node = $model->fetchRow(array("id = ?" => $id));
		if (!$node) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect($this->view->url(array(
				"module" => "admin",
				"controller" => $this->_request->getControllerName(),
				"action" => "index"
			), "default", true));
		}

		if ($this->_request->isPost()) {
			$contentTaxonomyModel = new ContentTaxonomyModel();
			$contentTaxonomyModel->delete($contentTaxonomyModel->getAdapter()->quoteInto("cid = ?", $id));

			$newTaxonomy = $this->_request->getPost("tid", array());
			$contentTaxonomyModel->assignContent($id, $newTaxonomy);

			$this->flash->addMessage(array('success' => $this->view->translate('Taxonomy assigned.')));
			$this->redirect($this->view->url(array(
				"module" => "admin",
				"controller" => $this->_request->getControllerName(),
				"action" => "taxonomy",
				"id" => $id
			), "default", true));
		}

		// get all taxonomy
		$taxonomyModel = new TaxonomyModel();
		$taxonomy = $taxonomyModel->getAllByDictionary();

		// get taxonomy by content
		$modelContent = new ContentModel();
		$contentTaxonomy = $modelContent->getTaxonomy($id);

		$this->view->taxonomy = $taxonomy;
		$this->view->contentTaxonomy = $contentTaxonomy;
		$this->view->content = $modelContent->fetchRow(array("id = ?" => $id));
		$this->view->node = $node;
	}

	/**
	 * Operations on the related content.
	 */
	public function relatedActionTemplate()
	{
		$id = intval($this->_request->getParam("id", 0));
		$modelContent = new ContentModel();
		$content = $modelContent->fetchRow(array("id = ?" => $id));
		if (!$content) {
			$this->flash->addMessage(array('error' => $this->view->translate('Invalid parametres.')));
			$this->redirect($this->view->url(array(
				"module" => "admin",
				"controller" => $this->_request->getControllerName(),
				"action" => "index"
			), "default", true));
		}

		$modelContentRelation = new ContentRelationModel();

		// add new related content
		if ($this->_request->isPost()) {
			$values = $this->_request->getPost();
			$modelContentRelation->set($id, $values["relation_cid"], $values["sticky"]);
			$this->flash->addMessage(array('success' => $this->view->translate('Related content added successfully.')));
			$this->redirect($this->view->url());
		}

		// modify existing content
		if ($this->_request->isPut()) {
			parse_str($this->_request->getRawBody(), $values);
			$modelContentRelation->set($id, $values["relation_cid"], $values["sticky"]);
			die();
		}

		// remove existing content
		if ($this->_request->isDelete()) {
			parse_str($this->_request->getRawBody(), $values);
			$modelContentRelation->remove($id, $values["relation_cid"]);
			$this->flash->addMessage(array('success' => $this->view->translate('Related content removed successfully.')));
			die();
		}

		$this->view->related = $modelContentRelation->getRelated($id, false);
		$this->view->unrelated = $modelContentRelation->getUnrelated($id);
		$this->view->content = $content;
	}
}
