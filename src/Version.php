<?php
namespace Mingos\uCMS;

/**
 * Project versioning
 */
class Version {
	/**
	 * Major version of the project.
	 *
	 * Goes up with a full release cycle or other major milestone is achieved: first stable version release, a stable
	 * release after a code rewrite, etc.
	 */
	const MAJOR = '1';

	/**
	 * Minor version of the project.
	 *
	 * Minor versions go up whenever a new feature or major behaviour is implemented.
	 */
	const MINOR = '5';

	/**
	 * Patch version of the project.
	 *
	 * Patch versions go up when a patch or a bunch thereof is officially added to the project. Patches involve minor
	 * improvements and bugfixes.
	 */
	const PATCH = '0';

	/**
	 * Create a string from the version constants
	 *
	 * @return string CMS version
	 */
	public static function versionString () {
		return self::MAJOR . "." . self::MINOR . "." . self::PATCH;
	}
}
