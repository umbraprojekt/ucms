<?php
namespace Mingos\uCMS\Helper;

class PaginatorHelper
{
	public static function make(
		\Zend_Paginator_Adapter_Interface $paginatorAdapter,
		$limit,
		$pageNumber,
		$viewPartial = "paginator.phtml"
	) {
		$paginator = new \Zend_Paginator($paginatorAdapter);
		$paginator->setItemCountPerPage($limit);
		$paginator->setCurrentPageNumber($pageNumber);
		\Zend_View_Helper_PaginationControl::setDefaultViewPartial($viewPartial);

		return $paginator;
	}
}
