angular.module("ucms").controller("ContentGalleryController", function(
	$scope,
	$http
) {
	$scope.node = window.scope.node;
	$scope.related = window.scope.related;
	$scope.unrelated = window.scope.unrelated;

	$scope.relate = function(gallery) {
		$http({
			method: "POST",
			url: "/admin/gallery/relate-content",
			data: {
				ccid: $scope.node.id,
				gcid: gallery.id
			}
		}).success(function() {
			window.location.reload();
		});
	};
	$scope.unrelate = function(gallery) {
		$http({
			method: "POST",
			url: "/admin/gallery/unrelate-content",
			data: {
				ccid: $scope.node.id,
				gcid: gallery.id
			}
		}).success(function() {
			window.location.reload();
		});
	};
	$scope.create = function(title) {
		$http({
			method: "POST",
			url: "/admin/gallery/add",
			data: $.param({
				content_title: title,
				continue: "continue"
			}),
			headers: {"Content-Type": "application/x-www-form-urlencoded"}
		}).success(function() {
			window.location.reload();
		});
	};
});
