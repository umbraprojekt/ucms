angular.module("ucms").controller("MenuItemsController", function(
	$scope,
	$http,
	$window,
	fileUpload
) {
	var assignFile = function(fileId, menuId) {
		return $http.put("/admin/menu/item-image/id/" + menuId, {fileId: fileId});
	};
	var getFile = function(fileId) {
		return $http.get("/admin/file/file", {id: fileId});
	};
	$scope.upload = function(file, menuId) {
		fileUpload(file, "/admin/file/add")
			.then(function(result) {
				var fileId = result.data.id;
				return assignFile(fileId, menuId);
			})
			.then(function() {
				$window.location.reload();
			});
	};
	$scope.removeFile = function(menuId) {
		assignFile(null, menuId)
			.then(function() {
				$window.location.reload();
			});
	};
});
