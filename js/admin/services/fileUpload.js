angular.module("ucms").factory("fileUpload", function($http) {
	return function(file, url) {
		var formData = new FormData();
		formData.append("file", file);
		return $http.post(url, formData, {
			transformRequest: angular.identity,
			headers: {
				"Content-Type": undefined
			}
		});
	};
});
