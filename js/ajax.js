$(document).ready(
	function() {
		$("input[type='text'].ajax").on({
			blur: function() {
				var elt = $(this);
				var val = elt.val();

				if (val != elt.get(0).defaultValue) {
					elt.addClass("loading");
					$.ajax({
						url: elt.attr("data-url"),
						dataType: "json",
						data: {
							column: elt.attr("data-column"),
							id: elt.attr("data-id"),
							val: val
						},
						success: function(data) {
							elt.removeClass('loading');
							if(data.result) {
								elt.val(data.val);
								elt.get(0).defaultValue = data.val;
							} else {
								elt.val(elt.get(0).defaultValue);
							}
						}
					});
				}
			}
		});
	}
);

/**
 * Add image preview to node edit form; fetch image and display thumbnail BEFORE the specified selector.
 * @param {Number} imageId
 * @param {String} selector
 */
var getRemovableImage = function(imageId, selector)
{
	$(function() {
		$.ajax({
			method: "GET",
			url: "/admin/image/url",
			data: {
				id: imageId,
				w: 100,
				h: 100,
				type: "adaptive",
				fx: [
					["sharpen", 16]
				]
			},
			success: function(data) {
				var img = $(document.createElement("IMG")).attr("src", data);
				var remove = $(document.createElement("BUTTON"));
				var wrapper = $(document.createElement("DIV")).addClass("removable-image-wrapper");
				wrapper.append(img).append(remove);

				$(selector).before(wrapper);

				remove.on("click", function(event) {
					event.preventDefault();
					$.ajax({
						url: "/admin/image/delete-ajax",
						method: "GET",
						data: {
							id: imageId
						},
						success: function() {
							$(wrapper).remove();
						}
					});
				});
			}
		});
	});
};
