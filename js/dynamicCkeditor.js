/**
 * Dynamically load CKEditor (to be used when changing input type between HTML and Markdown)
 */
$(function() {
	var inputFormatChange = function() {
		var els = ["teaser", "body"];
		var format = document.getElementById("input_format").value;

		els.forEach(function(item) {
			var el = document.getElementById(item);
			if (el) {
				el = $(el);
			} else {
				return;
			}
			if ("html" === format) {
				var editor = CKEDITOR.replace(el.get(0));
				el.data("editor", editor);
			} else {
				if (el.data("editor")) {
					el.data("editor").destroy();
					el.data("editor", null);
				}
			}
		});
	};

	var input = $("#input_format");
	if (input.length) {
		input.on("change", inputFormatChange);
		inputFormatChange();
	}
});
