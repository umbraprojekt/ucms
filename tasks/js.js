var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");

gulp.task("js:admin", function() {
	return gulp.src([
		"bower_components/jquery/dist/jquery.js",
		"bower_components/mousetrap/mousetrap.js",
		"bower_components/angular/angular.js",
		"js/ucms.js",
		"js/ajax.js",
		"js/dynamicCkeditor.js",
		"js/admin/**/*.js"
	])
		.pipe(concat("admin.js"))
		.pipe(uglify())
		.pipe(gulp.dest("public_html/js"));
});
