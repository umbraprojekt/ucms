var gulp = require("gulp");
var less = require("gulp-less");

gulp.task("css:admin", function() {
	return gulp.src("less/admin.less")
		.pipe(less({
			cleancss: false,
			compress: true
		}))
		.pipe(gulp.dest("public_html/css"));
});

gulp.task("css:login", function() {
	return gulp.src("less/login.less")
		.pipe(less({
			cleancss: false,
			compress: true
		}))
		.pipe(gulp.dest("public_html/css"));
});

gulp.task("css:dev", function() {
	return gulp.src("less/style.less")
		.pipe(less({
			cleancss: false,
			compress: false
		}))
		.pipe(gulp.dest("public_html/css"));
});

gulp.task("css:dist", function() {
	return gulp.src("less/style.less")
		.pipe(less({
			cleancss: true,
			compress: true
		}))
		.pipe(gulp.dest("public_html/css"));
});
