# uCMS

uCMS is a programmer-oriented content management system.

## Installation

Clone the repo:

```
git clone https://bitbucket.org/umbraprojekt/ucms.git
cd ucms
```

Install dependencies:

```
composer install
npm install
bower install
```

Build:

```
grunt less
```

Edit **application/configs/application.ini** and provide database connection info. Make sure the provided database exists.

Provide a vhost. For the purpose of this readme, I'll assume the server is running on localhost:8080.

Start the webserver. Install the application by opening `http://localhost:8080/install`.

Open admin panel at `http://localhost:8080/admin`.
